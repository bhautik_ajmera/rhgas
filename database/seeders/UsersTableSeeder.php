<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'code'           => 'E001',
                'name'           => 'Admin',
                'email'          => 'admin@gmail.com',
                'password'       => bcrypt('admin@123'),
                'mobile_no'      => '1234567890',
                'address'        => 'Jamnagar',
                'pin_code'       => '361008',
                'state_id'       => 1,
                'city_id'        => 1,
                'remember_token' => null
            ],
        ];

        User::insert($users);
    }
}
