<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\City;
use App\Models\State;

class StateCitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$stateObj       = new State();
		$stateObj->name = 'Gujarat';
		$stateObj->save();

		$cityObj           = new City();
		$cityObj->state_id = $stateObj->id;
		$cityObj->name     = 'Ahmedabad';
		$cityObj->save();
    }
}
