<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'configuration_access',
            ],
            [
                'id'    => 18,
                'title' => 'city_create',
            ],
            [
                'id'    => 19,
                'title' => 'city_edit',
            ],
            [
                'id'    => 20,
                'title' => 'city_show',
            ],
            [
                'id'    => 21,
                'title' => 'city_delete',
            ],
            [
                'id'    => 22,
                'title' => 'city_access',
            ],
            [
                'id'    => 23,
                'title' => 'branch_create',
            ],
            [
                'id'    => 24,
                'title' => 'branch_edit',
            ],
            [
                'id'    => 25,
                'title' => 'branch_show',
            ],
            [
                'id'    => 26,
                'title' => 'branch_delete',
            ],
            [
                'id'    => 27,
                'title' => 'branch_access',
            ],
            [
                'id'    => 28,
                'title' => 'state_create',
            ],
            [
                'id'    => 29,
                'title' => 'state_edit',
            ],
            [
                'id'    => 30,
                'title' => 'state_show',
            ],
            [
                'id'    => 31,
                'title' => 'state_delete',
            ],
            [
                'id'    => 32,
                'title' => 'state_access',
            ],
            [
                'id'    => 33,
                'title' => 'customer_create',
            ],
            [
                'id'    => 34,
                'title' => 'customer_edit',
            ],
            [
                'id'    => 35,
                'title' => 'customer_show',
            ],
            [
                'id'    => 36,
                'title' => 'customer_delete',
            ],
            [
                'id'    => 37,
                'title' => 'customer_access',
            ],
            [
                'id'    => 38,
                'title' => 'supplier_create',
            ],
            [
                'id'    => 39,
                'title' => 'supplier_edit',
            ],
            [
                'id'    => 40,
                'title' => 'supplier_show',
            ],
            [
                'id'    => 41,
                'title' => 'supplier_delete',
            ],
            [
                'id'    => 42,
                'title' => 'supplier_access',
            ],
            [
                'id'    => 43,
                'title' => 'ga_create',
            ],
            [
                'id'    => 44,
                'title' => 'ga_edit',
            ],
            [
                'id'    => 45,
                'title' => 'ga_show',
            ],
            [
                'id'    => 46,
                'title' => 'ga_delete',
            ],
            [
                'id'    => 47,
                'title' => 'ga_access',
            ],
            [
                'id'    => 48,
                'title' => 'cylinder_create',
            ],
            [
                'id'    => 49,
                'title' => 'cylinder_edit',
            ],
            [
                'id'    => 50,
                'title' => 'cylinder_show',
            ],
            [
                'id'    => 51,
                'title' => 'cylinder_delete',
            ],
            [
                'id'    => 52,
                'title' => 'cylinder_access',
            ],
            [
                'id'    => 53,
                'title' => 'cylinder_company_create',
            ],
            [
                'id'    => 54,
                'title' => 'cylinder_company_edit',
            ],
            [
                'id'    => 55,
                'title' => 'cylinder_company_show',
            ],
            [
                'id'    => 56,
                'title' => 'cylinder_company_delete',
            ],
            [
                'id'    => 57,
                'title' => 'cylinder_company_access',
            ],
            [
                'id'    => 58,
                'title' => 'cylinder_issue_create',
            ],
            [
                'id'    => 59,
                'title' => 'cylinder_issue_edit',
            ],
            [
                'id'    => 60,
                'title' => 'cylinder_issue_show',
            ],
            [
                'id'    => 61,
                'title' => 'cylinder_issue_delete',
            ],
            [
                'id'    => 62,
                'title' => 'cylinder_issue_access',
            ],
            [
                'id'    => 63,
                'title' => 'income_expense_create',
            ],
            [
                'id'    => 64,
                'title' => 'income_expense_edit',
            ],
            [
                'id'    => 65,
                'title' => 'income_expense_show',
            ],
            [
                'id'    => 66,
                'title' => 'income_expense_delete',
            ],
            [
                'id'    => 67,
                'title' => 'income_expense_access',
            ],
            [
                'id'    => 68,
                'title' => 'purchase_create',
            ],
            [
                'id'    => 69,
                'title' => 'purchase_edit',
            ],
            [
                'id'    => 70,
                'title' => 'purchase_show',
            ],
            [
                'id'    => 71,
                'title' => 'purchase_delete',
            ],
            [
                'id'    => 72,
                'title' => 'purchase_access',
            ],
            [
                'id'    => 73,
                'title' => 'cylinder_management_access',
            ],
            [
                'id'    => 74,
                'title' => 'profile_password_edit',
            ],
            [
                'id'    => 75,
                'title' => 'report_access'
            ],
            [
                'id'    => 76,
                'title' => 'income_expense_report_access'
            ],
            [
                'id'    => 77,
                'title' => 'cylinder_issue_received_access'
            ],
            [
                'id'    => 78,
                'title' => 'refilling_create'
            ],
            [
                'id'    => 79,
                'title' => 'refilling_edit'
            ],
            [
                'id'    => 80,
                'title' => 'refilling_show'
            ],
            [
                'id'    => 81,
                'title' => 'refilling_delete'
            ],
            [
                'id'    => 82,
                'title' => 'refilling_access'
            ],
            [
                'id'    => 83,
                'title' => 'refilling_received_access'
            ],
            [
                'id'    => 84,
                'title' => 'invoice_access'
            ],
            [
                'id'    => 85,
                'title' => 'customer_invoice_create'
            ],
            [
                'id'    => 86,
                'title' => 'cylinder_report_access'
            ]
        ];

        Permission::insert($permissions);
    }
}
