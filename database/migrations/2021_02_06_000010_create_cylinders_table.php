<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCylindersTable extends Migration
{
    public function up()
    {
        Schema::create('cylinders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_cyl_no');
            $table->string('branch_cyl_no')->nullable();
            $table->string('capacity');
            $table->date('manufacturing_date');
            $table->date('last_hydrotest_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
