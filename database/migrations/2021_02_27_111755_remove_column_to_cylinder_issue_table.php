<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveColumnToCylinderIssueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cylinder_issue', function (Blueprint $table) {
            $table->dropColumn('cylinder_no');
            $table->dropColumn('gas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cylinder_issue', function (Blueprint $table) {
            $table->string('cylinder_no')->after('remark');
            $table->string('gas')->after('cylinder_no');
        });
    }
}
