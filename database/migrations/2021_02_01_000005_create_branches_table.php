<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchesTable extends Migration
{
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('code');
            $table->string('contact_person');
            $table->string('company_mob_no');
            $table->string('personal_mob_no')->nullable();
            $table->longText('address');
            $table->string('pin_code');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
