<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCylinderIssueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cylinder_issue', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('branch_id');
            $table->integer('cyl_com_id');
            $table->string('po_num')->nullable()->default(null);
            $table->date('po_date');
            $table->string('sys_chal_no')->nullable()->default(null);
            $table->date('delivery_date');
            $table->string('veh_no')->nullable()->default(null);
            $table->longText('remark')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cylinder_issue');
    }
}
