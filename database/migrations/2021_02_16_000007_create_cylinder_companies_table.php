<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCylinderCompaniesTable extends Migration
{
    public function up()
    {
        Schema::create('cylinder_companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->nullable();
            $table->string('name');
            $table->string('mobile_no');
            $table->longText('address');
            $table->string('pin_code')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
