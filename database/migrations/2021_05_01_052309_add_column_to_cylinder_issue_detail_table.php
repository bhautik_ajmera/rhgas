<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToCylinderIssueDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cylinder_issue_detail', function (Blueprint $table) {
            $table->integer('rent')->nullable()->default(null)->after('rate');
            $table->integer('start_rent')->nullable()->default(null)->after('rent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cylinder_issue_detail', function (Blueprint $table) {
            $table->dropColumn('rent');
            $table->dropColumn('start_rent');
        });
    }
}
