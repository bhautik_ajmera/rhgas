<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToCylindersTable extends Migration
{
    public function up()
    {
        Schema::table('cylinders', function (Blueprint $table) {
            $table->unsignedBigInteger('supplier_id');
            $table->foreign('supplier_id', 'supplier_fk_3131410')->references('id')->on('suppliers');
            $table->unsignedBigInteger('gas_id');
            $table->foreign('gas_id', 'gas_fk_3131411')->references('id')->on('gas');
            $table->unsignedBigInteger('used_for_id');
            $table->foreign('used_for_id', 'used_for_fk_3133495')->references('id')->on('gas');
        });
    }
}
