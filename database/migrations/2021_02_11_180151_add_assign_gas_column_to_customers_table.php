<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAssignGasColumnToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('gas_id');
            $table->dropColumn('rate');
            $table->dropColumn('rent');
            $table->dropColumn('start_rent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->integer('gas_id')->nullable()->default(null)->after('remark');
            $table->float('rate', 8, 2)->nullable()->default(null)->after('gas_id');
            $table->integer('rent')->nullable()->default(null)->after('rate');
            $table->integer('start_rent')->nullable()->default(null)->after('rent');
        });
    }
}
