<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->datetime('email_verified_at')->nullable();
            $table->string('password');
            $table->string('remember_token')->nullable();
            $table->string('code');
            $table->string('mobile_no');
            $table->longText('address');
            $table->string('pin_code');
            $table->string('addhar_card_no')->nullable();
            $table->longText('addhar_card_address')->nullable();
            $table->string('ref_con_name')->nullable();
            $table->string('ref_con_no')->nullable();
            $table->string('relation')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
