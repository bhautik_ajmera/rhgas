<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVehicleNoToRefillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('refilling', function (Blueprint $table) {
            $table->string('vehicle_no',15)->nullable()->default(null)->after('date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('refilling', function (Blueprint $table) {
            $table->dropColumn('vehicle_no');
        });
    }
}
