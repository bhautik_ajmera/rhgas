<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDischkToCylindersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cylinders', function (Blueprint $table) {
            $table->boolean('dis_cyl')->default(0)->comment = "0 = No, 1 = Yes";
            $table->longText('dis_note')->nullable()->default(null)->after('dis_cyl');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cylinders', function (Blueprint $table) {
            Schema::dropIfExists('dis_cyl');
            Schema::dropIfExists('dis_note');
        });
    }
}
