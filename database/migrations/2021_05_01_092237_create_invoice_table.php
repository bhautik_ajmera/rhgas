<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('invoice_no')->nullable()->default(null);
            $table->date('invoice_date');
            $table->string('to_name')->nullable()->default(null);
            $table->longText('to_address')->nullable()->default(null);
            $table->string('to_city')->nullable()->default(null);
            $table->string('to_state')->nullable()->default(null);
            $table->string('to_pin_code')->nullable()->default(null);
            $table->string('to_mob')->nullable()->default(null);
            $table->string('to_alt_mo')->nullable()->default(null);
            $table->string('to_email')->nullable()->default(null);
            $table->string('to_cus_code')->nullable()->default(null);
            $table->string('to_contact_person')->nullable()->default(null);
            $table->string('to_gst_no')->nullable()->default(null);
            $table->string('to_aadhaar_card_no')->nullable()->default(null);
            $table->float('row_total', 8, 2)->nullable()->default(null);
            $table->float('row_rent_total', 8, 2)->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice');
    }
}
