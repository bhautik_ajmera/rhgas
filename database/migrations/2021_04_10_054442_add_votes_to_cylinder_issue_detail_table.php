<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVotesToCylinderIssueDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cylinder_issue_detail', function (Blueprint $table) {
            $table->float('rate', 8, 2)->default(0.00)->after('gas_id');
            $table->boolean('status')->default(0)->after('rate')->comment = '0 = False, 1 = True';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cylinder_issue_detail', function (Blueprint $table) {
            $table->dropColumn('rate'); 
            $table->dropColumn('status'); 
        });
    }
}
