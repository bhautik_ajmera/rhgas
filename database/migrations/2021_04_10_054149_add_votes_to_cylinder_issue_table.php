<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVotesToCylinderIssueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cylinder_issue', function (Blueprint $table) {
            $table->boolean('is_issue_received')->default(0)->after('remark')->comment = '0 = No,1 = Yes';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cylinder_issue', function (Blueprint $table) {
            $table->dropColumn('is_issue_received');
        });
    }
}
