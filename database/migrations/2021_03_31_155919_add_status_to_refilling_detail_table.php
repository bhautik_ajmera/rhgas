<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToRefillingDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('refilling_detail', function (Blueprint $table) {
            $table->boolean('status')->default(0)->after('rate')->comment = '0 = False, 1 = True';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('refilling_detail', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
