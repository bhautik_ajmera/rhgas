<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignGasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_gas', function (Blueprint $table) {
            $table->id();
            $table->integer('customer_id');
            $table->integer('gas_id');
            $table->float('rate', 8, 2)->nullable()->default(null);
            $table->integer('rent')->nullable()->default(null);
            $table->integer('start_rent')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_gas');
    }
}
