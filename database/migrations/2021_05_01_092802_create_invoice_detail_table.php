<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_detail', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('invoice_id');
            $table->foreign('invoice_id')->references('id')->on('invoice')->onDelete('cascade');
            $table->string('challan_no')->nullable()->default(null);
            $table->string('gas')->nullable()->default(null);
            $table->string('cylinder')->nullable()->default(null);
            $table->date('issued_date')->nullable()->default(null);
            $table->date('received_date')->nullable()->default(null);
            $table->float('total', 8, 2)->nullable()->default(null);
            $table->integer('rate_per_day')->nullable()->default(null);
            $table->integer('start_rent')->nullable()->default(null);
            $table->float('rent_total', 8, 2)->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_detail');
    }
}
