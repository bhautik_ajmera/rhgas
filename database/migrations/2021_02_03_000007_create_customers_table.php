<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('companny_name')->unique();
            $table->string('code');
            $table->string('email')->nullable();
            $table->string('contact_person');
            $table->longText('address');
            $table->string('pin_code')->nullable();
            $table->string('mob_no');
            $table->string('alternate_mob_no')->nullable();
            $table->string('gstno')->nullable();
            $table->string('addhar_card_no')->nullable();
            $table->longText('addhar_card_address')->nullable();
            $table->longText('remark')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
