<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCylinderIssueDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cylinder_issue_detail', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cylinder_issue_id');
            $table->foreign('cylinder_issue_id')->references('id')->on('cylinder_issue')->onDelete('cascade');
            $table->integer('cyl_id');
            $table->integer('gas_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cylinder_issue_detail');
    }
}
