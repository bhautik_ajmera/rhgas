<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefillingDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refilling_detail', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('refilling_id');
            $table->foreign('refilling_id')->references('id')->on('refilling')->onDelete('cascade');
            $table->integer('gas_id');
            $table->integer('cyl_id');
            $table->float('rate', 8, 2)->default(0.00);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('refilling_detail');
    }
}
