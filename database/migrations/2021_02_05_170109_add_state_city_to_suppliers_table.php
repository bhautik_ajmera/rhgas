<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStateCityToSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('suppliers', function (Blueprint $table) {
            $table->integer('office_state')->after('office_pin_code');
            $table->integer('office_city')->after('office_state');
            $table->integer('plant_state')->after('plant_pin_code');
            $table->integer('plant_city')->after('plant_state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('suppliers', function (Blueprint $table) {
            $table->dropColumn('office_state');
            $table->dropColumn('office_city');
            $table->dropColumn('plant_state');
            $table->dropColumn('plant_city');
        });
    }
}
