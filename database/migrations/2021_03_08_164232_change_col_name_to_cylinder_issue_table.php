<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColNameToCylinderIssueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cylinder_issue', function (Blueprint $table) {
            $table->renameColumn('cyl_com_id','cus_id'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cylinder_issue', function (Blueprint $table) {
            $table->renameColumn('cus_id','cyl_com_id');
        });
    }
}
