<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('company_name');
            $table->string('contact_person');
            $table->string('email')->nullable();
            $table->string('mob_no');
            $table->string('alternate_mob_no')->nullable();
            $table->longText('office_address');
            $table->string('office_pin_code')->nullable();
            $table->longText('plant_address');
            $table->string('plant_pin_code')->nullable();
            $table->string('gst_no');
            $table->string('pan_no')->nullable();
            $table->longText('remark')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
