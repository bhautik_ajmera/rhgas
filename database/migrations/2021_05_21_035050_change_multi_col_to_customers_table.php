<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeMultiColToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('contact_person')->nullable()->default(null)->change();
            $table->longText('address')->nullable()->default(null)->change();
            $table->integer('state_id')->nullable()->default(null)->change();
            $table->integer('city_id')->nullable()->default(null)->change();
            $table->string('mob_no')->nullable()->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('contact_person');
            $table->longText('address');
            $table->integer('state_id');
            $table->integer('city_id');
            $table->string('mob_no');
        });
    }
}
