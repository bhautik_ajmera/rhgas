<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoice', function (Blueprint $table) {
            $table->float('cgst6', 8, 2)->nullable()->default(null)->after('row_rent_total');
            $table->float('sgst6', 8, 2)->nullable()->default(null)->after('cgst6');
            $table->float('cgst9', 8, 2)->nullable()->default(null)->after('sgst6');
            $table->float('sgst9', 8, 2)->nullable()->default(null)->after('cgst9');
            $table->float('net_total', 8, 2)->nullable()->default(null)->after('sgst9');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice', function (Blueprint $table) {
            $table->dropColumn('cgst6');
            $table->dropColumn('sgst6');
            $table->dropColumn('cgst9');
            $table->dropColumn('sgst9');
            $table->dropColumn('net_total');
        });
    }
}
