<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransTotalToCylinderIssueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cylinder_issue', function (Blueprint $table) {
            $table->string('trans')->nullable()->default(null)->after('remark');
            $table->double('total',8,2)->nullable()->default(0.00)->after('trans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cylinder_issue', function (Blueprint $table) {
            $table->dropColumn('trans');
            $table->dropColumn('total');
        });
    }
}
