<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveSupplierIdToCylindersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cylinders', function (Blueprint $table) {
            $table->dropForeign('supplier_fk_3131410');
            $table->dropColumn('supplier_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cylinders', function (Blueprint $table) {
            $table->foreign('supplier_id', 'supplier_fk_3131410')->references('id')->on('suppliers');
            $table->unsignedBigInteger('supplier_id');
        });
    }
}
