<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveVotesToCylinderCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cylinder_companies', function (Blueprint $table) {
            $table->dropColumn('mobile_no');
            $table->dropColumn('address');
            $table->dropColumn('pin_code');
            $table->dropColumn('state_id');
            $table->dropColumn('city_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cylinder_companies', function (Blueprint $table) {
            $table->string('mobile_no');
            $table->longText('address');
            $table->string('pin_code')->nullable();
            $table->integer('state_id')->after('pin_code');
            $table->integer('city_id')->after('state_id');
        });
    }
}
