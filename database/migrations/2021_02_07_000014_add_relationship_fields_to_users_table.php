<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToUsersTable extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('city_id');
            $table->foreign('city_id', 'city_fk_3137412')->references('id')->on('cities');
            $table->unsignedBigInteger('state_id');
            $table->foreign('state_id', 'state_fk_3137414')->references('id')->on('states');
        });
    }
}
