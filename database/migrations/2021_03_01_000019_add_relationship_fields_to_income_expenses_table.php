<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToIncomeExpensesTable extends Migration
{
    public function up()
    {
        Schema::table('income_expenses', function (Blueprint $table) {
            $table->unsignedBigInteger('branch_id');
            $table->foreign('branch_id', 'branch_fk_3317270')->references('id')->on('branches');
        });
    }
}
