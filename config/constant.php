<?php

return [
	'branchCodePrefix'      => 'B',
	'cusCodePrefix'         => 'C',
	'supCodePrefix'         => 'S',
	'gasCodePrefix'         => 'G',
	'userCodePrefix'        => 'E',
	'cylinderCompanyPrefix' => 'CC',
	'poNumPrefix'           => 'PN',
	'sysChalNoPrefix'       => 'CHN',
	'refSysChalNoPrefix'    => 'RCHN',
	'per'                   => [
	''                      => 'Select Per',
	'1'                     => 'Unit',
	'2'                     => 'KG'
	],
	'invNoPrefix'           => 'RHGINV',
	'invGST'                => '18%',
	'invTax'				=> [
		''   => 'Select',
		'12' => '12%',
		'18' => '18%'
	],
	'gasGST'				=> [
		''   => 'Select',
		'12' => '12%',
		'18' => '18%'
	]
];