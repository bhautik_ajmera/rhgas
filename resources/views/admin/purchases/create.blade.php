@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.purchase.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.purchases.store") }}" enctype="multipart/form-data" id="purFrm">
            @csrf

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="branch_id" class="required">{{ trans('cruds.purchase.fields.branch') }}</label>
                        <select class="form-control select2 {{ $errors->has('branch_id') ? 'is-invalid' : '' }}" name="branch_id" id="branch_id">
                            @foreach($branches as $id => $branch)
                                <option value="{{ $id }}" {{ old('branch_id') == $id ? 'selected' : '' }}>{{ $branch }}</option>
                            @endforeach
                        </select>
                        <div id="branchError"></div>
                        @if($errors->has('branch_id'))
                            <span class="text-danger">{{ 'The branch field is required.' }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.purchase.fields.branch_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="supplier_name">{{ trans('cruds.purchase.fields.supplier_name') }}</label>
                        <input class="form-control {{ $errors->has('supplier_name') ? 'is-invalid' : '' }}" type="text" name="supplier_name" id="supplier_name" value="{{ old('supplier_name', '') }}" />
                        @if($errors->has('supplier_name'))
                            <span class="text-danger">{{ $errors->first('supplier_name') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.purchase.fields.supplier_name_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="date">{{ trans('cruds.purchase.fields.date') }}</label>
                        <input class="form-control datepick {{ $errors->has('date') ? 'is-invalid' : '' }}" type="text" name="date" id="date" value="{{ old('date') }}" readonly="readonly" />
                        @if($errors->has('date'))
                            <span class="text-danger">{{ $errors->first('date') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.purchase.fields.date_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="invoice_no">{{ trans('cruds.purchase.fields.invoice_no') }}</label>
                        <input class="form-control text-uppercase {{ $errors->has('invoice_no') ? 'is-invalid' : '' }}" type="text" name="invoice_no" id="invoice_no" value="{{ old('invoice_no', '') }}" />
                        @if($errors->has('invoice_no'))
                            <span class="text-danger">{{ $errors->first('invoice_no') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.purchase.fields.invoice_no_helper') }}</span>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{ trans('cruds.purchase.fields.file') }}
                        </label>
                        
                        <!-- all image tpe,pdf and xls  -->
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="file" id="file" accept="image/*,application/pdf,application/vnd.ms-excel" />
                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                            </div>
                        </div>
                        <span id="selFileName"></span>
                    </div>
                </div>
            </div>

            <hr/>
        
            <ul>
                @if($errors->has('product.*'))
                    <li>
                        <span class="text-danger">{{ 'The Product field is required.' }}</span>
                    </li>
                @endif
                
                @if($errors->has('qty.*'))
                    <li>
                        <span class="text-danger">{{ 'The Qty field is required.' }}</span>
                    </li>
                @endif

                @if($errors->has('rate.*'))
                    <li>
                        <span class="text-danger">{{ 'The Rate field is required.' }}</span>
                    </li>
                @endif

                @if($errors->has('tamount.*'))
                    <li>
                        <span class="text-danger">{{ 'The Total Amount field is required.' }}</span>
                    </li>
                @endif
            </ul>

            <div class="row">
            <div id="test" class="col-sm-9">
                <table class="table table-striped table-sm" id="listing">
                    <thead>
                        <tr>
                            <th scope="col" width="30%">
                                <center>
                                    Product<span class="text text-danger">*</span>
                                </center>
                            </th>
                            
                            <th scope="col" width="10%">
                                <center>
                                    Qty<span class="text text-danger">*</span>
                                </center>
                            </th>
                      
                            <th scope="col" width="10%">
                                <center>
                                    Rate<span class="text text-danger">*</span>
                                </center>
                            </th>
                            
                            <th scope="col" width="20%">
                                <center>
                                    Per
                                </center>
                            </th>

                            <th scope="col" width="13%">
                                <center>
                                    Total <span class="text text-danger">*</span>
                                </center>
                            </th>

                            <th scope="col" width="10%"></th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>
                                <input type="text" class="form-control" name="product[]"autocomplete="off" placeholder="Product" />
                            </td>

                            <td>
                                <input type="text" class="form-control allownumericwithoutdecimal qty" name="qty[]" id="qty_0" data-index="0" autocomplete="off" placeholder="Qty" maxlength="4" />
                            </td>

                            <td>
                                <input type="text" class="form-control allownumericwithdecimal rate" name="rate[]" id="rate_0" data-index="0" autocomplete="off" placeholder="Rate" maxlength="5" />
                            </td>

                            <td>
                                <select class="form-control" name="per[]">
                                    @foreach(config('constant.per') as $perKey => $perValue)
                                        <option value="{{$perKey}}">
                                            {{$perValue}}
                                        </option>
                                    @endforeach
                                </select>
                            </td>

                            <td>
                                <input type="text" class="form-control allownumericwithdecimal" name="tamount[]" id="tamount_0" autocomplete="off" placeholder="Total Amount" readonly="readonly" value="0" />
                            </td>

                            <td>
                                <button type="button" class="btn btn-warning" id="addMore" title="Add More">
                                    <i class="far fas fa-plus-circle"></i>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-sm-3">
                <center><h5><u>Purchase Summery</u></h5></center>

                <table class="table">
                    <tr class="table-info">
                        <td>Subtotal</td>
                        <td>
                            <input type="text" name="subtotal" id="subtotal" class="form-control" autocomplete="off" readonly="required" value="0.00" />
                        </td>
                    </tr>

                    <tr>
                        <td>SGST</td>
                        <td>9 %</td>
                    </tr>

                    <tr>
                        <td>CGST</td>
                        <td>9 %</td>
                    </tr>

                    <tr>
                        <td>Adjustment</td>
                        <td>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="increment">
                                        <i class="fas fa-arrow-alt-circle-up" title="Round Up Value"></i>
                                    </span>
                                </div>
                                
                                <div class="input-group-append ml-2" id="decrement">
                                    <div class="input-group-text">
                                        <i class="fas fa-arrow-alt-circle-down" title="Round Down Value"></i>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr class="table-success">
                        <td>Grand Total</td>
                        <td>
                            <input type="text" name="grandtotal" id="grandtotal" class="form-control" autocomplete="off" readonly="required" value="0.00" />
                        </td>
                    </tr>
                </table>
            </div>
            </div>

            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.purchases.index')}}" class="btn btn-default">
                    {{ trans('global.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/purchase/add_edit.js?'.time())}}" ></script>
@endsection