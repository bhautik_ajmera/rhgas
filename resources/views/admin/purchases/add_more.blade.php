<tr id="{{'newPurchase_'.$count}}">
    <td>
        <input type="text" class="form-control" name="product[]"autocomplete="off" placeholder="Product" />
    </td>

    <td>
        <input type="text" class="form-control allownumericwithoutdecimal qty" name="qty[]" id="{{'qty_'.$count}}" data-index="{{$count}}" autocomplete="off" placeholder="Qty" maxlength="4" />
    </td>

    <td>
        <input type="text" class="form-control allownumericwithdecimal rate" name="rate[]" id="{{'rate_'.$count}}" data-index="{{$count}}" autocomplete="off" placeholder="Rate" maxlength="5" />
    </td>

    <td>
        <select class="form-control select2" name="per[]">
            @foreach(config('constant.per') as $perKey => $perValue)
                <option value="{{$perKey}}">
                    {{$perValue}}
                </option>
            @endforeach
        </select>
    </td>

    <td>
        <input type="text" class="form-control allownumericwithdecimal" name="tamount[]" id="{{'tamount_'.$count}}" autocomplete="off" placeholder="Total Amount" value="0" readonly="readonly" />
    </td>

    <td>
        <button type="button" class="btn btn-danger remove" id="{{$count}}" title="Remove">
            <i class="far fas fa-minus-circle"></i>
        </button>
    </td>
</tr>
