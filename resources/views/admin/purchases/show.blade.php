@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('global.view') }} {{ trans('cruds.purchase.title_singular') }}
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="branch_id">
                        {{ trans('cruds.purchase.fields.branch') }}
                    </label>
                    <select class="form-control select2" name="branch_id" id="branch_id" disabled="disabled">
                        @foreach($branches as $id => $branch)
                            <option value="{{ $id }}" {{ (old('branch_id') ? old('branch_id') : $purchase->branch->id ?? '') == $id ? 'selected' : '' }}>{{ $branch }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="supplier_name">
                        {{ trans('cruds.purchase.fields.supplier_name') }}
                    </label>
                    <input class="form-control" type="text" name="supplier_name" id="supplier_name" value="{{ old('supplier_name', $purchase->supplier_name) }}" disabled="disabled" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="date">
                        {{ trans('cruds.purchase.fields.date') }}
                    </label>
                    <input class="form-control" type="text" name="date" id="date" value="{{ old('date', $purchase->date) }}" disabled="disabled" />
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="invoice_no">
                        {{ trans('cruds.purchase.fields.invoice_no') }}
                    </label>
                    <input class="form-control text-uppercase" type="text" name="invoice_no" id="invoice_no" value="{{ old('invoice_no', $purchase->invoice_no) }}" disabled="disabled" />
                </div>
            </div>
        </div>         
        
        @if(!is_null($purchase->file))
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="date">
                        {{ trans('cruds.purchase.fields.file') }}
                    </label>

                    <div>
                        <a class="btn btn-xs btn-success" href="{{ route('admin.purchase.download', $purchase->id) }}" title="Download File">
                            <i class="fa fa-download"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @endif

        <hr/>
        
        <div class="row">
        <div class="col-sm-9">
            <table class="table table-striped table-sm" id="listing">
                <thead>
                    <tr>
                        <th scope="col" width="30%">
                            <center>
                                Product
                            </center>
                        </th>
                        
                        <th scope="col" width="10%">
                            <center>
                                Qty
                            </center>
                        </th>
                  
                        <th scope="col" width="10%">
                            <center>
                                Rate
                            </center>
                        </th>
                        
                        <th scope="col" width="20%">
                            <center>
                                Per
                            </center>
                        </th>

                        <th scope="col" width="13%">
                            <center>
                                Total
                            </center>
                        </th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($purDetail as $key => $value)
                    <tr id="{{'storedDetail_'.$value->id}}">
                        <td>
                            <input type="text" class="form-control" name="product[]"autocomplete="off" placeholder="Product" value="{{$value->product}}" disabled="disabled" />
                        </td>

                        <td>
                            <input type="text" class="form-control" name="qty[]" autocomplete="off" placeholder="Qty" value="{{$value->qty}}" disabled="disabled" />
                        </td>

                        <td>
                            <input type="text" class="form-control" name="rate[]" autocomplete="off" placeholder="Rate" value="{{$value->rate}}" disabled="disabled" />
                        </td>

                        <td>
                            <select class="form-control" name="per[]" disabled="disabled">
                                @foreach(config('constant.per') as $perKey => $perValue)
                                    <option value="{{$perKey}}" {{($value->per == $perKey)?'selected':''}}>
                                        {{$perValue}}
                                    </option>
                                @endforeach
                            </select>
                        </td>

                        <td>
                            <input type="text" class="form-control" name="tamount[]" autocomplete="off" placeholder="Total Amount" value="{{$value->total_amount}}" disabled="disabled" />
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="col-sm-3">
            <center><h5><u>Purchase Summery</u></h5></center>

            <table class="table">
                <tr class="table-info">
                    <td>Subtotal</td>
                    <td>
                        {{sprintf('%.2f',$purchase->subtotal)}}
                    </td>
                </tr>

                <tr>
                    <td>SGST</td>
                    <td>9 %</td>
                </tr>

                <tr>
                    <td>CGST</td>
                    <td>9 %</td>
                </tr>

                <tr class="table-success">
                    <td>Grand Total</td>
                    <td>
                        {{sprintf('%.2f',$purchase->grandtotal)}}
                    </td>
                </tr>
            </table>
        </div>
        </div>

        <div class="form-group mt-3">
            <a class="btn btn-info" href="{{ route('admin.purchases.edit', $purchase->id) }}">
                {{ trans('global.back_to_edit') }}
            </a>

            <a href="{{route('admin.purchases.index')}}" class="btn btn-default">
                {{ trans('global.cancel') }}
            </a>
        </div>
    </div>
</div>
@endsection
