@if(isset($issueDetail))
    @if(count($issueDetail) > 0)
        <span>
            <i><b>Note: Highlighted records with red color indicate cylinder is discontinue.</b></i>
        </span>

        <div id="test">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped table-sm" id="listing">
                        <thead>
                            <tr>
                                <th scope="col" width="25%">
                                    <center>
                                        Gas
                                    </center>
                                </th>
                                
                                <th scope="col" width="40%">
                                    <center>
                                        Cylinder No
                                    </center>
                                </th>
                          
                                <th scope="col" width="25%">
                                    <center>
                                        Rate
                                    </center>
                                </th>
                                
                                <th scope="col" width="10%">
                                	<center>
                                		Received
                                	</center>
                                </th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($issueDetail as $index => $value)
                                @php
                                    $allowToChange = $value::isLocked($value->cylinder_issue_id,$value->cyl_id,$value->updated_at);

                                    $disCyl = (!$value->status && $value->dis_cyl)?true:false;
                                @endphp

                                <tr id="{{'storedDetail_'.$value->id}}" class="{{($disCyl)?'table-danger':''}}">
                                    <td>
                                        <select class="form-control gas" name="gas[]" id="{{'gas_'.$index}}" disabled="disabled">
                                            <option value="">Select Gas</option>
                                            @foreach($gases as $gid => $gname)
                                                @if($value->gas_id == $gid)
                                                    <option value="{{$gid}}" selected="selected">
                                                        {{$gname}}
                                                    </option>
                                                @else
                                                    <option value="{{$gid}}">
                                                        {{$gname}}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </td>

                                    <td>
                                        <select class="form-control cyl_no text-uppercase" name="cylNo[]" data-id="{{$index}}" id="{{'cyl_'.$index}}" disabled="disabled">
                                            <option value="">Select Cylinder No</option>
                                            @foreach($cylinder as $data)

                                                @if($value->gas_id == $data->used_for_id)

                                                    @if($value->cyl_id == $data->id)
                                                        <option value="{{$data->id}}" selected="selected">
                                                            {{$data->branch_cyl_no}}
                                                        </option>
                                                    @else
                                                        <option value="{{$data->id}}">
                                                            {{$data->branch_cyl_no}}
                                                        </option>
                                                    @endif
                                                    
                                                @endif

                                            @endforeach
                                        </select>
                                    </td>

                                    <td>
                                        <div class="input-group">
                                            <input type="text" class="form-control allownumericwithdecimal rate" name="rate[]" id="{{'rate_'.$index}}" value="{{sprintf('%.2f',$value->rate)}}" autocomplete="off" disabled="disabled" />

                                            <div class="input-group-append">
                                                <span class="input-group-text">RS</span>
                                            </div>
                                        </div>
                                    </td>

                                    <td>
                                        @if($disCyl)
                                            <input type="checkbox" class="mt-3 ml-3 status" id="{{$value->id}}" disabled="disabled" />
                                        @elseif($allowToChange)
                                            <input type="checkbox" class="mt-3 ml-3 status" id="{{$value->id}}" checked="checked" disabled="disabled" />
                                        @else
                                            @if($value->status)
                                                <input type="checkbox" class="mt-3 ml-3 status" id="{{$value->id}}" checked="checked" />
                                            @else
                                                <input type="checkbox" class="mt-3 ml-3 status" id="{{$value->id}}" />
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                            <tr>
                                <td colspan="2">
                                    <div class="text text-right mt-2">
                                        <strong>Total</strong>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="total" id="total" autocomplete="off" value="0.00" readonly="readonly" placeholder="Total" />
                                        
                                        <div class="input-group-append">
                                            <span class="input-group-text">RS</span>
                                        </div>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @else
        <div class="row">
            <div class="col-sm-12 text-center">
                <h4>{{'No Record Found!'}}</h4>
            </div>
        </div>
    @endif
@endif