<select class="form-control select2" name="sysChalNo" id="sysChalNo">
	<option value="">Select</option>
	@if(isset($cissues))
		@foreach($cissues as $id => $cissue)
			<option value="{{$cissue}}">{{$cissue}}</option>
		@endforeach
	@endif
</select>