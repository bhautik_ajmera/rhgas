@extends('layouts.admin')

@section('content')
	@if(session()->has('success'))
	    <div class="alert alert-success">
	        {{ session()->get('success') }}
	    </div>
	@endif

	@if(session()->has('error'))
	    <div class="alert alert-danger">
	        {{ session()->get('error') }}
	    </div>
	@endif

	<div class="card">
	    <div class="card-header">
	        <span>
	            {{'Cylinder Issue Received'}}
	        </span>
	    </div>

	    <div class="card-body">
	    	<form action="javascript:void(0);" method="post" id="searchFrm">
	    	@csrf
		        <div class="row">
		        	<div class="col-sm-3">
		                <div class="form-group">
		                    <label class="required">Select Branch</label>

		                    <select class="form-control select2" name="branch" id="branch">
		                    	<option value="">Select</option>
		                    	@foreach($branches as $id => $branch)
		                    		<option value="{{ $id }}">{{ $branch }}</option>
		                    	@endforeach
		                    </select>
		                    <div id="brError"></div>
	                    </div>
		            </div>

		            <div class="col-sm-3">
		                <div class="form-group">
		                    <label class="required">Select Customer</label>

		                    <div id="renderCus">
		                    	@include('admin.issue_received.customer')
		                    </div>
		                    <div id="cusError"></div>
	                    </div>
		            </div>

		        	<div class="col-sm-3">
		                <div class="form-group">
		                    <label class="required">Select System Challan No</label>

		                    <div id="renderChalNo">
		                   		@include('admin.issue_received.challan_no')
		                    </div>
		                    <div id="chaError"></div>
	                    </div>
		            </div>

		            <div class="col-sm-1">
		            	<label>&nbsp;</label>
		            	<div class="form-group">
		                    <input type="submit" class="btn btn-success" id="search" value="Search">
		                </div>
		            </div>
		        </div>
	    	</form>

	    	<div id="renderList">
                @include('admin.issue_received.list')
            </div>
	    </div>
	</div>
@endsection

@section('scripts')
	@parent
    <script src="{{ asset('js/issue_rec/issue_received.js') }}"></script>
@endsection