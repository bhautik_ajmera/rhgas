<select class="form-control select2" name="customer" id="customer" >
	<option value="">Select</option>
	
	@if(isset($customers))
	    @foreach($customers as $id => $cname)
        	<option value="{{ $id }}" {{ old('cus_id') == $id ? 'selected' : '' }}>
        		{{ $cname }}
        	</option>
	    @endforeach
	@endif
</select>