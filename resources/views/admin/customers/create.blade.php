@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route("admin.customers.store") }}" enctype="multipart/form-data" id="cusFrm">
@csrf
    <div class="card">
        <div class="card-header">
            {{ trans('global.create') }} {{ trans('cruds.customer.title_singular') }}
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="companny_name">{{ trans('cruds.customer.fields.companny_name') }}</label>
                        <input class="form-control {{ $errors->has('companny_name') ? 'is-invalid' : '' }}" type="text" name="companny_name" id="companny_name" value="{{ old('companny_name', '') }}" autocomplete="off" />
                        @if($errors->has('companny_name'))
                            <span class="text-danger">{{ $errors->first('companny_name') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.customer.fields.companny_name_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{ trans('cruds.customer.fields.code') }}
                        </label>
                        
                        <div>
                            <i>{{"Auto Generate"}}</i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="email">{{ trans('cruds.customer.fields.email') }}</label>
                        <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="text" name="email" id="email" value="{{ old('email', '') }}" autocomplete="off" />
                        @if($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.customer.fields.email_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="branch_id">{{ trans('cruds.customer.fields.branch') }}</label>
                        <select class="form-control select2 {{ $errors->has('branch_id') ? 'is-invalid' : '' }}" name="branch_id" id="branch_id" >
                            @foreach($branches as $id => $branch)
                                <option value="{{ $id }}" {{ old('branch_id') == $id ? 'selected' : '' }}>{{ $branch }}</option>
                            @endforeach
                        </select>

                        @if($errors->has('branch_id'))
                            <span class="text-danger">{{ 'The branch field is required.' }}</span>
                        @endif
                        <div id="branchError"></div>

                        <span class="help-block">{{ trans('cruds.customer.fields.branch_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="contact_person">
                            {{ trans('cruds.customer.fields.contact_person') }}
                        </label>
                        
                        <input class="form-control" type="text" name="contact_person" id="contact_person" value="{{ old('contact_person', '') }}" autocomplete="off" />
                        
                        <span class="help-block">
                            {{ trans('cruds.customer.fields.contact_person_helper') }}
                        </span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="deposit" class="required">
                            {{ trans('cruds.customer.fields.deposit') }}
                        </label>
                        
                        <input class="form-control allownumericwithdecimal {{ $errors->has('deposit') ? 'is-invalid' : '' }}" type="text" name="deposit" id="deposit" value="{{ old('deposit', '0') }}" autocomplete="off" />
                        
                        @if($errors->has('deposit'))
                            <span class="text-danger">{{ $errors->first('deposit') }}</span>
                        @endif
                        
                        <span class="help-block">
                            {{ trans('cruds.customer.fields.deposit_helper') }}
                        </span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="address">{{ trans('cruds.customer.fields.address') }}</label>
                        <textarea class="form-control" name="address" id="address" >{{ old('address') }}</textarea>
                        <span class="help-block">{{ trans('cruds.customer.fields.address_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="address2">{{ trans('cruds.customer.fields.address2') }}</label>
                        <textarea class="form-control" name="address2" id="address2" >{{ old('address2') }}</textarea>
                        <span class="help-block">{{ trans('cruds.customer.fields.address_helper2') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="pin_code">{{ trans('cruds.customer.fields.pin_code') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('pin_code') ? 'is-invalid' : '' }}" type="text" name="pin_code" id="pin_code" value="{{ old('pin_code', '') }}" autocomplete="off" maxlength="6" />
                        @if($errors->has('pin_code'))
                            <span class="text-danger">{{ $errors->first('pin_code') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.customer.fields.pin_code_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="state_id">{{ 'State' }}</label>
                        <select class="form-control select2" name="state_id" id="state_id" />
                            @foreach($states as $id => $state)
                                <option value="{{ $id }}" {{ old('state_id') == $id ? 'selected' : '' }}>{{ $state }}</option>
                            @endforeach
                        </select>
                        <span class="help-block">{{ trans('cruds.customer.fields.state_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="city_id">{{ 'City' }}</label>
                        <select class="form-control select2" name="city_id" id="city_id" />
                            @if(isset($cities))
                                @foreach($cities as $id => $city)
                                    <option value="{{ $id }}" {{ old('city_id') == $id ? 'selected' : '' }}>{{ $city }}</option>
                                @endforeach
                            @endif
                        </select>
                        <span class="help-block">{{ trans('cruds.customer.fields.city_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="mob_no">{{ trans('cruds.customer.fields.mob_no') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('mob_no') ? 'is-invalid' : '' }}" type="text" name="mob_no" id="mob_no" value="{{ old('mob_no', '') }}" maxlength="10" autocomplete="off" />
                        @if($errors->has('mob_no'))
                            <span class="text-danger">{{ $errors->first('mob_no') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.customer.fields.mob_no_helper') }}</span>
                    </div>
                </div>
            </div>
                        
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="alternate_mob_no">{{ trans('cruds.customer.fields.alternate_mob_no') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('alternate_mob_no') ? 'is-invalid' : '' }}" type="text" name="alternate_mob_no" id="alternate_mob_no" value="{{ old('alternate_mob_no', '') }}" maxlength="10" autocomplete="off" />
                        @if($errors->has('alternate_mob_no'))
                            <span class="text-danger">{{ $errors->first('alternate_mob_no') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.customer.fields.alternate_mob_no_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="gstno">{{ trans('cruds.customer.fields.gstno') }}</label>
                        <input class="form-control text-uppercase" type="text" name="gstno" id="gstno" value="{{ old('gstno', '') }}" maxlength="15" autocomplete="off" />
                        <span class="help-block">{{ trans('cruds.customer.fields.gstno_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="addhar_card_no">{{ trans('cruds.customer.fields.addhar_card_no') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('addhar_card_no') ? 'is-invalid' : '' }}" type="text" name="addhar_card_no" id="addhar_card_no" value="{{ old('addhar_card_no', '') }}" maxlength="12" autocomplete="off" />
                        @if($errors->has('addhar_card_no'))
                            <span class="text-danger">{{ $errors->first('addhar_card_no') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.customer.fields.addhar_card_no_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="addhar_card_address">{{ trans('cruds.customer.fields.addhar_card_address') }}</label>
                        <textarea class="form-control" name="addhar_card_address" id="addhar_card_address" >{{ old('addhar_card_address') }}</textarea>
                        <span class="help-block">{{ trans('cruds.customer.fields.addhar_card_address_helper') }}</span>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="transportation">
                            {{ 'Transportation' }}
                        </label>
                        
                        <input type="text" class="form-control" name="trans" id="trans" value="{{ old('trans', '') }}" maxlength="191" autocomplete="off" />
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="remark">{{ trans('cruds.customer.fields.remark') }}</label>
                        <textarea class="form-control" name="remark" id="remark">{{ old('remark') }}</textarea>
                        <span class="help-block">{{ trans('cruds.customer.fields.remark_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.customers.index')}}" class="btn btn-default">
                    {{ trans('global.cancel') }}
                </a>
            </div>
        </div>
    </div>
</form>
@endsection

@section('scripts')
    <script src="{{ asset('js/customer/add_edit.js?'.time())}}" ></script>
@endsection