@extends('layouts.admin')

@section('styles')
    <style type="text/css">
        .buttons-select-all{
            display: none;
        }
        .buttons-select-none{
            display: none;
        }
        table.dataTable tbody td.select-checkbox:before, table.dataTable tbody td.select-checkbox:after, 
        table.dataTable tbody th.select-checkbox:before, table.dataTable tbody th.select-checkbox:after {
            display: none;
        }​
    </style>
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.customer.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-info" href="{{ route('admin.customers.edit', $customer->id) }}">
                    {{ trans('global.back_to_edit') }}
                </a>

                <a class="btn btn-default" href="{{ route('admin.customers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.customer.fields.id') }}
                        </th>
                        <td>
                            {{ $customer->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.customer.fields.companny_name') }}
                        </th>
                        <td>
                            {{ $customer->companny_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.customer.fields.code') }}
                        </th>
                        <td>
                            {{ $customer->code }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.customer.fields.email') }}
                        </th>
                        <td>
                            {{ !is_null($customer->email)?$customer->email:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.customer.fields.branch') }}
                        </th>
                        <td>
                            {{ $customer->branch->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.customer.fields.contact_person') }}
                        </th>
                        <td>
                            {{ is_null($customer->contact_person)?'-':$customer->contact_person }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.customer.fields.deposit') }}
                        </th>
                        <td>
                            {{ $customer->deposit }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.customer.fields.address') }}
                        </th>
                        <td>
                            {{ is_null($customer->address)?'-':$customer->address }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.customer.fields.address2') }}
                        </th>
                        <td>
                            {{ is_null($customer->address2)?'-':$customer->address2 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.customer.fields.pin_code') }}
                        </th>
                        <td>
                            {{ !is_null($customer->pin_code)?$customer->pin_code:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ 'State' }}
                        </th>
                        <td>
                            {{ ($state)?$state->name:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ 'City' }}
                        </th>
                        <td>
                            {{ ($city)?$city->name:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.customer.fields.mob_no') }}
                        </th>
                        <td>
                            {{ $customer->mob_no }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.customer.fields.alternate_mob_no') }}
                        </th>
                        <td>
                            {{ !is_null($customer->alternate_mob_no)?$customer->alternate_mob_no:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.customer.fields.gstno') }}
                        </th>
                        <td class="text text-uppercase">
                            {{ !is_null($customer->gstno)?$customer->gstno:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.customer.fields.addhar_card_no') }}
                        </th>
                        <td>
                            {{ !is_null($customer->addhar_card_no)?$customer->addhar_card_no:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.customer.fields.addhar_card_address') }}
                        </th>
                        <td>
                            {{ !is_null($customer->addhar_card_address)?$customer->addhar_card_address:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{'Transportation' }}
                        </th>
                        <td>
                            {{ !is_null($customer->trans)?$customer->trans:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.customer.fields.remark') }}
                        </th>
                        <td>
                            {{ !is_null($customer->remark)?$customer->remark:'-' }}
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="form-group">
                <a class="btn btn-info" href="{{ route('admin.customers.edit', $customer->id) }}">
                    {{ trans('global.back_to_edit') }}
                </a>
                
                <a class="btn btn-default" href="{{ route('admin.customers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>

    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link active show" href="javascript:void(0);" role="tab" data-toggle="tab">
                {{'Gas'}}
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active show" role="tabpanel">
            @include('admin.customers.gas.list')
        </div>
    </div>
</div>
@endsection