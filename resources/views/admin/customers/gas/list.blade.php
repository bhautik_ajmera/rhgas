<div class="m-3">
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success float-right" href="{{ route('admin.customers.assign_gas',$customer->id) }}">
                {{'Assign Gas'}}
            </a>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover datatable datatable-supplierCylinders">
                    <thead>
                        <tr>
                            <th>
                                {{'Gas'}}
                            </th>
                            <th>
                                {{'Basic Rate'}}
                                (<i class="fas fa-rupee-sign fa-xs"></i>)
                            </th>
                            <th>
                                {{'Rent / Day'}}
                                (<i class="fas fa-rupee-sign fa-xs"></i>)
                            </th>
                            <th width="20%">
                                {{'Start Rent After Days'}}
                            </th>
                            <th>
                                {{'Assign Date'}}
                            </th>
                            <th>
                                &nbsp;
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($assignGas as $value)
                            <tr data-entry-id="{{$value->id}}">
                                <td>
                                    {{$value->gname}}
                                </td>
                                <td>
                                    {{$value->rate}}
                                </td>
                                <td>
                                    {{$value->rent}}
                                </td>
                                <td>
                                    {{$value->start_rent}}
                                </td>
                                <td>
                                    {{date('j M, Y',strtotime($value->created_at))}}
                                </td>
                                <td>
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.customers.edit.assign_gas', $value->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                    
                                    <a class="btn btn-xs btn-danger delete" href="javascript:void(0);" data-id="{{$value->id}}">
                                        {{ trans('global.delete') }}
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
        $(document).on("click", ".delete", function(){
            var id = $(this).attr('data-id');

            if(confirm("Are you sure?")){
                window.location.href = getsiteurl()+'/admin/customers/delete/assign/gas/'+id;
            }
        });

        $.extend(true, $.fn.dataTable.defaults, {
            orderCellsTop: true,
            order: [[ 1, 'desc' ]],
            pageLength: 10,
        });
    
        let table = $('.datatable-supplierCylinders:not(.ajaxTable)').DataTable({});
        $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        });
    });
</script>
@endsection