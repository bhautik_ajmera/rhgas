@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('cruds.customer.gas') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.customers.save.assign_gas") }}" id="assignGasFrm">
        @csrf
            <input type="hidden" name="assignGasId" id="assignGasId" value="{{$assignGas->id}}" />
            <input type="hidden" name="cusId" id="cusId" value="{{$assignGas->customer_id}}" />

            <div class="form-group">
                <label class="required" for="gas_id">{{ trans('cruds.customer.fields.gas') }}</label>
                <select class="form-control select2" name="gas_id" id="gas_id">
                    @foreach($gases as $id => $gas)
                        <option value="{{ $id }}" {{ (old('gas_id') ? old('gas_id') : $assignGas->gas_id ?? '') == $id ? 'selected' : '' }}>
                            {{ $gas }}
                        </option>
                    @endforeach
                </select>
                @if($errors->has('gas_id'))
                    <span class="text-danger">{{ 'The gas field is required.' }}</span>
                @endif
                <div id="gasError"></div>
                <span class="help-block">{{ trans('cruds.customer.fields.gas_helper') }}</span>
            </div>
            
            <div class="form-group">
                <label class="required" for="rate">{{ trans('cruds.customer.fields.rate') }}</label>
                <input class="form-control allownumericwithdecimal" type="text" name="rate" id="rate" value="{{ old('rate', $assignGas->rate) }}" maxlength="8" autocomplete="off" />
                @if($errors->has('rate'))
                    <span class="text-danger">{{ $errors->first('rate') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.customer.fields.rate_helper') }}</span>
            </div>

            <div class="form-group">
                <label class="required" for="rent">{{ trans('cruds.customer.fields.rent') }}</label>
                <input class="form-control allownumericwithoutdecimal" type="text" name="rent" id="rent" value="{{ old('rent', $assignGas->rent) }}" maxlength="3" autocomplete="off" />
                @if($errors->has('rent'))
                    <span class="text-danger">{{ $errors->first('rent') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.customer.fields.rent_helper') }}</span>
            </div>

            <div class="form-group">
                <label class="required" for="start_rent">{{ trans('cruds.customer.fields.start_rent') }}</label>
                <input class="form-control allownumericwithoutdecimal" type="text" name="start_rent" id="start_rent" value="{{ old('start_rent', $assignGas->start_rent) }}" maxlength="3" autocomplete="off" />
                @if($errors->has('start_rent'))
                    <span class="text-danger">{{ $errors->first('start_rent') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.customer.fields.start_rent_helper') }}</span>
            </div>

            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.customers.show',$assignGas->customer_id)}}" class="btn btn-default">
                    {{ trans('global.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/customer/gas/add_edit.js?'.time())}}" ></script>
@endsection