@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.user.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.users.update", [$user->id]) }}" enctype="multipart/form-data" id="userEditFrm">
            @method('PUT')
            @csrf

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="code">{{ trans('cruds.user.fields.code') }}</label>
                        <input class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}" type="text" name="code" id="code" value="{{ old('code', $user->code) }}" readonly="readonly" autocomplete="off" />
                        @if($errors->has('code'))
                            <span class="text-danger">{{ $errors->first('code') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.user.fields.code_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="name">{{ trans('cruds.user.fields.name') }}</label>
                        <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $user->name) }}" autocomplete="off" />
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.user.fields.name_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="email">{{ trans('cruds.user.fields.email') }}</label>
                        <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="text" name="email" id="email" value="{{ old('email', $user->email) }}" autocomplete="off" />
                        @if($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.user.fields.email_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="" for="password">{{ trans('cruds.user.fields.password') }}</label>
                        <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password" name="password" id="password" autocomplete="off" />
                        @if($errors->has('password'))
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.user.fields.password_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="mobile_no">{{ trans('cruds.user.fields.mobile_no') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('mobile_no') ? 'is-invalid' : '' }}" type="text" name="mobile_no" id="mobile_no" value="{{ old('mobile_no', $user->mobile_no) }}" maxlength="10" autocomplete="off" />
                        @if($errors->has('mobile_no'))
                            <span class="text-danger">{{ $errors->first('mobile_no') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.user.fields.mobile_no_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="address">{{ trans('cruds.user.fields.address') }}</label>
                        <textarea class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" name="address" id="address">{{ old('address', $user->address) }}</textarea>
                        @if($errors->has('address'))
                            <span class="text-danger">{{ $errors->first('address') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.user.fields.address_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="pin_code">{{ trans('cruds.user.fields.pin_code') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('pin_code') ? 'is-invalid' : '' }}" type="text" name="pin_code" id="pin_code" value="{{ old('pin_code', $user->pin_code) }}" maxlength="6" autocomplete="off" />
                        @if($errors->has('pin_code'))
                            <span class="text-danger">{{ $errors->first('pin_code') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.user.fields.pin_code_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="state_id">{{ trans('cruds.user.fields.state') }}</label>
                        <select class="form-control select2 {{ $errors->has('state_id') ? 'is-invalid' : '' }}" name="state_id" id="state_id">
                            @foreach($states as $id => $state)
                                <option value="{{ $id }}" {{ (old('state_id') ? old('state_id') : $user->state->id ?? '') == $id ? 'selected' : '' }}>{{ $state }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('state_id'))
                            <span class="text-danger">{{ 'The state field is required.' }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.user.fields.state_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="city_id">{{ trans('cruds.user.fields.city') }}</label>
                        <select class="form-control select2 {{ $errors->has('city_id') ? 'is-invalid' : '' }}" name="city_id" id="city_id">
                            @foreach($cities as $id => $city)
                                <option value="{{ $id }}" {{ (old('city_id') ? old('city_id') : $user->city->id ?? '') == $id ? 'selected' : '' }}>{{ $city }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('city_id'))
                            <span class="text-danger">{{ 'The city field is required.' }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.user.fields.city_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="addhar_card_no">{{ trans('cruds.user.fields.addhar_card_no') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('addhar_card_no') ? 'is-invalid' : '' }}" type="text" name="addhar_card_no" id="addhar_card_no" value="{{ old('addhar_card_no', $user->addhar_card_no) }}" maxlength="12" autocomplete="off" />
                        @if($errors->has('addhar_card_no'))
                            <span class="text-danger">{{ $errors->first('addhar_card_no') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.user.fields.addhar_card_no_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="addhar_card_address">{{ trans('cruds.user.fields.addhar_card_address') }}</label>
                        <textarea class="form-control {{ $errors->has('addhar_card_address') ? 'is-invalid' : '' }}" name="addhar_card_address" id="addhar_card_address">{{ old('addhar_card_address', $user->addhar_card_address) }}</textarea>
                        @if($errors->has('addhar_card_address'))
                            <span class="text-danger">{{ $errors->first('addhar_card_address') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.user.fields.addhar_card_address_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="ref_con_name">{{ trans('cruds.user.fields.ref_con_name') }}</label>
                        <input class="form-control {{ $errors->has('ref_con_name') ? 'is-invalid' : '' }}" type="text" name="ref_con_name" id="ref_con_name" value="{{ old('ref_con_name', $user->ref_con_name) }}" autocomplete="off" />
                        @if($errors->has('ref_con_name'))
                            <span class="text-danger">{{ $errors->first('ref_con_name') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.user.fields.ref_con_name_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="ref_con_no">{{ trans('cruds.user.fields.ref_con_no') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('ref_con_no') ? 'is-invalid' : '' }}" type="text" name="ref_con_no" id="ref_con_no" value="{{ old('ref_con_no', $user->ref_con_no) }}" maxlength="10"  autocomplete="off" />
                        @if($errors->has('ref_con_no'))
                            <span class="text-danger">{{ $errors->first('ref_con_no') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.user.fields.ref_con_no_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="ref_con_no">{{ trans('cruds.user.fields.ref_con_no') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('ref_con_no') ? 'is-invalid' : '' }}" type="text" name="ref_con_no" id="ref_con_no" value="{{ old('ref_con_no', $user->ref_con_no) }}" maxlength="10"  autocomplete="off" />
                        @if($errors->has('ref_con_no'))
                            <span class="text-danger">{{ $errors->first('ref_con_no') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.user.fields.ref_con_no_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="relation">{{ trans('cruds.user.fields.relation') }}</label>
                        <input class="form-control {{ $errors->has('relation') ? 'is-invalid' : '' }}" type="text" name="relation" id="relation" value="{{ old('relation', $user->relation) }}" autocomplete="off" />
                        @if($errors->has('relation'))
                            <span class="text-danger">{{ $errors->first('relation') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.user.fields.relation_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="roles">{{ trans('cruds.user.fields.roles') }}</label>
                        <div style="padding-bottom: 4px">
                            <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                            <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                        </div>
                        <select class="form-control select2 {{ $errors->has('roles') ? 'is-invalid' : '' }}" name="roles[]" id="roles" multiple>
                            @foreach($roles as $id => $roles)
                                <option value="{{ $id }}" {{ (in_array($id, old('roles', [])) || $user->roles->contains($id)) ? 'selected' : '' }}>{{ $roles }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('roles'))
                            <span class="text-danger">{{ $errors->first('roles') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.user.fields.roles_helper') }}</span>
                    </div>
                </div>
            </div>
                        
            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.users.index')}}" class="btn btn-default">
                    {{ trans('global.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/user/add_edit.js?'.time())}}" ></script>
@endsection