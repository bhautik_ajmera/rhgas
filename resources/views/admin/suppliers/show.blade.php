@extends('layouts.admin')

@section('styles')
    <style type="text/css">
        .buttons-select-all{
            display: none;
        }
        .buttons-select-none{
            display: none;
        }
        table.dataTable tbody td.select-checkbox:before, table.dataTable tbody td.select-checkbox:after, 
        table.dataTable tbody th.select-checkbox:before, table.dataTable tbody th.select-checkbox:after {
            display: none;
        }​
    </style>
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.supplier.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-info" href="{{ route('admin.suppliers.edit', $supplier->id) }}">
                    {{ trans('global.back_to_edit') }}
                </a>

                <a class="btn btn-default" href="{{ route('admin.suppliers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.supplier.fields.id') }}
                        </th>
                        <td>
                            {{ $supplier->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.supplier.fields.code') }}
                        </th>
                        <td>
                            {{ $supplier->code }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.supplier.fields.company_name') }}
                        </th>
                        <td>
                            {{ $supplier->company_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.supplier.fields.contact_person') }}
                        </th>
                        <td>
                            {{ $supplier->contact_person }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.supplier.fields.email') }}
                        </th>
                        <td>
                            {{ !is_null($supplier->email)?$supplier->email:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.supplier.fields.mob_no') }}
                        </th>
                        <td>
                            {{ $supplier->mob_no }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.supplier.fields.alternate_mob_no') }}
                        </th>
                        <td>
                            {{ !is_null($supplier->alternate_mob_no)?$supplier->alternate_mob_no:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.supplier.fields.office_address') }}
                        </th>
                        <td>
                            {{ $supplier->office_address }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.supplier.fields.office_pin_code') }}
                        </th>
                        <td>
                            {{ !is_null($supplier->office_pin_code)?$supplier->office_pin_code:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ 'Office State' }}
                        </th>
                        <td>
                            {{$supplier->getState($supplier->office_state)}}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ 'Office City' }}
                        </th>
                        <td>
                            {{$supplier->getCity($supplier->office_city)}}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.supplier.fields.plant_address') }}
                        </th>
                        <td>
                            {{ $supplier->plant_address }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.supplier.fields.plant_pin_code') }}
                        </th>
                        <td>
                            {{ !is_null($supplier->plant_pin_code)?$supplier->plant_pin_code:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ 'Plant State' }}
                        </th>
                        <td>
                            {{$supplier->getState($supplier->plant_state)}}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ 'Plant City' }}
                        </th>
                        <td>
                            {{$supplier->getCity($supplier->plant_city)}}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.supplier.fields.gst_no') }}
                        </th>
                        <td class="text-uppercase">
                            {{ $supplier->gst_no }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.supplier.fields.pan_no') }}
                        </th>
                        <td>
                            {{ !is_null($supplier->pan_no)?$supplier->pan_no:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.supplier.fields.remark') }}
                        </th>
                        <td>
                            {{ !is_null($supplier->remark)?$supplier->remark:'-' }}
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="form-group">
                <a class="btn btn-info" href="{{ route('admin.suppliers.edit', $supplier->id) }}">
                    {{ trans('global.back_to_edit') }}
                </a>
                
                <a class="btn btn-default" href="{{ route('admin.suppliers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <?php /*
        <li class="nav-item">
            <a class="nav-link active show" href="#cylinder_tab" role="tab" data-toggle="tab">
                <!-- {{ trans('cruds.cylinder.title') }} -->
                {{'Supplier Cyliner'}}
            </a>
        </li>
        */ ?>

        <li class="nav-item">
            <a class="nav-link active show" href="#gas_tab" role="tab" data-toggle="tab">
                {{'Gas'}}
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="cylinder_tab">
            <?php /*
            @includeIf('admin.suppliers.relationships.supplierCylinders', ['cylinders' => $supplier->supplierCylinders])
            */ ?>
        </div>

        <div class="tab-pane active show" role="tabpanel" id="gas_tab">
            @include('admin.suppliers.gas.list')
        </div>
    </div>
</div>
@endsection