@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('cruds.supplier.gas') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.suppliers.save.assign_gas") }}" id="assignGasFrm">
        @csrf
            <input type="hidden" name="assignGasId" id="assignGasId" value="{{$assignGas->id}}" />
            <input type="hidden" name="supId" id="supId" value="{{$assignGas->supplier_id}}" />

            <div class="form-group">
                <label class="required" for="gas_id">{{ trans('cruds.supplier.fields.gas') }}</label>
                <select class="form-control select2" name="gas_id" id="gas_id">
                    @foreach($gases as $id => $gas)
                        <option value="{{ $id }}" {{ (old('gas_id') ? old('gas_id') : $assignGas->gas_id ?? '') == $id ? 'selected' : '' }}>
                            {{ $gas }}
                        </option>
                    @endforeach
                </select>
                @if($errors->has('gas_id'))
                    <span class="text-danger">{{ 'The gas field is required.' }}</span>
                @endif
                <div id="gasError"></div>
                <span class="help-block">{{ trans('cruds.supplier.fields.gas_helper') }}</span>
            </div>
            
            <div class="form-group">
                <label class="required" for="rate">{{ trans('cruds.supplier.fields.rate') }}</label>
                <input class="form-control allownumericwithdecimal" type="text" name="rate" id="rate" value="{{ old('rate', $assignGas->rate) }}" maxlength="5" autocomplete="off" />
                @if($errors->has('rate'))
                    <span class="text-danger">{{ $errors->first('rate') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.supplier.fields.rate_helper') }}</span>
            </div>

            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.suppliers.show',$assignGas->supplier_id)}}" class="btn btn-default">
                    {{ trans('global.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/supplier/gas/add_edit.js?'.time())}}" ></script>
@endsection