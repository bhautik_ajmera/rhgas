@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route("admin.suppliers.store") }}" enctype="multipart/form-data" id="supFrm">
@csrf
    <div class="card">
        <div class="card-header">
            {{ trans('global.create') }} {{ trans('cruds.supplier.title_singular') }}
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{ trans('cruds.supplier.fields.code') }}
                        </label>
                        
                        <div>
                            <i>{{"Auto Generate"}}</i>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="company_name">{{ trans('cruds.supplier.fields.company_name') }}</label>
                        <input class="form-control {{ $errors->has('company_name') ? 'is-invalid' : '' }}" type="text" name="company_name" id="company_name" value="{{ old('company_name', '') }}" autocomplete="off" />
                        @if($errors->has('company_name'))
                            <span class="text-danger">{{ $errors->first('company_name') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.supplier.fields.company_name_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="contact_person">{{ trans('cruds.supplier.fields.contact_person') }}</label>
                        <input class="form-control {{ $errors->has('contact_person') ? 'is-invalid' : '' }}" type="text" name="contact_person" id="contact_person" value="{{ old('contact_person', '') }}" autocomplete="off" />
                        @if($errors->has('contact_person'))
                            <span class="text-danger">{{ $errors->first('contact_person') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.supplier.fields.contact_person_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="email">{{ trans('cruds.supplier.fields.email') }}</label>
                        <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="text" name="email" id="email" value="{{ old('email', '') }}" autocomplete="off" />
                        @if($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.supplier.fields.email_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="mob_no">{{ trans('cruds.supplier.fields.mob_no') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('mob_no') ? 'is-invalid' : '' }}" type="text" name="mob_no" id="mob_no" value="{{ old('mob_no', '') }}" maxlength="10" autocomplete="off" />
                        @if($errors->has('mob_no'))
                            <span class="text-danger">{{ $errors->first('mob_no') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.supplier.fields.mob_no_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="alternate_mob_no">{{ trans('cruds.supplier.fields.alternate_mob_no') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('alternate_mob_no') ? 'is-invalid' : '' }}" type="text" name="alternate_mob_no" id="alternate_mob_no" value="{{ old('alternate_mob_no', '') }}" maxlength="10" autocomplete="off" />
                        @if($errors->has('alternate_mob_no'))
                            <span class="text-danger">{{ $errors->first('alternate_mob_no') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.supplier.fields.alternate_mob_no_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="office_address">{{ trans('cruds.supplier.fields.office_address') }}</label>
                        <textarea class="form-control {{ $errors->has('office_address') ? 'is-invalid' : '' }}" name="office_address" id="office_address" >{{ old('office_address') }}</textarea>
                        @if($errors->has('office_address'))
                            <span class="text-danger">{{ $errors->first('office_address') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.supplier.fields.office_address_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="office_pin_code">{{ trans('cruds.supplier.fields.office_pin_code') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('office_pin_code') ? 'is-invalid' : '' }}" type="text" name="office_pin_code" id="office_pin_code" value="{{ old('office_pin_code', '') }}" maxlength="6" autocomplete="off" />
                        @if($errors->has('office_pin_code'))
                            <span class="text-danger">{{ $errors->first('office_pin_code') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.supplier.fields.office_pin_code_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="office_state">{{ 'Office State' }}</label>
                        <select class="form-control select2 {{ $errors->has('office_state') ? 'is-invalid' : '' }}" name="office_state" id="office_state" />
                            @foreach($states as $id => $state)
                                <option value="{{ $id }}" {{ old('office_state') == $id ? 'selected' : '' }}>{{ $state }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('office_state'))
                            <span class="text-danger">{{ 'The state field is required.' }}</span>
                        @endif
                        <div id="officeStateError"></div>
                        <span class="help-block">{{ trans('cruds.supplier.fields.office_state_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="office_city">{{ 'Office City' }}</label>
                        <select class="form-control select2 {{ $errors->has('office_city') ? 'is-invalid' : '' }}" name="office_city" id="office_city" />
                            @if(isset($cities))
                                @foreach($cities as $id => $city)
                                    <option value="{{ $id }}" {{ old('office_city') == $id ? 'selected' : '' }}>{{ $city }}</option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has('office_city'))
                            <span class="text-danger">{{ 'The city field is required.' }}</span>
                        @endif
                        <div id="officeCityError"></div>
                        <span class="help-block">{{ trans('cruds.supplier.fields.office_city_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="plant_address">{{ trans('cruds.supplier.fields.plant_address') }}</label>
                        <textarea class="form-control {{ $errors->has('plant_address') ? 'is-invalid' : '' }}" name="plant_address" id="plant_address" >{{ old('plant_address') }}</textarea>
                        @if($errors->has('plant_address'))
                            <span class="text-danger">{{ $errors->first('plant_address') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.supplier.fields.plant_address_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="plant_pin_code">{{ trans('cruds.supplier.fields.plant_pin_code') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('plant_pin_code') ? 'is-invalid' : '' }}" type="text" name="plant_pin_code" id="plant_pin_code" value="{{ old('plant_pin_code', '') }}" maxlength="6" autocomplete="off" />
                        @if($errors->has('plant_pin_code'))
                            <span class="text-danger">{{ $errors->first('plant_pin_code') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.supplier.fields.plant_pin_code_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="plant_state">{{ 'Plant State' }}</label>
                        <select class="form-control select2 {{ $errors->has('plant_state') ? 'is-invalid' : '' }}" name="plant_state" id="plant_state" />
                            @foreach($states as $id => $state)
                                <option value="{{ $id }}" {{ old('plant_state') == $id ? 'selected' : '' }}>{{ $state }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('plant_state'))
                            <span class="text-danger">{{ 'The state field is required.' }}</span>
                        @endif
                        <div id="plantStateError"></div>
                        <span class="help-block">{{ trans('cruds.supplier.fields.plant_state_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="plant_city">{{ 'Plant City' }}</label>
                        <select class="form-control select2 {{ $errors->has('plant_city') ? 'is-invalid' : '' }}" name="plant_city" id="plant_city" />
                            @if(isset($cities))
                                @foreach($cities as $id => $city)
                                    <option value="{{ $id }}" {{ old('plant_city') == $id ? 'selected' : '' }}>{{ $city }}</option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has('plant_city'))
                            <span class="text-danger">{{ 'The city field is required.' }}</span>
                        @endif
                        <div id="planCityError"></div>
                        <span class="help-block">{{ trans('cruds.supplier.fields.plant_city_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="gst_no">{{ trans('cruds.supplier.fields.gst_no') }}</label>
                        <input class="form-control text-uppercase {{ $errors->has('gst_no') ? 'is-invalid' : '' }}" type="text" name="gst_no" id="gst_no" value="{{ old('gst_no', '') }}" maxlength="15" autocomplete="off" />
                        @if($errors->has('gst_no'))
                            <span class="text-danger">{{ $errors->first('gst_no') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.supplier.fields.gst_no_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="pan_no">{{ trans('cruds.supplier.fields.pan_no') }}</label>
                        <input class="form-control {{ $errors->has('pan_no') ? 'is-invalid' : '' }}" type="text" name="pan_no" id="pan_no" value="{{ old('pan_no', '') }}" maxlength="10" autocomplete="off" />
                        @if($errors->has('pan_no'))
                            <span class="text-danger">{{ $errors->first('pan_no') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.supplier.fields.pan_no_helper') }}</span>
                    </div>
                </div>
            </div>            

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="remark">{{ trans('cruds.supplier.fields.remark') }}</label>
                        <textarea class="form-control {{ $errors->has('remark') ? 'is-invalid' : '' }}" name="remark" id="remark">{{ old('remark') }}</textarea>
                        @if($errors->has('remark'))
                            <span class="text-danger">{{ $errors->first('remark') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.supplier.fields.remark_helper') }}</span>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.suppliers.index')}}" class="btn btn-default">
                    {{ trans('global.cancel') }}
                </a>
            </div>
        </div>
    </div>
</form>
@endsection

@section('scripts')
    <script src="{{ asset('js/supplier/add_edit.js?'.time())}}" ></script>
@endsection