@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.branch.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-info" href="{{ route('admin.branches.edit', $branch->id) }}">
                    {{ trans('global.back_to_edit') }}
                </a>

                <a class="btn btn-default" href="{{ route('admin.branches.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.branch.fields.id') }}
                        </th>
                        <td>
                            {{ $branch->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.branch.fields.name') }}
                        </th>
                        <td>
                            {{ $branch->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.branch.fields.code') }}
                        </th>
                        <td>
                            {{ $branch->code }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.branch.fields.contact_person') }}
                        </th>
                        <td>
                            {{ $branch->contact_person }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.branch.fields.company_mob_no') }}
                        </th>
                        <td>
                            {{ $branch->company_mob_no }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.branch.fields.personal_mob_no') }}
                        </th>
                        <td>
                            {{ !is_null($branch->personal_mob_no)?$branch->personal_mob_no:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.branch.fields.address') }}
                        </th>
                        <td>
                            {{ $branch->address }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.branch.fields.pin_code') }}
                        </th>
                        <td>
                            {{ $branch->pin_code }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ 'State' }}
                        </th>
                        <td>
                            {{ ($state)?$state->name:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.branch.fields.city') }}
                        </th>
                        <td>
                            {{ $branch->city->name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-info" href="{{ route('admin.branches.edit', $branch->id) }}">
                    {{ trans('global.back_to_edit') }}
                </a>
                
                <a class="btn btn-default" href="{{ route('admin.branches.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item active show">
            <a class="nav-link" href="#branch_customers" role="tab" data-toggle="tab">
                {{ trans('cruds.customer.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active show" role="tabpanel" id="branch_customers">
            @includeIf('admin.branches.relationships.branchCustomers', ['customers' => $branch->branchCustomers])
        </div>
    </div>
</div>
@endsection