<select class="form-control select2 {{ $errors->has('city_id') ? 'is-invalid' : '' }}" name="city_id" id="city_id" />
	@if(isset($cities))
	    @foreach($cities as $id => $city)
	        <option value="{{ $id }}" {{ old('city_id') == $id ? 'selected' : '' }}>{{ $city }}</option>
	    @endforeach
	@else
		<option>Please Select</option>
	@endif
</select>
