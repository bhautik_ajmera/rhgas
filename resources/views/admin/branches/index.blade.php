@extends('layouts.admin')
@section('content')

@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif

@if(session()->has('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
@endif

<div class="card">
    <div class="card-header">
        <span>
            {{ trans('cruds.branch.title_singular') }} {{ trans('global.list') }}
        </span>

        @can('branch_create')
            <a class="btn btn-success float-right" href="{{ route('admin.branches.create') }}">
                <i class="fa fa-plus"></i>
            </a>
        @endcan
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Branch">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.branch.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.branch.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.branch.fields.code') }}
                        </th>
                        <th>
                            {{ trans('cruds.branch.fields.company_mob_no') }}
                        </th>
                        <th>
                            {{ trans('cruds.branch.fields.city') }}
                        </th>
                        <th width="10%">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($branches as $key => $branch)
                        <tr data-entry-id="{{ $branch->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $branch->id ?? '' }}
                            </td>
                            <td>
                                {{ $branch->name ?? '' }}
                            </td>
                            <td>
                                {{ $branch->code ?? '' }}
                            </td>
                            <td>
                                {{ $branch->company_mob_no ?? '' }}
                            </td>
                            <td>
                                {{ $branch->city->name ?? '' }}
                            </td>
                            <td>
                                @can('branch_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.branches.show', $branch->id) }}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                @endcan

                                @can('branch_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.branches.edit', $branch->id) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                @endcan
                                

                                @can('branch_delete')
                                    <form action="{{ route('admin.branches.destroy', $branch->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('branch_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.branches.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 10,
  });
  let table = $('.datatable-Branch:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection