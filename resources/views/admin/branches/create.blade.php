@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.branch.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.branches.store") }}" enctype="multipart/form-data" id="branchFrm">
            @csrf

            <!-- <div class="form-group">
                <label class="required" for="code">{{ trans('cruds.branch.fields.code') }}</label>
                <input class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}" type="text" name="code" id="code" value="{{ old('code', '') }}" autocomplete="off" />
                @if($errors->has('code'))
                    <span class="text-danger">{{ $errors->first('code') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.branch.fields.code_helper') }}</span>
            </div> -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="form-group">
                            <label class="required" for="name">{{ trans('cruds.branch.fields.name') }}</label>
                            <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" autocomplete="off" />
                            @if($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.branch.fields.name_helper') }}</span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{ trans('cruds.branch.fields.code') }}
                        </label>
                        
                        <div>
                            <i>{{"Auto Generate"}}</i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="contact_person">{{ trans('cruds.branch.fields.contact_person') }}</label>
                        <input class="form-control {{ $errors->has('contact_person') ? 'is-invalid' : '' }}" type="text" name="contact_person" id="contact_person" value="{{ old('contact_person', '') }}" autocomplete="off" />
                        @if($errors->has('contact_person'))
                            <span class="text-danger">{{ $errors->first('contact_person') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.branch.fields.contact_person_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="company_mob_no">{{ trans('cruds.branch.fields.company_mob_no') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('company_mob_no') ? 'is-invalid' : '' }}" type="text" name="company_mob_no" id="company_mob_no" value="{{ old('company_mob_no', '') }}" autocomplete="off" maxlength="10" />
                        @if($errors->has('company_mob_no'))
                            <span class="text-danger">{{ $errors->first('company_mob_no') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.branch.fields.company_mob_no_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="personal_mob_no">{{ trans('cruds.branch.fields.personal_mob_no') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('personal_mob_no') ? 'is-invalid' : '' }}" type="text" name="personal_mob_no" id="personal_mob_no" value="{{ old('personal_mob_no', '') }}" autocomplete="off" maxlength="10" />
                        @if($errors->has('personal_mob_no'))
                            <span class="text-danger">{{ $errors->first('personal_mob_no') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.branch.fields.personal_mob_no_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="address">{{ trans('cruds.branch.fields.address') }}</label>
                        <textarea class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" name="address" id="address" rows="3" cols="3" maxlength="255" />{{ old('address') }}</textarea>
                        @if($errors->has('address'))
                            <span class="text-danger">{{ $errors->first('address') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.branch.fields.address_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="pin_code">{{ trans('cruds.branch.fields.pin_code') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('pin_code') ? 'is-invalid' : '' }}" type="text" name="pin_code" id="pin_code" value="{{ old('pin_code', '') }}" autocomplete="off" maxlength="6" />
                        @if($errors->has('pin_code'))
                            <span class="text-danger">{{ $errors->first('pin_code') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.branch.fields.pin_code_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="state_id">{{ 'State' }}</label>
                        <select class="form-control select2 {{ $errors->has('state_id') ? 'is-invalid' : '' }}" name="state_id" id="state_id" />
                            @foreach($states as $id => $state)
                                <option value="{{ $id }}" {{ old('state_id') == $id ? 'selected' : '' }}>{{ $state }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('state_id'))
                            <span class="text-danger">{{ 'The state field is required.' }}</span>
                        @endif
                        <div id="stateError"></div>
                        <span class="help-block">{{ trans('cruds.branch.fields.state_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="city_id">{{ trans('cruds.branch.fields.city') }}</label>

                        <select class="form-control select2 {{ $errors->has('city_id') ? 'is-invalid' : '' }}" name="city_id" id="city_id" />
                            @if(isset($cities))
                                @foreach($cities as $id => $city)
                                    <option value="{{ $id }}" {{ old('city_id') == $id ? 'selected' : '' }}>{{ $city }}</option>
                                @endforeach
                            @endif
                        </select>

                        <!-- <div id="renderCities"> -->
                            <!-- @include('admin.branches.cities') -->
                        <!-- </div> -->
                        
                        @if($errors->has('city_id'))
                            <span class="text-danger">{{ 'The city field is required.' }}</span>
                        @endif

                        <div id="cityError"></div>
                        <span class="help-block">{{ trans('cruds.branch.fields.city_helper') }}</span>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.branches.index')}}" class="btn btn-default">
                    {{ trans('global.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/branch/add_edit.js?'.time())}}" ></script>
@endsection