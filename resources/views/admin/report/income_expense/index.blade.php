@extends('layouts.admin')

@section('styles')
	<style type="text/css">
		.buttons-select-all,.buttons-select-none{
			display: none;
		}

		table.dataTable tbody td.select-checkbox:before, table.dataTable tbody th.select-checkbox:before{
			display: none;
		}
	</style>
    <link href="{{ asset('css/daterangepicker.css') }}" rel="stylesheet">
@endsection

@section('content')
@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif

@if(session()->has('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
@endif

<div class="card">
    <div class="card-header">
        <span>
            {{'Income Expense Report'}}
        </span>
    </div>

    <div class="card-body">
    	<form action="{{route('admin.report.income-expense')}}" method="post" id="searchFrm">
        @csrf
	    	<div class="row">
	            <div class="col-sm-3">
	                <div class="form-group">
	                    <label class="required">Select Branch</label>

	                    <select class="form-control select2 {{ $errors->has('branch_id') ? 'is-invalid' : '' }}" name="branch_id" id="branch_id" >
                            @foreach($branches as $id => $branch)
                                <option value="{{ $id }}" {{ (old('branch_id') ? old('branch_id') : $selBranchId ?? '') == $id ? 'selected' : '' }}>{{ $branch }}</option>
                            @endforeach
                        </select>

                        <div id="branchError"></div>
                        @if($errors->has('branch_id'))
	                        <span class="text-danger">{{ 'The branch field is required.' }}</span>
	                    @endif
	                </div>
	            </div>

	            <div class="col-sm-3">
	                <div class="form-group">
	                    <label class="required">Select Date</label>
	                    
	                    <input type="text" class="form-control" name="startEndDate" id="startEndDate" readonly="readonly" value="{{isset($selDate)?$selDate:''}}" />

	                    @if($errors->has('startEndDate'))
	                        <span class="text-danger">{{ 'The date field is required.' }}</span>
	                    @endif
	                </div>
	            </div>

	            <div class="col-sm-1">
	            	<label>&nbsp;</label>
	            	<div class="form-group">
	                    <input type="submit" class="btn btn-success" id="search" value="Search">
	                </div>
	            </div>
	        </div>
    	</form>
        
        <div class="row">
        	<div class="col-sm-6">
        		<div>
	        		<span><b><center>Income Detail</center></b></span>
        		</div>
        		<hr/>

        		<div class="table-responsive">
		            <table class="incomeListing table table-bordered table-striped table-hover datatable">
		                <thead>
		                    <tr>
		                        <th>
		                            {{'Description'}}
		                        </th>
		                        <th>
		                            {{'Rate'}}
		                        </th>
		                    </tr>
		                </thead>
		                <tbody>
		                	@if(isset($incomeDetail))
			                    @foreach($incomeDetail as $value)
			                        <tr data-entry-id="{{$value->id}}">
			                            <td>
			                                {{$value->desc}}
			                            </td>
			                            <td>
			                                {{$value->rate}}
			                            </td>
			                        </tr>
			                    @endforeach
		                    @endif
		                </tbody>
		            </table>
		        </div>
        	</div>

        	<div class="col-sm-6">
        		<div>
        			<span><b><center>Expense Detail</center></b></span>
        		</div>
        		<hr/>

        		<div class="table-responsive">
		            <table class="expenseListing table table-bordered table-striped table-hover datatable">
		                <thead>
		                    <tr>
		                        <th>
		                            {{'Description'}}
		                        </th>
		                        <th>
		                            {{'Rate'}}
		                        </th>
		                    </tr>
		                </thead>
		                <tbody>
		                	@if(isset($expenseDetail))
			                    @foreach($expenseDetail as $value)
			                        <tr data-entry-id="{{$value->id}}">
			                            <td>
			                                {{$value->desc}}
			                            </td>
			                            <td>
			                                {{$value->rate}}
			                            </td>
			                        </tr>
			                    @endforeach
		                    @endif
		                </tbody>
		            </table>
		        </div>
        	</div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
	@parent
    <script src="{{ asset('js/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/report/income_expense.js') }}"></script>
@endsection