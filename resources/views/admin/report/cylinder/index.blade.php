@extends('layouts.admin')

@section('styles')
    <style type="text/css">
        .buttons-select-all,.buttons-select-none{
            display: none;
        }

        table.dataTable tbody td.select-checkbox:before, table.dataTable tbody th.select-checkbox:before{
            display: none;
        }
    </style>
@endsection

@section('content')
@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif

@if(session()->has('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
@endif

<div class="card">
    <div class="card-header">
        <span>
            {{'Cylinder Report'}}
            <input type="hidden" name="cusName" id="cusName" value="{{($customer)?$customer->companny_name:''}}" />
        </span>
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Ga">
                <thead>
                    <tr>
                        <th>{{'No'}}</th>
                        <th id="thcylno">{{'Cylinder No'}}</th>
                        <th id="thstatus">{{'Status'}}</th>
                        <th id="thgas">{{'Gas' }}</th>
                        <th id="thrcn">{{'Refilling Challan No'}}</th>
                        <th id="thsup">{{'Supplier'}}</th>
                        <th id="thrd">{{'Refilling Date'}}</th>
                        <th id="thinsd">{{'In-Stock Date'}}</th>
                        <!-- <th>{{'Status'}}</th> -->
                        <th id="thicn">{{'Issue Challan No'}}</th>
                        <th id="thcus">{{'Customer'}}</th>
                        <th id="thisd">{{'Issued Date'}}</th>
                        <th id="thred">{{'Received Date'}}</th>
                        <!-- <th>{{'Status'}}</th> -->
                        <th>{{'Rent/Day'}}</th>
                        <th>{{'Start Rent After Days'}}</th>
                        <th>{{'Holding Days'}}</th>
                        <th>{{'Holding Charge'}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $key => $value)
                        @php
                            $value = $value::customerCycle($value);
                        @endphp

                        <tr class="{{($value->disp_as_dis_cyl)?'table-danger':''}}">
                            <td class="table-success">{{$key+1}}</td>
                            
                            <td class="text text-uppercase table-success">
                                {{$value->branch_cyl_no}}
                            </td>                            

                            <td class="table-success text-capitalize">
                                @if($value->disp_as_dis_cyl)
                                    {{'(Discontinue Cylinder)'}}
                                    <br/>
                                @endif
                                
                                {{($value->received_date == '-')?$value->cstatus:'-'}}
                            </td>

                            <td class="table-success">
                                {{isset($value->gname)?$value->gname:'-'}}
                            </td>

                            <td class="table-primary">
                                {{isset($value->refchal)?$value->refchal:'-'}}
                            </td>
                            
                            <td class="table-primary">
                                {{isset($value->sname)?$value->sname:'-'}}
                            </td>
                            
                            <td class="table-primary">
                                {{isset($value->date)?date('d/m/Y',strtotime($value->date)):'-'}}
                            </td>
                            
                            <td class="table-primary">
                                @if($value->status)
                                    {{date('d/m/Y',strtotime($value->rd_ua))}}
                                @else
                                    {{'-'}}
                                @endif
                            </td>
                            
                            <!-- <td class="table-primary">
                                @if(isset($value->cyl_id))
                                    {{($value->status)?'In-Stock':'Refilling'}}
                                @else
                                    {{'-'}}
                                @endif
                            </td> -->
                            
                            <td class="table-warning">
                                {{$value->issue_chal}}
                            </td>

                            <td class="table-warning">
                                {{$value->cname}}
                            </td>
                            
                            <td class="table-warning">
                                {{$value->issue_date}}
                            </td>
                            
                            <td class="table-warning">
                                {{$value->received_date}}
                            </td>
                            
                            <!-- <td class="table-warning">
                                {{$value->status1}}
                            </td> -->

                            <td>
                                <i class="fas fa-rupee-sign fa-xs"></i>
                                {{$value->rent_per_day}}
                            </td>

                            <td>
                                {{$value->start_rent_after_days}}
                            </td>

                            <td>
                                {{$value->holiding_days}}
                            </td>

                            <td>
                                <i class="fas fa-rupee-sign fa-xs"></i>
                                {{$value->holding_charge}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@parent
<script>
    $(function () {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

        $('.datatable-Ga thead tr').clone(true).appendTo('.datatable-Ga thead');
        $('.datatable-Ga thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
     
            $('input', this ).on( 'keyup change', function (){
                if(table.column(i).search() !== this.value){
                    table.column(i).search( this.value).draw();
                }
            });
        });

        $.extend(true, $.fn.dataTable.defaults, {
            orderCellsTop: true,
            fixedHeader: true,
            // order: [[ 1, 'desc' ]],
            // oSearch:{'sSearch':''},
            pageLength: 10,
        });
    
        let table = $('.datatable-Ga:not(.ajaxTable)').DataTable({ buttons: dtButtons });

        var cusName = $('#cusName').val();
        if(cusName != ''){
            table.column(9).search(cusName).draw();
        }
    })
</script>
@endsection