@extends('layouts.admin')

@section('styles')
<style type="text/css">
    .buttons-select-all,.buttons-select-none{
        display: none;
    }

    table.dataTable tbody td.select-checkbox:before, table.dataTable tbody th.select-checkbox:before{
        display: none;
    }
</style>
@endsection

@section('content')
@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif

@if(session()->has('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
@endif

<div class="card">
    <div class="card-header">
        <span>
            {{'Invoice List'}}
        </span>

        @can('customer_invoice_create')
            <a class="btn btn-success float-right" href="{{ route('admin.invoice.create') }}">
                <i class="fa fa-plus"></i>
            </a>
        @endcan
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Ga">
                <thead>
                    <tr>
                        <th>
                            Invoice No
                        </th>
                        <th>
                            Invoice Type
                        </th>
                        <th>
                            Invoice Date
                        </th>
                        <th>
                            Company Name
                        </th>
                        <th>
                            Mobile Number
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($invoices as $invoice)
                        <tr id="{{'storedDetail_'.$invoice->id}}">
                            <td>{{$invoice->invoice_no}}</td>
                            <td>{{($invoice->invoice_type)?'Normal Invoice':'Rent Invoice'}}</td>
                            <td>{{date('d/m/Y',strtotime($invoice->invoice_date))}}</td>
                            <td>{{$invoice->to_name}}</td>
                            <td>{{$invoice->to_mob}}</td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{route('admin.invoice.view',$invoice->id)}}" title="View Invoice">
                                    <i class="fa fa-eye"></i>
                                </a>

                                <a class="btn btn-xs btn-info" href="{{route('admin.invoice.download',$invoice->id)}}" title="Download Invoice">
                                    <i class="fa fa-download"></i>
                                </a>

                                <button class="btn btn-xs btn-danger delete" id="{{$invoice->id}}" data-invtype="{{$invoice->invoice_type}}" title="Delete Invoice">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@parent
    <script>
        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    
            $.extend(true, $.fn.dataTable.defaults, {
                orderCellsTop: true,
                // order: [[ 1, 'desc' ]],
                pageLength: 10,
                columnDefs: [{ 
                    'orderable': false, 'targets': [5]
                }]
            });
            
            $('.datatable-Ga:not(.ajaxTable)').DataTable({ buttons: dtButtons });

            $(document).on("click", ".delete", function(){
                var id      = $(this).attr('id');
                var invType = $(this).attr('data-invtype');

                Swal.fire({
                  title: 'Are you sure you want to delete?',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes'
                }).then((result) => {
                    if(result.value) {
                        $.ajax({
                            type: "GET",
                            url: getsiteurl()+'/admin/invoice/delete',
                            data:{'id':id,'invType':invType},
                            success: function(response) {
                                Swal.fire({
                                    type: response.status,
                                    title: response.message,
                                    showConfirmButton:false,
                                    timer: 1000
                                });

                                if(response.status == 'success'){
                                    $('#storedDetail_'+id).remove();
                                }
                            }
                        });
                    }
                });
            });
        })
    </script>
@endsection