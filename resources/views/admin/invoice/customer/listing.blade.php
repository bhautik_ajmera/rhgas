@php $lastTotal = $lastRentTotal = 0; @endphp

<div class="row">
    <div class="col-12 table-responsive">
        <table class="table table-striped" id="listing">
            <thead>
                <tr>
                    <th>Challan No</th>
                    <th>Transportation</th>
                    <th>Gas</th>
                    <th>HSN Code</th>
                    <th>Cylinder No</th>
                    <th>Issued Date</th>

                    @if($itype)
                        <th>Price Per Bottle <i class="fas fa-rupee-sign fa-xs"></i></th>
                        <th>Qty</th>
                        <th>Total <i class="fas fa-rupee-sign fa-xs"></i></th>
                    @else
                        <th>Received Date</th>
                        <th>Rent/Day <i class="fas fa-rupee-sign fa-xs"></i></th>
                        <th>Start Rent After Days</th>
                        <th>Late Days</th>
                        <th>Total <i class="fas fa-rupee-sign fa-xs"></i></th>
                    @endif

                    <th>Tax</th>
                </tr>
            </thead>

            <tbody>
                @if(isset($cylIssueDetail) && count($cylIssueDetail) > 0)            
                    @foreach($cylIssueDetail as $value)
                        @php
                            $issReDate = $value::getIssedReceivedDate($value->cylinder_issue_id);

                            if(!$itype){
                                $temp = $value::calRent($issReDate->delivery_date,$value->updated_at,$value->rent,$value->start_rent,true);
                            }
                        @endphp

                        <tr class="info">
                            <td>
                                {{substr(strtoupper(config('constant.sysChalNoPrefix')), 0, 3).sprintf("%03d",$value->cylinder_issue_id)}}
                            </td>
                            <input type="hidden" name="chalNo[]" value="{{substr(strtoupper(config('constant.sysChalNoPrefix')), 0, 3).sprintf("%03d",$value->cylinder_issue_id)}}" />

                            <td width="13%">
                                {{$value->trans}}
                            </td>
                            <input type="hidden" name="transDetail[]" value="{{$value->trans}}" />

                            <td>
                                {{$value->gname}}
                            </td>
                            <input type="hidden" name="gas[]" value="{{$value->gname}} "/>

                            <td>
                                {{$value->hsn_code}}
                            </td>
                            <input type="hidden" name="hsnCode[]" value="{{$value->hsn_code}} "/>

                            <td class="text text-uppercase">
                                {{$value->branch_cyl_no}}
                            </td>
                            <input type="hidden" name="cylNo[]" value="{{strtoupper($value->branch_cyl_no)}} "/>

                            <td>
                                {{date('d/m/Y',strtotime($issReDate->delivery_date))}}
                            </td>
                            <input type="hidden" name="issueDate[]" value="{{date('d/m/Y',strtotime($issReDate->delivery_date))}} "/>

                            @if($itype)
                                <td>
                                    {{sprintf('%.2f',$value->rate)}}
                                </td>
                                <input type="hidden" name="total[]" value="{{sprintf('%.2f',$value->rate)}}" />

                                <td>{{'1'}}</td>

                                <td>
                                    {{sprintf('%.2f',$value->rate)}}
                                </td>
                            @else
                                <td>
                                    {{date('d/m/Y',strtotime($value->updated_at))}}
                                </td>
                                <input type="hidden" name="recDate[]" value="{{date('d/m/Y',strtotime($value->updated_at))}} "/>

                                <td>
                                    {{$value->rent}}
                                </td>
                                <input type="hidden" name="rentPerDay[]" value="{{$value->rent}}" />

                                <td>
                                    {{$value->start_rent}}
                                </td>
                                <input type="hidden" name="startRent[]" value="{{$value->start_rent}}" />

                                <td>
                                    {{ $temp['holiding_days'] }}
                                </td>
                                <input type="hidden" name="lateDays[]" value="{{$temp['holiding_days']}}" />
                                
                                <td>
                                    {{sprintf('%.2f',$temp['holding_charge'])}}
                                </td>
                                <input type="hidden" name="rentTotal[]" value="{{sprintf('%.2f',$temp['holding_charge'])}}" />
                            @endif

                            <td>
                                <select class="form-control tax" id="{{$value->id}}" name="tax[]">
                                    <option value="">Select</option>
                                    @if(!is_null($value->ggst))
                                        <option value="{{$value->ggst}}" selected="selected">
                                            {{$value->ggst.'%'}}
                                        </option>
                                    @endif
                                </select>
                            </td>
                        </tr>
                        
                        @php 
                            $lastTotal+=$value->rate; 
                            $lastRentTotal+=(isset($temp))?$temp['holding_charge']:0;
                        @endphp
                    @endforeach
                    
                    <!--  -->
                    <tr class="table-info">
                        <td colspan="8">&nbsp;</td>

                        @if($itype)
                            <td><span>{{sprintf('%.2f',$lastTotal)}}</span></td>
                            <input type="hidden" name="lastTotal" id="lastTotal" value="{{sprintf('%.2f',$lastTotal)}}" />
                        @else
                            <td colspan="2">&nbsp;</td>

                            <td><span>{{sprintf('%.2f',$lastRentTotal)}}</span></td>
                            <input type="hidden" name="lastRentTotal" id="lastRentTotal" value="{{sprintf('%.2f',$lastRentTotal)}}" />
                        @endif

                        <td>&nbsp;</td>
                    </tr>
                    <!--  -->
                @else
                    <tr>
                        <td colspan="{{($itype)?10:12}}">
                            <center>{{'No Data Found'}}</center>
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    
    </div>
</div>

<div class="row">
    <div class="col-sm-3 offset-sm-9 table-responsive">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <th>Total</th>
                    <td>
                        @if($itype)
                            {{sprintf('%.2f',$lastTotal)}}
                        @else
                            {{sprintf('%.2f',$lastRentTotal)}}
                        @endif
                    </td>
                </tr>

                <tr>
                    <th>CGST (6%)</th>
                    <td id="tbl_cgst6">0.00</td>
                    <input type="hidden" name="cgst6" id="cgst6" value="0.00" />
                </tr>

                <tr>
                    <th>SGST (6%)</th>
                    <td id="tbl_sgst6">0.00</td>
                    <input type="hidden" name="sgst6" id="sgst6" value="0.00" />
                </tr>

                <tr>
                    <th>CGST (9%)</th>
                    <td id="tbl_cgst9">0.00</td>
                    <input type="hidden" name="cgst9" id="cgst9" value="0.00" />
                </tr>

                <tr>
                    <th>SGST (9%)</th>
                    <td id="tbl_sgst9">0.00</td>
                    <input type="hidden" name="sgst9" id="sgst9" value="0.00" />
                </tr>

                <tr>
                    <th>Net Total</th>
                    <td id="tbl_netTotal">{{sprintf('%.2f',($lastTotal+$lastRentTotal))}}</td>
                    <input type="hidden" name="netTotal" id="netTotal" value="{{sprintf('%.2f',($lastTotal+$lastRentTotal))}}" />
                </tr>
            </tbody>
        </table>
    </div>
</div>

