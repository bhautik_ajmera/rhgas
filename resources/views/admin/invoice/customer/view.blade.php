@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <form method="post" action="{{route('admin.invoice.download')}}">
        @csrf
        <input type="hidden" name="invoiceId" id="invoiceId" value="{{$invoice->id}}" />

        <div class="row">
            <div class="col-12">
                <label>
                    <h3>View Invoice</h3>
                </label>

                <a href="{{route('admin.invoice')}}" class="btn btn-default float-right">
                    Back
                </a>

                <button type="submit" class="btn btn-success float-right mr-1" title="Download Invoice">
                    <i class="fas fa-download"></i>
                </button>

                <select class="form-control col-sm-2 float-right mr-1" name="invoiceCopy">
                    <option value="1">Original</option>
                    <option value="2">Duplicate</option>
                    <option value="3">Triplicate</option>
                    <option value="4">Extra Copy</option>
                </select>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col-12">
            <div class="invoice p-3 mb-3">
                <div class="row">
                    <div class="col-12">
                        <div class="float-left">
                            <h5>
                                <img src="{{asset('images/logo.jpeg')}}" class="rounded-circle" alt="{{ trans('panel.site_title') }}" height="35px" />
                                <span style="margin-left: -10px;">
                                    {{ trans('panel.site_title') }}
                                </span>
                            </h5>
                        </div>

                        <div class="float-right">
                            <h5>
                                <span>
                                    <small >Invoice No: {{$invoice->invoice_no}}</small>
                                </span>
                                
                                <span>
                                    | <small>Date: {{date('d/m/Y',strtotime($invoice->invoice_date))}}</small>
                                </span>
                            </h5>
                        </div>
                    </div>
                </div>

                <div class="row invoice-info mt-3">
                    <div class="col-sm-4 invoice-col">
                        From
                        <address>
                            <strong>R H GASES</strong><br>
                            Plot No. 212 Main Road Phase-2 Dared<br>
                            Jamnagar - 361004<br>
                            Mob: 8128157173<br>
                            GST NO. - 24CVMPP0402J1ZO
                        </address>
                    </div>

                    <div class="col-sm-4 invoice-col">
                        To
                        <address>
                            <strong>{{$invoice->to_name}}</strong><br>
                            {{$invoice->to_address}}<br>
                            {{$invoice->to_city.', '.$invoice->to_state.' '.$invoice->to_pin_code}}<br>
                            Phone: {{$invoice->to_mob}} {{!is_null($invoice->to_alt_mo)?(','.$invoice->to_alt_mo):''}}<br>
                            Email: {{$invoice->to_email}}
                        </address>
                    </div>

                    <div class="col-sm-4 invoice-col">
                        <b>Customer Code: </b> {{$invoice->to_cus_code}}<br>
                        <b>Contact Person: </b> {{$invoice->to_contact_person}}<br>
                        <b>GST Number: </b> {{$invoice->to_gst_no}}<br>
                        <b>Aadhaar Card Number: </b> {{$invoice->to_aadhaar_card_no}}<br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped" id="listing">
                            <thead>
                                <tr>
                                    <th>Challan No</th>
                                    <th>Transportation</th>
                                    <th>Gas</th>
                                    <th>HSN Code</th>
                                    <th>Cylinder No</th>
                                    <th>Issue Date</th>

                                    @if($invoice->invoice_type)
                                        <th>
                                            Price Per Bottle (<i class="fas fa-rupee-sign fa-xs"></i>)
                                        </th>
                                        <th>Qty</th>
                                        <th>
                                            Total (<i class="fas fa-rupee-sign fa-xs"></i>)
                                        </th>
                                    @else
                                        <th>Received Date</th>
                                        <th>
                                            Rent/Day (<i class="fas fa-rupee-sign fa-xs"></i>)
                                        </th>
                                        <th>Start Rent After Days</th>
                                        <th>Late Days</th>
                                        <th>
                                            Total (<i class="fas fa-rupee-sign fa-xs"></i>)
                                        </th>
                                    @endif

                                    <th>Tax</th>
                                </tr>
                            </thead>
                            <tbody>                                    
                                @foreach($invDetail as $value)
                                    <tr>
                                        <td>
                                            {{$value->challan_no}}
                                        </td>

                                        <td  width="13%">
                                            {{$value->trans}}
                                        </td>

                                        <td>
                                            {{$value->gas}}
                                        </td>

                                        <td>
                                            {{$value->hsn_code}}
                                        </td>

                                        <td>
                                            {{$value->cylinder}}
                                        </td>

                                        <td>
                                            {{date('d/m/Y',strtotime($value->issued_date))}}
                                        </td>

                                        @if($invoice->invoice_type)
                                            <td>
                                                {{sprintf('%.2f',$value->price_per_bottle)}}
                                            </td>

                                            <td>
                                                {{$value->qty}}
                                            </td>

                                            <td>
                                                {{sprintf('%.2f',$value->total)}}
                                            </td>
                                        @else
                                            <td>
                                                {{date('d/m/Y',strtotime($value->received_date))}}
                                            </td>

                                            <td>
                                                {{sprintf('%.2f',$value->rate_per_day)}}
                                            </td>
                                            
                                            <td>
                                                {{$value->start_rent}}
                                            </td>
    
                                            <td>
                                                {{$value->late_days}}
                                            </td>

                                            <td>
                                                {{sprintf('%.2f',$value->rent_total)}}
                                            </td>
                                        @endif

                                        <td>
                                            {{sprintf('%.2f',$value->tax_per).' %'}}
                                        </td>
                                    </tr>
                                @endforeach

                                <tr class="table-info">
                                    <td colspan="8">&nbsp;</td>

                                    @if(!$invoice->invoice_type)
                                        <td colspan="2">&nbsp;</td>
                                    @endif

                                    <td colspan="2">
                                        <span>{{sprintf('%.2f',$invoice->row_total)}}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3 offset-sm-9 table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th>Total</th>
                                    <td>
                                        <i class="fas fa-rupee-sign fa-xs"></i>
                                        {{sprintf('%.2f',$invoice->row_total)}}
                                    </td>
                                </tr>

                                <tr>
                                    <th>CGST (6%)</th>
                                    <td>
                                        <i class="fas fa-rupee-sign fa-xs"></i>
                                        {{sprintf('%.2f',$invoice->cgst6)}}
                                    </td>
                                </tr>

                                <tr>
                                    <th>SGST (6%)</th>
                                    <td>
                                        <i class="fas fa-rupee-sign fa-xs"></i>
                                        {{sprintf('%.2f',$invoice->sgst6)}}
                                    </td>
                                </tr>

                                <tr>
                                    <th>CGST (9%)</th>
                                    <td>
                                        <i class="fas fa-rupee-sign fa-xs"></i>
                                        {{sprintf('%.2f',$invoice->cgst9)}}
                                    </td>
                                </tr>

                                <tr>
                                    <th>SGST (9%)</th>
                                    <td>
                                        <i class="fas fa-rupee-sign fa-xs"></i>
                                        {{sprintf('%.2f',$invoice->sgst9)}}
                                    </td>
                                </tr>

                                <tr>
                                    <th>Net Total</th>
                                    <td>
                                        <i class="fas fa-rupee-sign fa-xs"></i>
                                        {{sprintf('%.2f',$invoice->net_total)}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <!-- <script src="{{ asset('js/jQuery.print.js')}}" ></script> -->
@endsection