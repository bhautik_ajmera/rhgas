<div class="form-group">
    <label class="required">Challan No</label>

    <select class="select2bs4" multiple="multiple" data-placeholder="Select Challan No"
    style="width: 100%;" name="challan" id="challan">
        @if(isset($cylIssue))
            @foreach($cylIssue as $id => $chal)
                <option value="{{$id}}">{{$chal}}</option>
            @endforeach
        @endif
    </select>
</div>