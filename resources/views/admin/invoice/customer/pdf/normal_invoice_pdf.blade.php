<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="images/favicon.png" rel="icon" />
    <title>Invoice</title>
    <meta name="author" content="harnishdesign.net">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link href="https://fonts.googleapis.com/css?family=poppins:400,400i,700,700i" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />  

    <style>
        .body-container {
            padding: 0 !important;
            margin: 0 !important;
            display: block !important;
            min-width: 100% !important;
            width: 100% !important;
            background: #fff;
            -webkit-text-size-adjust: none;
        }
    </style>
</head>

<body class="body-container">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f4f4">
        <tr>
            <td align="center" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff">
                    <tr>
                        <td>
                            <table width="90% " border="0 " cellspacing="0 " cellpadding="0 " align="center">
                                <tr>
                                    <td style="font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; text-align:right;">
                                        @if($invoiceCopy == 1)
                                            {{'Original'}}
                                        @elseif($invoiceCopy == 2)
                                            {{'Duplicate'}}
                                        @elseif($invoiceCopy == 3)
                                            {{'Triplicate'}}
                                        @else
                                            {{'Extra Copy'}}
                                        @endif
                                    </td>
                                </tr>
                            </table>

                            <!-- BODY -->
                            <table width="90%" align="center" cellspacing=" 0 " cellpadding="0 " style="margin: 0px auto 0px auto; border: 2px solid #000000; ">
                                <tr>
                                    <td>
                                        <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                            <tr>
                                                <td style="width: 10%;text-align:left;border-bottom: 2px solid #000000;">
                                                    <?php /*
                                                    <img src="<?php echo $pic;?>" title="R H GASES" alt="R H GASES" style="width: 60%;padding:8px 0 8px 20px">
                                                    */ ?>
                                                    
                                                    <img src="{{ public_path('images/invoice_logo.PNG') }}" title="R H GASES" alt="R H GASES" style="width: 60%;padding:8px 0 8px 20px">
                                                </td>

                                                <td style="border-bottom: 2px solid #000000; font-weight: 600; color:#010101; font-family:'poppins';font-size:30px; line-height:50px; text-align:center;">
                                                    R H GASES
                                                </td>

                                                <td style="width: 10%;text-align:left;border-bottom: 2px solid #000000;">
                                                </td>
                                            </tr>
                                        </table>


                                        <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                            <tr>
                                                <td style="border-bottom: 2px solid #000000; font-weight: 400; color:#010101; font-family:'poppins';font-size:18px; line-height:40px; text-align:center;">
                                                    <b>TAX INVOICE</b>
                                                </td>
                                            </tr>
                                        </table>

                                        <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                            <tr>
                                                <td rowspan="3" style="padding:8px 0px 8px 10px;width:50%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>R H GASES</b> <br/>Plot No. 212 Main Road Phase-2 Dared <br/>Jamnagar - 361004<br/>Mob: 8128157173<br/>GST NO. - 24CVMPP0402J1ZO
                                                </td>

                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">
                                                        Invoice No.<br/>
                                                        <b>{{$invoice->invoice_no}}</b>
                                                </td>

                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">
                                                        Dated<br/>
                                                        <b>
                                                            {{date('d-M-y',strtotime($invoice->invoice_date))}}
                                                        </b>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">
                                                        Delivery Note <br/><b> &nbsp;</b>
                                                </td>

                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">
                                                        Mode/Terms Of Payment<br/><b> &nbsp;</b>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">
                                                        Buyer's Order No.<br/><b> &nbsp;</b>
                                                </td>
                                                
                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">
                                                        Dated<br/><b> &nbsp;</b>
                                                </td>
                                            </tr>
                                        </table>

                                        <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                            <tr>
                                                <td rowspan="3" style="padding:8px 0px 8px 10px;width:50%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>Buyer</b><br/>

                                                    {{$invoice->to_name}}<br/>

                                                    {{$invoice->to_address}}<br/>

                                                    {{$invoice->to_city.', '.$invoice->to_state.' '.$invoice->to_pin_code}}<br/>

                                                    Phone: {{$invoice->to_mob}} {{!is_null($invoice->to_alt_mo)?(','.$invoice->to_alt_mo):''}}<br>

                                                    {{'GST NO. - '.$invoice->to_gst_no}}<br/>
                                                    
                                                    {{'Place Of Supply - Gujarat'}}
                                                </td>
                                                
                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">
                                                    Despatch Document No.<br/>
                                                    <b>&nbsp;</b>
                                                </td>

                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">   
                                                    Delivery Note Date<br/>
                                                    <b>&nbsp;</b>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">Despatched through <br/><b> &nbsp;</b></td>
                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">Destination<br/><b> &nbsp;</b></td>
                                            </tr>

                                            <tr>
                                                <td colspan="2" style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">Terms of Delivery<br/><b> &nbsp;</b></td>
                                            </tr>
                                        </table>

                                        <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                            <tr>
                                                <td style="text-align:center;padding:8px 0px ;width:5%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>Sr.No.</b>
                                                </td>

                                                <td style="text-align:center;padding:8px 0px ;width:39.5%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>Description Of Goods</b>
                                                </td>

                                                <td style="text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>HSN</b>
                                                </td>

                                                <td style="text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>Chalan No</b>
                                                </td>

                                                <td style="text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>Qty</b>
                                                </td>

                                                <td style="text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>Rate</b>
                                                </td>

                                                <td style="text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>Per</b>
                                                </td>

                                                <td style="text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>
                                                        Amount &nbsp;
                                                        <i class="fas fa-rupee-sign fa-xs" style="
                                                        margin-top: 3px;margin-left: 4px;"></i>
                                                    </b>
                                                </td>
                                            </tr>
                                        </table>

                                        <div style="height: auto;">
                                            <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">

                                                @foreach($invoiceDetail as $index => $value)
                                                <tr>
                                                    <td style=" vertical-align:top;text-align:center;padding:8px 0px ;width:5%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                        'poppins';font-size:16px; line-height:25px; ">
                                                        {{$index+1}}
                                                    </td>

                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:39.5%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                        'poppins';font-size:16px; line-height:25px; ">
                                                        {{$value->cylinder.' ('.$value->gas.')'}}
                                                    </td>

                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                        'poppins';font-size:16px; line-height:25px; ">
                                                        {{$value->hsn_code}}
                                                    </td>

                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                        'poppins';font-size:16px; line-height:25px; ">
                                                        {{$value->challan_no}}
                                                    </td>

                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                        'poppins';font-size:16px; line-height:25px; ">
                                                        {{$value->qty}}
                                                    </td>

                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                        'poppins';font-size:16px; line-height:25px; ">
                                                        #
                                                    </td>

                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                        'poppins';font-size:16px; line-height:25px; ">
                                                        Cyl
                                                    </td>

                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;font-weight: 100; color:#010101; font-family:
                                                        'poppins';font-size:16px; line-height:25px; ">
                                                        {{sprintf('%.2f',$value->total)}}
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </table>
                                        </div>

                                        <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                            <tr>
                                                <td style=" vertical-align:top;text-align:right;padding:2px 8px;width:90%;border-top: 2px solid #000000;border-right: 2px solid #000000;color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    <b>Transportation</b>
                                                </td>

                                                <td style="vertical-align:top;text-align:center;padding:2px 8px;width:8.2%;border-top: 2px solid #000000;font-weight: 100; color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    #
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style=" vertical-align:top;text-align:right;padding:2px 8px;width:90%;border-right: 2px solid #000000;color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    <b>Sub Total</b>
                                                </td>

                                                <td style="vertical-align:top;padding:2px 8px;width:8.2%;font-weight: 100; color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    
                                                    <i class="fas fa-rupee-sign fa-xs" style="
                                                    margin-left: 17px;margin-top: 2px;"></i>
                                                    {{sprintf('%.2f',$invoice->row_total)}}
                                                </td>
                                            </tr>

                                            @if($invoice->cgst6 > 0 || $invoice->sgst6 > 0)
                                            <tr>
                                                <td style=" vertical-align:top;text-align:right;padding:2px 8px;width:90%;border-right: 2px solid #000000;color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    <b>CGST@ 6%</b>
                                                </td>

                                                <td style="vertical-align:top;padding:2px 8px;width:8.2%;font-weight: 100; color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    <i class="fas fa-rupee-sign fa-xs" style="
                                                    margin-left: 17px;margin-top: 2px;"></i>
                                                    {{sprintf('%.2f',$invoice->cgst6)}}
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td style=" vertical-align:top;text-align:right;padding:2px 8px;width:90%;border-right: 2px solid #000000;color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    <b>SGST@ 6%</b>
                                                </td>

                                                <td style="vertical-align:top;padding:2px 8px;width:8.2%;font-weight: 100; color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    <i class="fas fa-rupee-sign fa-xs" style="
                                                    margin-left: 17px;margin-top: 2px;"></i>
                                                    {{sprintf('%.2f',$invoice->sgst6)}}
                                                </td>
                                            </tr>
                                            @endif

                                            @if($invoice->cgst9 > 0 || $invoice->sgst9 > 0)
                                            <tr>
                                                <td style=" vertical-align:top;text-align:right;padding:2px 8px;width:90%;border-right: 2px solid #000000;color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    <b>CGST@ 9%</b>
                                                </td>

                                                <td style="vertical-align:top;padding:2px 8px;width:8.2%;font-weight: 100; color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    <i class="fas fa-rupee-sign fa-xs" style="
                                                    margin-left: 17px;margin-top: 2px;"></i>
                                                    {{sprintf('%.2f',$invoice->cgst9)}}
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td style=" vertical-align:top;text-align:right;padding:2px 8px;width:90%;border-right: 2px solid #000000;color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    <b>SGST@ 9%</b>
                                                </td>

                                                <td style="vertical-align:top;padding:2px 8px;width:8.2%;font-weight: 100; color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    <i class="fas fa-rupee-sign fa-xs" style="
                                                    margin-left: 17px;margin-top: 2px;"></i>
                                                    {{sprintf('%.2f',$invoice->sgst9)}}
                                                </td>
                                            </tr>
                                            @endif

                                            <tr>
                                                <td style=" vertical-align:top;text-align:right;padding:2px 8px;width:90%;border-right: 2px solid #000000;color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    <b>Total </b>
                                                </td>

                                                <td style="vertical-align:top;padding:2px 8px;width:8.2%;font-weight: 100; color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    <i class="fas fa-rupee-sign fa-xs" style="
                                                    margin-left: 17px;margin-top: 2px;"></i>
                                                    {{sprintf('%.2f',$invoice->net_total)}}
                                                </td>
                                            </tr>

                                            <?php /*
                                            <tr>
                                                <td style=" vertical-align:top;text-align:right;padding:2px 8px;width:90%;border-right: 2px solid #000000;color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    <b>Round Off </b>
                                                </td>

                                                <td style="vertical-align:top;text-align:center;padding:2px 8px;width:8.2%;font-weight: 100; color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    -0.01
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style=" vertical-align:top;text-align:right;padding:2px 8px;width:90%;border-right: 2px solid #000000;color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    <b>Gand Total </b>
                                                </td>

                                                <td style="vertical-align:top;text-align:center;padding:2px 8px;width:8.2%;font-weight: 100; color:#010101; font-family:'poppins';font-size:16px; line-height:25px; ">
                                                    1600.00
                                                </td>
                                            </tr>
                                            */ ?>
                                        </table>

                                        <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                            <tr>
                                                <td style=" vertical-align:top;text-align:left;padding:2px 8px;width:90%;border-top: 2px solid #000000;color:#010101; font-family:'poppins';font-size:14px; line-height:25px; ">
                                                    <b>TERMS AND CONDITIONS <br/>Cylinders to be return in good condition. </b>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style=" vertical-align:top;text-align:left;padding:50px 0px 8px 8px; width:90%;color:#010101; font-family:'poppins';font-size:14px; line-height:25px; ">
                                                    <b>PLEASE MAKE A PAYMENT TO <br/>Beneficiary Name: R H GASES<br/>Beneficiary Account Number: 020505503604<br/>Bank Name and Address: ICICI Bank Main Branch Jamnagar<br/>Bank IFS Code: ICIC0000205</b>
                                                </td>
                                            </tr>
                                        </table>

                                        <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                            <tr>
                                                <td style="border-top: 2px solid #000000; font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:30px; text-align:center;">
                                                    <i>THANK YOU FOR YOUR BUSINESS!</i></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!-- END BODY -->

                            <table width="90% " border="0 " cellspacing="0 " cellpadding="0 " align="center">
                                <tr>
                                    <td style="padding:4px 45px 30px 0px;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:30px; text-align:right;">
                                        <b>For R.H.Gases</b>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding-right:15px;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:30px; text-align:right;">
                                        Authorized Signature
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>


