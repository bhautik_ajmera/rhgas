<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="images/favicon.png" rel="icon" />
    <title>Invoice</title>
    <meta name="author" content="harnishdesign.net">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link href="https://fonts.googleapis.com/css?family=poppins:400,400i,700,700i" rel="stylesheet" />
    <style>
        .body-container {
            padding: 0 !important;
            margin: 0 !important;
            display: block !important;
            min-width: 100% !important;
            width: 100% !important;
            background: #fff;
            -webkit-text-size-adjust: none;
        }
    </style>
</head>

<body class="body-container">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f4f4">
        <tr>
            <td align="center" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff">
                    <tr>
                        <td>

                            <!-- orginal-text -->
                            <table width="90% " border="0 " cellspacing="0 " cellpadding="0 " align="center">
                                <tr>
                                    <td style="font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; text-align:right;">
                                        Original</td>
                                </tr>
                            </table>
                            <!-- orginal-text end -->


                            <!-- BODY -->
                            <table width="90%" align="center" cellspacing=" 0 " cellpadding="0 " style="margin: 0px auto 0px auto; border: 2px solid #000000; ">
                                <tr>
                                    <td>

                                        <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                            <tr>
                                                <td style="width: 10%;text-align:left;border-bottom: 2px solid #000000;">
                                                    <img src="{{ asset('images\invoice_logo.png') }}" style="width: 60%;padding:8px 0 8px 20px">
                                                </td>
                                                <td style="border-bottom: 2px solid #000000; font-weight: 600; color:#010101; font-family:'poppins';font-size:30px; line-height:50px; text-align:center;">
                                                    <!-- <img src="logo-r-h.PNG" style="text-align: right;"> -->
                                                    R H GASES
                                                </td>
                                                <td style="width: 10%;text-align:left;border-bottom: 2px solid #000000;">
                                                </td>
                                            </tr>
                                        </table>


                                        <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                            <tr>
                                                <td style="border-bottom: 2px solid #000000; font-weight: 400; color:#010101; font-family:'poppins';font-size:18px; line-height:40px; text-align:center;">
                                                    <b>TAX INVOICE</b>
                                                </td>
                                            </tr>
                                        </table>

                                        <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                            <tr>
                                                <td rowspan="3" style="padding:8px 0px 8px 10px;width:50%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>R H GASES</b> <br/>Plot No. 212 Main Road Phase-2 Dared <br/>Jamnagar - 361004<br/>Mob: 8128157173<br/>GST NO. - 24CVMPP0402J1ZO
                                                </td>
                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">Invoice No.<br/><b>RHG00280</b></td>
                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">Dated<br/><b>30-Apr-21</b></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">Delivery Note <br/><b> &nbsp;</b></td>
                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">Mode/Terms Of Payment<br/><b> &nbsp;</b></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">Buyer's Order No.<br/><b> &nbsp;</b></td>
                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">Dated<br/><b> &nbsp;</b></td>
                                            </tr>

                                        </table>


                                        <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                            <tr>
                                                <td rowspan="3" style="padding:8px 0px 8px 10px;width:50%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>Buyer</b>
                                                    <br/>Verai Metal Products<br/>Plot No. 11 Shree Hari Ind. Estate GIDC Phase-3 <br/>Dared- Jamnagar 361004<br/>GST NO. - 24ARWPMO977D1ZG<br/>Place Of Supply - Gujarat
                                                </td>
                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">Despatch Document No.<br/>
                                                    <b>&nbsp</b>
                                                </td>
                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">Delivery Note Date<br/><b>&nbsp;</b></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">Despatched through <br/><b> &nbsp;</b></td>
                                                <td style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">Destination<br/><b> &nbsp;</b></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="padding:4px 0px 4px 8px;width:27%;border-top: 0;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:22px">Terms of Delivery<br/><b> &nbsp;</b></td>
                                            </tr>
                                        </table>

                                        <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                            <tr>
                                                <td style="text-align:center;padding:8px 0px ;width:5%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>Sr.No.</b>
                                                </td>
                                                <td style="text-align:center;padding:8px 0px ;width:43%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>Description Of Goods</b>
                                                </td>
                                                <td style="text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>HSN</b>
                                                </td>
                                                <td style="text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>Chalan No</b>
                                                </td>
                                                <td style="text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>Qty</b>
                                                </td>
                                                <td style="text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>Rate</b>
                                                </td>
                                                <td style="text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>Per</b>
                                                </td>
                                                <td style="text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-bottom: 2px solid #000000;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:25px;">
                                                    <b>Amount</b>
                                                </td>

                                            </tr>
                                        </table>

                                        <div style="height: 369px">
                                            <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                                <tr>
                                                    <td style=" vertical-align:top;text-align:center;padding:8px 0px ;width:5%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        1
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:43%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        Argon
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        2804
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        1849
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        1
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        677.97
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        Cyl
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        678
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style=" vertical-align:top;text-align:center;padding:8px 0px ;width:5%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        2
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:43%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        Argon
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        2804
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        1849
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        1
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        677.97
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        Cyl
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        678
                                                    </td>

                                                </tr>


                                                <tr>
                                                    <td style=" vertical-align:top;text-align:center;padding:8px 0px ;width:5%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:43%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>

                                                </tr>

                                                <tr>
                                                    <td style=" vertical-align:top;text-align:center;padding:8px 0px ;width:5%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:43%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>

                                                </tr>

                                                <tr>
                                                    <td style=" vertical-align:top;text-align:center;padding:8px 0px ;width:5%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:43%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>

                                                </tr>


                                                <tr>
                                                    <td style=" vertical-align:top;text-align:center;padding:8px 0px ;width:5%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:43%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>

                                                </tr>


                                                <tr>
                                                    <td style=" vertical-align:top;text-align:center;padding:8px 0px ;width:5%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:43%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>

                                                </tr>


                                                <tr>
                                                    <td style=" vertical-align:top;text-align:center;padding:8px 0px ;width:5%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:43%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>

                                                </tr>


                                                <tr>
                                                    <td style=" vertical-align:top;text-align:center;padding:8px 0px ;width:5%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:43%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp;
                                                    </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;border-right: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>
                                                    <td style="vertical-align:top;text-align:center;padding:8px 0px ;width:8%;border-top: 0;font-weight: 100; color:#010101; font-family:
                                                'poppins';font-size:16px; line-height:25px; ">
                                                        &nbsp; </td>

                                                </tr>



                                            </table>


                                        </div>

                                        <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                            <tr>
                                                <td style=" vertical-align:top;text-align:right;padding:2px 8px;width:90%;border-top: 2px solid #000000;border-right: 2px solid #000000;color:#010101; font-family:
                                            'poppins';font-size:16px; line-height:25px; ">
                                                    <b>Transportation</b>
                                                </td>
                                                <td style="vertical-align:top;text-align:center;padding:2px 8px;width:8.2%;border-top: 2px solid #000000;font-weight: 100; color:#010101; font-family:
                                            'poppins';font-size:16px; line-height:25px; ">
                                                    0
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style=" vertical-align:top;text-align:right;padding:2px 8px;width:90%;border-right: 2px solid #000000;color:#010101; font-family:
                                            'poppins';font-size:16px; line-height:25px; ">
                                                    <b>Sub Total</b>
                                                </td>
                                                <td style="vertical-align:top;text-align:center;padding:2px 8px;width:8.2%;font-weight: 100; color:#010101; font-family:
                                            'poppins';font-size:16px; line-height:25px; ">
                                                    1355.9
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style=" vertical-align:top;text-align:right;padding:2px 8px;width:90%;border-right: 2px solid #000000;color:#010101; font-family:
                                            'poppins';font-size:16px; line-height:25px; ">
                                                    <b>CGST@ 9%</b>
                                                </td>
                                                <td style="vertical-align:top;text-align:center;padding:2px 8px;width:8.2%;font-weight: 100; color:#010101; font-family:
                                            'poppins';font-size:16px; line-height:25px; ">
                                                    122.0346
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style=" vertical-align:top;text-align:right;padding:2px 8px;width:90%;border-right: 2px solid #000000;color:#010101; font-family:
                                            'poppins';font-size:16px; line-height:25px; ">
                                                    <b>SGST@ 9%</b>
                                                </td>
                                                <td style="vertical-align:top;text-align:center;padding:2px 8px;width:8.2%;font-weight: 100; color:#010101; font-family:
                                            'poppins';font-size:16px; line-height:25px; ">
                                                    122.0346
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style=" vertical-align:top;text-align:right;padding:2px 8px;width:90%;border-right: 2px solid #000000;color:#010101; font-family:
                                            'poppins';font-size:16px; line-height:25px; ">
                                                    <b>Total </b>
                                                </td>
                                                <td style="vertical-align:top;text-align:center;padding:2px 8px;width:8.2%;font-weight: 100; color:#010101; font-family:
                                            'poppins';font-size:16px; line-height:25px; ">
                                                    1600.01
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style=" vertical-align:top;text-align:right;padding:2px 8px;width:90%;border-right: 2px solid #000000;color:#010101; font-family:
                                            'poppins';font-size:16px; line-height:25px; ">
                                                    <b>Round Off </b>
                                                </td>
                                                <td style="vertical-align:top;text-align:center;padding:2px 8px;width:8.2%;font-weight: 100; color:#010101; font-family:
                                            'poppins';font-size:16px; line-height:25px; ">
                                                    -0.01
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style=" vertical-align:top;text-align:right;padding:2px 8px;width:90%;border-right: 2px solid #000000;color:#010101; font-family:
                                            'poppins';font-size:16px; line-height:25px; ">
                                                    <b>Gand Total </b>
                                                </td>
                                                <td style="vertical-align:top;text-align:center;padding:2px 8px;width:8.2%;font-weight: 100; color:#010101; font-family:
                                            'poppins';font-size:16px; line-height:25px; ">
                                                    1600.00
                                                </td>
                                            </tr>
                                        </table>


                                        <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                            <tr>
                                                <td style=" vertical-align:top;text-align:left;padding:2px 8px;width:90%;border-top: 2px solid #000000;color:#010101; font-family:
                                            'poppins';font-size:14px; line-height:25px; ">
                                                    <b>TERMS AND CONDITIONS <br/>Cylinders to be return in good condition. </b>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style=" vertical-align:top;text-align:left;padding:50px 0px 8px 8px; width:90%;color:#010101; font-family:
                                            'poppins';font-size:14px; line-height:25px; ">
                                                    <b>PLEASE MAKE A PAYMENT TO <br/>Beneficiary Name: R H GASES<br/>Beneficiary Account Number: 020505503604<br/>Bank Name and Address: ICICI Bank Main Branch Jamnagar<br/>Bank IFS Code: ICIC0000205</b>
                                                </td>

                                            </tr>

                                        </table>



                                        <table width="100% " border="0 " cellspacing="0 " cellpadding="0 ">
                                            <tr>
                                                <td style="border-top: 2px solid #000000; font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:30px; text-align:center;">
                                                    <i>THANK YOU FOR YOUR BUSINESS!</i></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!-- END BODY -->

                            <!-- footer -->
                            <table width="90% " border="0 " cellspacing="0 " cellpadding="0 " align="center">
                                <tr>
                                    <td style="padding:4px 45px 30px 0px;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:30px; text-align:right;">
                                        <b>For R.H.Gases</b></td>
                                </tr>

                                <tr>
                                    <td style="padding-right:15px;font-weight: 400; color:#010101; font-family:'poppins';font-size:16px; line-height:30px; text-align:right;">
                                        Authorized Signature</td>
                                </tr>
                            </table>
                            <!-- footer end -->

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>