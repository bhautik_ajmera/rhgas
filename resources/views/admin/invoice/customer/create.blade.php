@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('css/select2-bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif

@if(session()->has('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
@endif

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
            <h3>Create Invoice</h3>
        </div>
        <div class="col-sm-6">
            <a href="{{route('admin.invoice')}}" class="btn btn-default float-right">
                Back
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <form method="post" action="{{route('admin.invoice.save')}}" id="invFrm">
            @csrf
                <div class="callout callout-info">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="required">Customer</label>

                                <select class="form-control select2" name="customer" id="customer">
                                    <option value="">Select Customer</option>
                                    @foreach($customers as $cid => $customer)
                                        <option value="{{$cid}}">{{$customer}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <label class="required">Invoice</label>

                            <div class="form-group">
                                <div class="form-check form-check-inline mt-1">
                                    <input type="radio" class="form-check-input normal-rent" name="invtype" value="1" checked />
                                    <label class="form-check-label">
                                        Normal Invoice
                                    </label>
                                </div>

                                <div class="form-check form-check-inline">
                                    <input type="radio" class="form-check-input normal-rent" name="invtype" value="0" />
                                    <label class="form-check-label">
                                        Rent Invoice
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-5" id="renderChallan">
                            @include('admin.invoice.customer.challan_no')
                        </div>

                    </div>
                </div>
            
                <div class="invoice p-3 mb-3">
                    <div class="row">
                        <div class="col-12">
                            <div class="float-left">
                                <h5>
                                    <img src="{{asset('images/logo.jpeg')}}" class="rounded-circle" alt="{{ trans('panel.site_title') }}" height="35px" />
                                    <span style="margin-left: -10px;">
                                        {{ trans('panel.site_title') }}
                                    </span>
                                </h5>
                            </div>

                            <div class="float-right">
                                <!-- <span>
                                    <small >Invoice No: 001</small>
                                </span> -->
                                
                                <span>
                                    Date: {{date('d/m/Y')}}
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row invoice-info mt-3" id="renderAdd" style="height: 175px;">
                        @include('admin.invoice.customer.from_to_address')
                    </div>

                    <div id="renderListing">
                        @include('admin.invoice.customer.listing')
                    </div>
                    
                    <div class="row no-print">
                        <div class="col-12">
                            <input type="submit" id="createInvoice" class="btn btn-success float-right" value="Create Invoice" disabled="disabled" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('js/invoice/customer/cus_inv.js') }}"></script>
@endsection