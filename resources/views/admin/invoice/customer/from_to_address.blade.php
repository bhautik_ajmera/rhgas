<div class="col-sm-4 invoice-col">
    From
    <address>
        <strong>R H GASES</strong><br>
        Plot No. 212 Main Road Phase-2 Dared<br>
        Jamnagar - 361004<br>
        Mob: 8128157173<br>
        GST NO. - 24CVMPP0402J1ZO
    </address>
</div>

<div class="col-sm-4 invoice-col">
    To
	@if(isset($cdetail))
		<address>
		    <strong>{{$cdetail->companny_name}}</strong><br>
		    {{$cdetail->address}}<br>
		    {{$cdetail->cname.', '.$cdetail->sname.' '.$cdetail->pin_code}}<br>
		    Phone: {{$cdetail->mob_no}} {{!is_null($cdetail->alternate_mob_no)?(','.$cdetail->alternate_mob_no):''}}<br>
		    Email: {{$cdetail->email}}
		</address>
        
        <input type="hidden" name="toName" value="{{$cdetail->companny_name}}" />
        <input type="hidden" name="toAddress" value="{{$cdetail->address}}" />
        <input type="hidden" name="toCity" value="{{$cdetail->cname}}" />
        <input type="hidden" name="toState" value="{{$cdetail->sname}}" />
        <input type="hidden" name="toPinCode" value="{{$cdetail->pin_code}}" />
        <input type="hidden" name="toMob" value="{{$cdetail->mob_no}}" />
        <input type="hidden" name="toAltMob" value="{{$cdetail->alternate_mob_no}}" />
        <input type="hidden" name="toEmail" value="{{$cdetail->email}}" />
	@endif
</div>

<div class="col-sm-4 invoice-col">
    <b>Customer Code: </b> {{isset($cdetail)?strtoupper($cdetail->code):''}}<br>
    <b>Contact Person: </b> {{isset($cdetail)?strtoupper($cdetail->contact_person):''}}<br>
    <b>GST Number: </b> {{isset($cdetail)?strtoupper($cdetail->gstno):''}}<br>
    <b>Aadhaar Card Number: </b> {{isset($cdetail)?$cdetail->addhar_card_no:''}}<br>

    <input type="hidden" name="toCusCode" value="{{isset($cdetail)?strtoupper($cdetail->code):''}}" />
    <input type="hidden" name="toConPer" value="{{isset($cdetail)?strtoupper($cdetail->contact_person):''}}" />
    <input type="hidden" name="toGST" value="{{isset($cdetail)?strtoupper($cdetail->gstno):''}}" />
    <input type="hidden" name="toAadhaar" value="{{isset($cdetail)?$cdetail->addhar_card_no:''}}" />
</div>