@extends('layouts.admin')

@section('content')
	@if(session()->has('success'))
	    <div class="alert alert-success">
	        {{ session()->get('success') }}
	    </div>
	@endif

	@if(session()->has('error'))
	    <div class="alert alert-danger">
	        {{ session()->get('error') }}
	    </div>
	@endif

	<div class="card">
	    <div class="card-header">
	        <span>
	            {{'Create Cylinder Refilling'}}
	        </span>
	    </div>

	    <form method="POST" action="{{ route("admin.cylinder.refilling.store") }}" enctype="multipart/form-data" id="cylRefiFrm">
        @csrf

    	    <div class="card-body">
    	        <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="required">
                                {{'Supplier'}}
                            </label>
                            <i class="float-right fas fa-eye mr-1" id="view_sup" title="View Supplier Detail" data-toggle="modal"></i>

                            <select class="form-control select2 {{ $errors->has('supplier_id') ? 'is-invalid' : '' }}" name="supplier_id" id="supplier_id" >
                                @foreach($supplieres as $id => $supplier)
                                    <option value="{{ $id }}" {{ old('supplier_id') == $id ? 'selected' : '' }}>{{ $supplier }}</option>
                                @endforeach
                            </select>

                            @if($errors->has('supplier_id'))
                                <span class="text-danger">
                                	{{ 'The supplier field is required.' }}
                                </span>
                            @endif

                            <div id="suppError"></div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>
                                {{'System Challan No'}}
                            </label>
                            
                            <div>
                                <i>{{"Auto Generate"}}</i> 
                            </div>
                        </div>
                    </div>
                </div>
    	        
    	        <div class="row">
                    <div class="col-sm-6">
    	            	<div class="form-group">
    	                    <label class="required">
    	                        {{'Date'}}
    	                    </label>
    	                    
    	                    <input class="form-control rdate {{ $errors->has('date') ? 'is-invalid' : '' }}" type="text" name="date" id="date" value="{{ old('date') }}" autocomplete="off" readonly="readonly" />
    	                    
    	                    @if($errors->has('date'))
    	                        <span class="text-danger">{{ $errors->first('date') }}</span>
    	                    @endif
    	                </div>
                	</div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>
                                {{'Vehicle No'}}
                            </label>
                            <input class="form-control text-uppercase" type="text" name="vehicle_no" id="vehicle_no" value="{{ old('vehicle_no', '') }}" autocomplete="off" maxlength="15" />
                        </div>
                    </div>
                </div>

                <div class="row">
                	<div class="col-sm-6">
                        <div class="form-group">
                            <label>
                                {{'Remarks'}}
                            </label>
                            <textarea class="form-control" name="remark" id="remark">{{ old('remark') }}</textarea>
                        </div>
                    </div>
                </div>

                <hr/>

                <ul>
                    @if($errors->has('gas.*'))
                        <li>
                            <span class="text-danger">{{ 'The Gas field is required.' }}</span>
                        </li>
                    @endif

                    @if($errors->has('cylNo.*'))
                        <li>
                            <span class="text-danger">{{ 'The Cylinder No field is required.' }}</span>
                        </li>
                    @endif
                </ul>

                <div id="test">
                    <input type="hidden" name="rowCount" id="rowCount" value="0" />
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-striped table-sm" id="listing">
                                <thead>
                                    <tr>
                                        <th scope="col" width="25%">
                                            <center>
                                                Gas<span class="text text-danger">*</span>
                                            </center>
                                        </th>
                                        
                                        <th scope="col" width="40%">
                                            <center>
                                                Cylinder No<span class="text text-danger">*</span>
                                            </center>
                                        </th>
                                  
                                        <th scope="col" width="25%">
                                            <center>
                                                Rate
                                            </center>
                                        </th>
                                        
                                        <th scope="col" width="10%"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <select class="form-control gas" name="gas[]" id="gas_0">
                                                <option value="">Select Gas</option>
                                            </select>
                                        </td>

                                        <td>
                                            <select class="form-control cyl_no" name="cylNo[]" data-id="0" id="cyl_0">
                                                <option value="">Select Cylinder No</option>
                                            </select>
                                        </td>

                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control allownumericwithdecimal rate" name="rate[]" id="rate_0" value="0.00" autocomplete="off" readonly="readonly" />
                                                
                                                <div class="input-group-append">
                                                    <span class="input-group-text">RS</span>
                                                </div>
                                            </div>
                                        </td>

                                        <input type="hidden" name="status[]" value="0" />
                                        <td>
                                            <button type="button" class="btn btn-warning" id="addMore" title="Add More">
                                                <i class="far fas fa-plus-circle"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="text text-right mt-2">
                                                <strong>Total</strong>
                                            </div>
                                        </td>
                                        <td >
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="total" id="total" autocomplete="off" value="0.00" readonly="readonly" placeholder="Total" />
                                                
                                                <div class="input-group-append">
                                                    <span class="input-group-text">RS</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-success" type="submit">
                        {{ trans('global.save') }}
                    </button>
                    <a href="{{route('admin.cylinder.refilling')}}" class="btn btn-default">
                        {{ trans('global.cancel') }}
                    </a>
                </div>
    	    </div>
        </form>
	</div>

	<div class="modal fade" id="supplier-modal">
	    <div class="modal-dialog">
	        <div class="modal-content" id="renderSupDetail">
	            @include('admin.refilling.view_sup_detail')
	        </div>
	    </div>
	</div>
@endsection

@section('scripts')
	@parent
    <script src="{{ asset('js/cylinder-refilling/add_edit.js?'.time())}}" ></script>
@endsection