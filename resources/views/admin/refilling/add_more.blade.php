<tr id="{{'newRef_'.$count}}">
    <td>
        <select class="form-control gas" name="gas[]" id="{{'gas_'.$count}}">
            <option value="">Select Gas</option>
        </select>
    </td>

    <td>
        <select class="form-control cyl_no" name="cylNo[]" id="{{'cyl_'.$count}}" data-id="{{$count}}">
            <option value="">Select Cylinder No</option>
        </select>
    </td>

    <td>
        <div class="input-group">
            <input type="text" class="form-control allownumericwithdecimal rate" name="rate[]" id="{{'rate_'.$count}}" value="0.00" autocomplete="off" readonly="readonly" />
            <div class="input-group-append">
                <span class="input-group-text">RS</span>
            </div>
        </div>
    </td>

    <input type="hidden" name="status[]" value="0" />
    <td>
        <button type="button" class="btn btn-danger remove" id="{{$count}}" title="Remove">
            <i class="far fas fa-minus-circle"></i>
        </button>
    </td>
</tr>
