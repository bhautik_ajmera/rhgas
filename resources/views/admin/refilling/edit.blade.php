@extends('layouts.admin')

@section('content')
	@if(session()->has('success'))
	    <div class="alert alert-success">
	        {{ session()->get('success') }}
	    </div>
	@endif

	@if(session()->has('error'))
	    <div class="alert alert-danger">
	        {{ session()->get('error') }}
	    </div>
	@endif

	<div class="card">
	    <div class="card-header">
	        <span>
	            {{'Edit Cylinder Refilling'}}
	        </span>
	    </div>

	    <form method="POST" action="{{route('admin.cylinder.refilling.update')}}" enctype="multipart/form-data" id="cylRefiFrm">
        @csrf
        <input type="hidden" name="refillingId" id="refillingId" value="{{$refilling->id}}" />

	    <div class="card-body">
	        <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required">
                            {{'Supplier'}}
                        </label>
                        <i class="float-right fas fa-eye mr-1" id="view_sup" title="View Supplier Detail" data-toggle="modal"></i>

                        <select class="form-control disable_select {{ $errors->has('supplier_id') ? 'is-invalid' : '' }}" name="supplier_id" id="supplier_id" >
                            @foreach($supplieres as $id => $supplier)
                                <option value="{{ $id }}" {{ (old('supplier_id') ? old('supplier_id') : $refilling->supplier_id ?? '') == $id ? 'selected' : '' }}>{{ $supplier }}</option>
                            @endforeach
                        </select>

                        @if($errors->has('supplier_id'))
                            <span class="text-danger">
                            	{{ 'The supplier field is required.' }}
                            </span>
                        @endif

                        <div id="suppError"></div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'System Challan No'}}
                        </label>
                        
                        <input class="form-control" type="text" name="sys_chal_no" id="sys_chal_no" autocomplete="off" readonly="readonly" value="{{$refilling->sys_chal_no}}" />
                    </div>
                </div>
            </div>
	        
	        <div class="row">
                <div class="col-sm-6">
	            	<div class="form-group">
	                    <label class="required">
	                        {{'Date'}}
	                    </label>
	                    
	                    <input class="form-control rdate {{ $errors->has('date') ? 'is-invalid' : '' }}" type="text" name="date" id="date" value="{{ old('date', date('d-m-Y',strtotime($refilling->date))) }}" autocomplete="off" readonly="readonly" />
	                    
	                    @if($errors->has('date'))
	                        <span class="text-danger">{{ $errors->first('date') }}</span>
	                    @endif
	                </div>
            	</div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'Vehicle No'}}
                        </label>
                        <input class="form-control text-uppercase" type="text" name="vehicle_no" id="vehicle_no" value="{{ old('vehicle_no', $refilling->vehicle_no) }}" autocomplete="off" maxlength="15" />
                    </div>
                </div>
            </div>

            <div class="row">
            	<div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'Remarks'}}
                        </label>
                        <textarea class="form-control" name="remark" id="remark">{{ old('remark', $refilling->remark) }}</textarea>
                    </div>
                </div>
            </div>

            <hr/>

            <ul>
                @if($errors->has('gas.*'))
                    <li>
                        <span class="text-danger">{{ 'The Gas field is required.' }}</span>
                    </li>
                @endif

                @if($errors->has('cylNo.*'))
                    <li>
                        <span class="text-danger">{{ 'The Cylinder No field is required.' }}</span>
                    </li>
                @endif
            </ul>

            <div id="test">
                <input type="hidden" name="rowCount" id="rowCount" value="{{count($refDetail) - 1}}" />
                
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-striped table-sm" id="listing">
                            <thead>
                                <tr>
                                    <th scope="col" width="25%">
                                        <center>
                                            Gas<span class="text text-danger">*</span>
                                        </center>
                                    </th>
                                    
                                    <th scope="col" width="40%">
                                        <center>
                                            Cylinder No<span class="text text-danger">*</span>
                                        </center>
                                    </th>
                              
                                    <th scope="col" width="25%">
                                        <center>
                                            Rate
                                        </center>
                                    </th>
                                    
                                    <th scope="col" width="10%"></th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($refDetail as $index => $value)
                                    @php
                                        $allowToChange = $value::isProFurther($value->cyl_id,$value->updated_at,$value->cstatus);
                                    @endphp

                                    <tr id="{{'storedDetail_'.$value->id}}" class="{{($allowToChange)?'table-success':''}}">
                                        <td>
                                            <select class="form-control gas {{($allowToChange)?'disable_select' : ''}}" name="gas[]" id="{{'gas_'.$index}}">
                                                <option value="">Select Gas</option>
                                                @foreach($gases as $gid => $gname)
                                                    @if($value->gas_id == $gid)
                                                        <option value="{{$gid}}" selected="selected">
                                                            {{$gname}}
                                                        </option>
                                                    @else
                                                        <option value="{{$gid}}">
                                                            {{$gname}}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>

                                        <td>
                                            <select class="form-control cyl_no text-uppercase {{ ($allowToChange)?'disable_select':''}}" name="cylNo[]" data-id="{{$index}}" id="{{'cyl_'.$index}}">
                                                <option value="">Select Cylinder No</option>
                                                @foreach($cylinder as $data)

                                                    @if($value->gas_id == $data->used_for_id)

                                                        @if($value->cyl_id == $data->id)
                                                            <option value="{{$data->id}}" selected="selected">
                                                                {{$data->branch_cyl_no}}
                                                            </option>
                                                        @else
                                                            <option value="{{$data->id}}">
                                                                {{$data->branch_cyl_no}}
                                                            </option>
                                                        @endif
                                                        
                                                    @endif

                                                @endforeach
                                            </select>
                                        </td>

                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control allownumericwithdecimal rate" name="rate[]" id="{{'rate_'.$index}}" value="{{sprintf('%.2f',$value->rate)}}" autocomplete="off" readonly="readonly" />

                                                <div class="input-group-append">
                                                    <span class="input-group-text">RS</span>
                                                </div>
                                            </div>
                                        </td>

                                        <input type="hidden" name="status[]" value="{{$value->status}}" />
                                        <td>
                                            @if($index == 0)
                                                <button type="button" class="btn btn-warning" id="addMore" title="Add More">
                                                    <i class="far fas fa-plus-circle"></i>
                                                </button>
                                            @elseif(!$allowToChange)
                                                <button type="button" class="btn btn-danger delete" id="{{$value->id}}" data-row="{{$index}}" title="Delete">
                                                    <i class="far fas fa-trash"></i>
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                
                                <tr>
                                    <td colspan="2">
                                        <div class="text text-right mt-2">
                                            <strong>Total</strong>
                                        </div>
                                    </td>
                                    <td >
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="total" id="total" autocomplete="off" value="0.00" readonly="readonly" placeholder="Total" />
                                            
                                            <div class="input-group-append">
                                                <span class="input-group-text">RS</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.cylinder.refilling')}}" class="btn btn-default">
                    {{ trans('global.cancel') }}
                </a>
            </div>
	    </div>
        </form>
	</div>

	<div class="modal fade" id="supplier-modal">
	    <div class="modal-dialog">
	        <div class="modal-content" id="renderSupDetail">
	            @include('admin.refilling.view_sup_detail')
	        </div>
	    </div>
	</div>
@endsection

@section('scripts')
	@parent
    <script src="{{ asset('js/cylinder-refilling/add_edit.js?'.time())}}" ></script>
@endsection