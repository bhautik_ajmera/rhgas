<div class="modal-header">
    <h4 class="modal-title">
    	{{isset($supdetail)?$supdetail->company_name.' Detail':''}}
    </h4>
    
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div class="modal-body">
	@if(isset($supdetail))
	    <table class="table">
	    	<tr>
		        <th>
		            {{ trans('cruds.supplier.fields.code') }}
		        </th>
		        <td>
		            {{ $supdetail->code }}
		        </td>
		    </tr>

	    	<tr>
	            <th>
	                {{ trans('cruds.supplier.fields.contact_person') }}
	            </th>
	            <td>
	                {{ $supdetail->contact_person }}
	            </td>
	        </tr>

	    	<tr>
	            <th>
	                {{ trans('cruds.supplier.fields.email') }}
	            </th>
	            <td>
	                {{ !is_null($supdetail->email)?$supdetail->email:'-' }}
	            </td>
	        </tr>

	    	<tr>
	            <th>
	                {{ trans('cruds.supplier.fields.mob_no') }}
	            </th>
	            <td>
	                {{ $supdetail->mob_no }}
	            </td>
	        </tr>

	        <tr>
	            <th>
	                {{ trans('cruds.supplier.fields.alternate_mob_no') }}
	            </th>
	            <td>
	                {{ !is_null($supdetail->alternate_mob_no)?$supdetail->alternate_mob_no:'-' }}
	            </td>
	        </tr>

	    	<tr>
	            <th>
	                {{ trans('cruds.supplier.fields.office_address') }}
	            </th>
	            <td>
	                {{ $supdetail->office_address }}
	            </td>
	        </tr>

	        <tr>
	            <th>
	                {{ trans('cruds.supplier.fields.office_pin_code') }}
	            </th>
	            <td>
	                {{ !is_null($supdetail->office_pin_code)?$supdetail->office_pin_code:'-' }}
	            </td>
	        </tr>

	        <tr>
	            <th>
	                {{ 'Office State' }}
	            </th>
	            <td>
	            	{{$supdetail->getState($supdetail->office_state)}}
	            </td>
	        </tr>

	        <tr>
	            <th>
	                {{ 'Office City' }}
	            </th>
	            <td>
	            	{{$supdetail->getCity($supdetail->office_city)}}
	            </td>
	        </tr>

	        <tr>
	            <th>
	                {{ trans('cruds.supplier.fields.plant_address') }}
	            </th>
	            <td>
	                {{ $supdetail->plant_address }}
	            </td>
	        </tr>

	        <tr>
	            <th>
	                {{ trans('cruds.supplier.fields.plant_pin_code') }}
	            </th>
	            <td>
	                {{ !is_null($supdetail->plant_pin_code)?$supdetail->plant_pin_code:'-' }}
	            </td>
	        </tr>

	        <tr>
	            <th>
	                {{ 'Plant State' }}
	            </th>
	            <td>
	            	{{$supdetail->getState($supdetail->plant_state)}}
	            </td>
	        </tr>

	        <tr>
	            <th>
	                {{ 'Plant City' }}
	            </th>
	            <td>
	            	{{$supdetail->getCity($supdetail->plant_city)}}
	            </td>
	        </tr>

	        <tr>
	            <th>
	                {{ trans('cruds.supplier.fields.gst_no') }}
	            </th>
	            <td class="text-uppercase">
	                {{ $supdetail->gst_no }}
	            </td>
	        </tr>

	        <tr>
	            <th>
	                {{ trans('cruds.supplier.fields.pan_no') }}
	            </th>
	            <td>
	                {{ !is_null($supdetail->pan_no)?$supdetail->pan_no:'-' }}
	            </td>
	        </tr>

	        <tr>
	            <th>
	                {{ trans('cruds.supplier.fields.remark') }}
	            </th>
	            <td>
	                {{ !is_null($supdetail->remark)?$supdetail->remark:'-' }}
	            </td>
	        </tr>
	    </table>
	@else
		<div class="text text-center">
			<h5>No Record Found!</h5>
		</div>
    @endif
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default float-right" data-dismiss="modal">
        Close
    </button>
</div>