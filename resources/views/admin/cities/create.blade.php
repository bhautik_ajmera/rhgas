@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.city.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.cities.store") }}" enctype="multipart/form-data" id="cityFrm" class="form-horizontal">
            @csrf
            <div class="form-group row">
                <label class="required col-sm-1 col-form-label" for="state_id">
                    {{ trans('cruds.city.fields.state') }}
                </label>
                <div class="col-sm-3">
                    <select class="form-control select2 {{ $errors->has('state_id') ? 'is-invalid' : '' }}" name="state_id" id="state_id" />
                        <option value="">Please select</option>
                        @foreach($states as $state)
                            <option value="{{ $state->id }}" {{ old('state_id') == $state->id ? 'selected' : '' }}>{{ $state->name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('state_id'))
                        <!-- <span class="text-danger">{{ $errors->first('state_id') }}</span> -->
                        <span class="text-danger">{{ 'The state filed is required.' }}</span>
                    @endif
                    <div id="stateError"></div>
                    <span class="help-block">{{ trans('cruds.city.fields.state_helper') }}</span>
                </div>
            </div>

            <div class="form-group row">
                <label class="required col-sm-1 col-form-label" for="name">
                    {{ trans('cruds.city.fields.name') }}
                </label>
                <div class="col-sm-3">
                    <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" autocomplete="off" />
                    @if($errors->has('name'))
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.city.fields.name_helper') }}</span>
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.cities.index')}}" class="btn btn-default">
                    {{ trans('global.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/cities/add_edit.js?'.time())}}" ></script>
@endsection