@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.incomeExpense.title') }}
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>
                        {{ trans('cruds.incomeExpense.fields.branch') }}
                    </label>
                    <select class="form-control select2" name="branch_id" id="branch_id" disabled="disabled">
                        @foreach($branches as $id => $branch)
                            <option value="{{ $id }}" {{ (old('branch_id') ? old('branch_id') : $incomeExpense->branch->id ?? '') == $id ? 'selected' : '' }}>{{ $branch }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>
                        {{ trans('cruds.incomeExpense.fields.date') }}
                    </label>
                    <input class="form-control" type="text" name="date" id="date" value="{{ old('date', $incomeExpense->date) }}" disabled="disabled">
                </div>
            </div>
        </div>

        <div class="row mb-3">
            <div class="col-sm-6">
                <label>Income Detail</label>
                <hr/>

                <div class="row">
                    <div class="col-sm-9">
                        <label>Description</label>
                    </div>
                    
                    <div class="col-sm-2">
                        <label>Rate</label>
                    </div>
                </div>
                
                @foreach($inDetail as $key => $value)
                    <div class="input-group {{($key > 0)?'mt-3':''}}">
                        <input type="text" class="form-control col-sm-10" name="incDesc[]" placeholder="Description" value="{{$value->desc}}" disabled="disabled" />
                        <input type="text" class="form-control col-sm-2 ml-2" name="incRate[]" placeholder="Rate" value="{{$value->rate}}" disabled="disabled"/>
                    </div>
                @endforeach
            </div>

            <div class="col-sm-6">
                <label>Expense Detail</label>
                <hr/>

                <div class="row">
                    <div class="col-sm-9">
                        <label>Description</label>
                    </div>
                    
                    <div class="col-sm-2">
                        <label>Rate</label>
                    </div>
                </div>

                @foreach($exDetail as $key => $value)
                    <div class="input-group {{($key > 0)?'mt-3':''}}">
                        <input type="text" class="form-control col-sm-10" name="expDesc[]" placeholder="Description" value="{{$value->desc}}" disabled="disabled" />
                        <input type="text" class="form-control col-sm-2 ml-2" name="expRate[]" placeholder="Rate" value="{{$value->rate}}" disabled="disabled" />
                    </div>
                @endforeach
            </div>
        </div>

        <div class="form-group">
            <a class="btn btn-info" href="{{ route('admin.income-expenses.edit', $incomeExpense->id) }}">
                {{ trans('global.back_to_edit') }}
            </a>
                
            <a href="{{route('admin.income-expenses.index')}}" class="btn btn-default">
                {{ trans('global.cancel') }}
            </a>
        </div>
    </div>
</div>
@endsection

