@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.incomeExpense.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.income-expenses.store") }}" enctype="multipart/form-data" id="inExFrm">
            @csrf

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="branch_id">{{ trans('cruds.incomeExpense.fields.branch') }}</label>
                        <select class="form-control select2 {{ $errors->has('branch_id') ? 'is-invalid' : '' }}" name="branch_id" id="branch_id">
                            @foreach($branches as $id => $branch)
                                <option value="{{ $id }}" {{ old('branch_id') == $id ? 'selected' : '' }}>{{ $branch }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('branch_id'))
                            <span class="text-danger">{{ $errors->first('branch_id') }}</span>
                        @endif
                        <div id="branchError"></div>
                        <span class="help-block">
                            {{ trans('cruds.incomeExpense.fields.branch_helper') }}
                        </span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="date">{{ trans('cruds.incomeExpense.fields.date') }}</label>
                        <input class="form-control datepick {{ $errors->has('date') ? 'is-invalid' : '' }}" type="text" name="date" id="date" value="{{ old('date') }}" readonly="required">
                        @if($errors->has('date'))
                            <span class="text-danger">{{ $errors->first('date') }}</span>
                        @endif
                        <span class="help-block">
                            {{ trans('cruds.incomeExpense.fields.date_helper') }}
                        </span>
                    </div>
                </div>
            </div>

            <div class="row mb-3" id="inExDiv">
                <div class="col-sm-6 incomeDiv">
                    <label>Income Detail</label>

                    <hr/>

                    <ul>
                        @if($errors->has('incRate.*'))
                            <li>
                                <span class="text-danger">{{ 'The rate field is required.' }}</span>
                            </li>
                        @endif
                    </ul>

                    <div class="row">
                        <div class="col-sm-9">
                            <label>Description</label>
                        </div>
                        
                        <div class="col-sm-2">
                            <label>Rate<span class="text text-danger">*</span></label>
                        </div>
                    </div>

                    <div class="input-group">
                        <input type="text" class="form-control col-sm-9" name="incDesc[]" placeholder="Description" />
                        <input type="text" class="form-control col-sm-2 ml-1 allownumericwithdecimal" name="incRate[]" placeholder="Rate" maxlength="5" autocomplete="off" />
                    
                        <button type="button" class="btn btn-warning ml-1" id="addMoreInc" title="Add More Income">
                            <i class="far fas fa-plus-circle"></i>
                        </button>
                    </div>

                    <div id="addIncome"></div>
                </div>

                <div class="col-sm-6 expDiv">
                    <label>Expense Detail</label>

                    <hr/>

                    <ul>
                        @if($errors->has('expRate.*'))
                            <li>
                                <span class="text-danger">{{ 'The rate field is required.' }}</span>
                            </li>
                        @endif
                    </ul>

                    <div class="row">
                        <div class="col-sm-9">
                            <label>Description</label>
                        </div>
                        
                        <div class="col-sm-2">
                            <label>Rate<span class="text text-danger">*</span></label>
                        </div>
                    </div>

                    <div class="input-group">
                        <input type="text" class="form-control col-sm-9" name="expDesc[]" placeholder="Description" />
                        <input type="text" class="form-control col-sm-2 ml-1 allownumericwithdecimal" name="expRate[]" placeholder="Rate" maxlength="5" autocomplete="off"/>

                        <button type="button" class="btn btn-warning ml-1" id="addMoreExp" title="Add More Expense">
                            <i class="far fas fa-plus-circle"></i>
                        </button>
                    </div>
                    
                    <div id="addExpense"></div>
                </div>
            </div>            

            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.income-expenses.index')}}" class="btn btn-default">
                    {{ trans('global.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/income_expense/add_edit.js?'.time())}}" ></script>
@endsection