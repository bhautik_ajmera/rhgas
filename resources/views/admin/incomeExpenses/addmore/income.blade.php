<div class="input-group mt-3" id="{{'newIncome_'.$count}}">
    <input type="text" class="form-control col-sm-9" name="incDesc[]" placeholder="Description" />
    <input type="text" class="form-control col-sm-2 ml-1 allownumericwithdecimal" name="incRate[]" placeholder="Rate" maxlength="5" autocomplete="off" />

    <button type="button" class="btn btn-danger ml-1 remove_inc" id="{{$count}}" title="Remove">
        <i class="far fas fa-minus-circle"></i>
    </button>
</div>