@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        {{'View Cylinder Issue'}}
    </div>

    <input type="hidden" name="cylIssId" id="cylIssId" value="{{$cylIssue->id}}" />
    <div class="card-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>
                        {{'Branch/Distributor'}}
                    </label>
                    <select class="form-control select2" name="branch_id" id="branch_id" disabled="disabled">
                        @foreach($branches as $id => $branch)
                            <option value="{{ $id }}" {{ (old('branch_id') ? old('branch_id') : $cylIssue->branch_id ?? '') == $id ? 'selected' : '' }}>{{ $branch }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>
                        {{'Customer'}}
                    </label>

                    <select class="form-control select2 {{ $errors->has('cus_id') ? 'is-invalid' : '' }}" name="cus_id" id="cus_id" disabled="disabled">
                        <option value="">Please Select</option>

                        @foreach($customers as $id => $cname)
                            @if($cylIssue->cus_id == $id)
                                <option value="{{ $id }}" selected="selected">
                                    {{ $cname }}
                                </option>
                            @else
                                <option value="{{ $id }}" {{ old('cus_id') == $id ? 'selected' : '' }}>
                                    {{ $cname }}
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>
                        {{'PO Number'}}
                    </label>
                    <input class="form-control" type="text" name="po_num" id="po_num" autocomplete="off" readonly="readonly" value="{{$cylIssue->po_num}}" />
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>
                        {{'PO Date'}}
                    </label>
                    <input class="form-control" type="text" name="po_date" id="po_date" autocomplete="off" readonly="readonly" value="{{(!is_null($cylIssue->po_date))?date('d-m-Y',strtotime($cylIssue->po_date)):''}}" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>
                        {{'System Challan No'}}
                    </label>
                    <input class="form-control" type="text" name="sys_chal_no" id="sys_chal_no" autocomplete="off" readonly="readonly" value="{{$cylIssue->sys_chal_no}}" />
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>
                        {{'Delivery Date'}}
                    </label>
                    <input class="form-control" type="text" name="delivery_date" id="delivery_date" value="{{date('d-m-Y',strtotime($cylIssue->delivery_date))}}" autocomplete="off" readonly="readonly" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>
                        {{'Transportation'}}
                    </label>
                    <input class="form-control" type="text" name="trans" id="trans" value="{{ old('trans', $cylIssue->trans) }}" autocomplete="off" readonly="readonly" />
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>
                        {{'Vehicle No'}}
                    </label>
                    <input class="form-control" type="text" name="vehicle_no" id="vehicle_no" value="{{$cylIssue->veh_no}}" autocomplete="off" maxlength="15" readonly="readonly" />
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>
                        {{'Mobile Number'}}
                    </label>
                    <input class="form-control" type="text" name="mob_no" id="mob_no" value="{{$cylIssue->mob_no}}" autocomplete="off" readonly="readonly" />
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>
                        {{'Address'}}
                    </label>
                    <textarea class="form-control" name="address" id="address" readonly="readonly">{{$cylIssue->address}}</textarea>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>
                        {{'Remarks'}}
                    </label>
                    <textarea class="form-control" name="remark" id="remark" readonly="readonly">{{ old('remark', $cylIssue->remark) }}</textarea>
                </div>
            </div>
        </div>
        <hr/>
        
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-striped table-sm" id="listing">
                    <thead>
                        <tr>
                            <th scope="col" width="25%">
                                <center>
                                    Gas<span class="text text-danger">*</span>
                                </center>
                            </th>
                            
                            <th scope="col" width="40%">
                                <center>
                                    Cylinder No<span class="text text-danger">*</span>
                                </center>
                            </th>
                      
                            <th scope="col" width="25%">
                                <center>
                                    Basic Rate
                                </center>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cylIssueDetails as $key => $cylIssueDetail)
                            <tr id="{{'storedDetail_'.$cylIssueDetail->id}}">
                                <td>
                                    <select class="form-control gas" name="gas[]" id="{{'gas_'.$key}}" disabled="disabled">
                                        @foreach($gases as $index => $gas)
                                            @if($cylIssueDetail->gas_id == $index)
                                                <option value="{{$index}}" selected="selected">
                                                    {{$gas}}
                                                </option>
                                            @else
                                                <option value="{{$index}}">{{$gas}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </td>

                                <td>
                                    <select class="form-control cyl_no text-uppercase" name="cylNo[]" id="{{'cyl_'.$key}}" disabled="disabled">
                                        <option value="">Select Cylinder No</option>
                                        @foreach($cylIssueDetail['cylinderes'] as $id => $cylinder)
                                            @if($cylIssueDetail->cyl_id == $id)
                                                <option value="{{$id}}" selected="selected">
                                                    {{$cylinder}}
                                                </option>
                                            @else
                                                <option value="{{$id}}">
                                                    {{$cylinder}}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </td>

                                <td>
                                    <div class="input-group">
                                        <input type="text" class="form-control allownumericwithdecimal rate" name="rate[]" id="{{'rate_'.$key}}" value="{{sprintf('%.2f',$cylIssueDetail['rate'])}}" autocomplete="off" readonly="readonly" />

                                        <div class="input-group-append">
                                            <span class="input-group-text">RS</span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        <tr>
                            <td colspan="2">
                                <div class="text text-right mt-2">
                                    <strong>Total</strong>
                                </div>
                            </td>
                            <td >
                                <div class="input-group">
                                    <input type="text" class="form-control" name="total" id="total" autocomplete="off" value="0.00" readonly="readonly" placeholder="Total" />
                                    
                                    <div class="input-group-append">
                                        <span class="input-group-text">RS</span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="form-group">
            @if(!$cylIssue->is_invoice_created)
                <a class="btn btn-info" href="{{ route('admin.cylinder-issue.edit', $cylIssue->id) }}">
                    {{ trans('global.back_to_edit') }}
                </a>
            @endif

            <a href="{{route('admin.cylinder-issue.index')}}" class="btn btn-default">
                {{ trans('global.cancel') }}
            </a>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/cylinder-issue/add_edit.js?'.time())}}" ></script>
@endsection