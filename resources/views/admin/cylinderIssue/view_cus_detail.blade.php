<div class="modal-header">
    <h4 class="modal-title">
    	{{isset($customer)?$customer->companny_name.' Detail':''}}
    </h4>
    
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div class="modal-body">
    <table class="table">
    	<tr>
    		<th>Name</th>
    		<td>
    			{{isset($customer)?$customer->companny_name:''}}
    		</td>
    	</tr>

    	<tr>
    		<th>Code</th>
    		<td>
    			{{isset($customer)?$customer->code:''}}
    		</td>
    	</tr>

    	<tr>
    		<th>Email</th>
    		<td>
    			{{isset($customer)?$customer->email:''}}
    		</td>
    	</tr>

    	<tr>
    		<th>Branch</th>
    		<td>
    			{{isset($customer)?$customer->bname:''}}
    		</td>
    	</tr>

    	<tr>
    		<th width="30%">Contact Person</th>
    		<td>
    			{{isset($customer)?$customer->contact_person:''}}
    		</td>
    	</tr>

    	<tr>
    		<th>Address Line 1</th>
    		<td>
    			{{isset($customer)?$customer->address:''}}
    		</td>
    	</tr>

        <tr>
            <th>Address Line 2</th>
            <td>
                {{isset($customer)?$customer->address2:''}}
            </td>
        </tr>

    	<tr>
    		<th>Pin Code</th>
    		<td>
    			{{isset($customer)?$customer->pin_code:''}}
    		</td>
    	</tr>

    	<tr>
    		<th>State</th>
    		<td>
    			{{isset($customer)?$customer->sname:''}}
    		</td>
    	</tr>

    	<tr>
    		<th>City</th>
    		<td>
    			{{isset($customer)?$customer->cname:''}}
    		</td>
    	</tr>

    	<tr>
    		<th>Mobile Number</th>
    		<td>
    			{{isset($customer)?$customer->mob_no:''}}
    		</td>
    	</tr>

    	<tr>
    		<th>Alternate Mobile Number</th>
    		<td>
    			{{isset($customer)?$customer->alternate_mob_no:''}}
    		</td>
    	</tr>

    	<tr>
    		<th>GST Number</th>
    		<td class="text-uppercase">
    			{{isset($customer)?$customer->gstno:''}}
    		</td>
    	</tr>

    	<tr>
    		<th>Addhar Card No</th>
    		<td>
    			{{isset($customer)?$customer->addhar_card_no:''}}
    		</td>
    	</tr>

    	<tr>
    		<th>Addhar Card Address</th>
    		<td>
    			{{isset($customer)?$customer->addhar_card_address:''}}
    		</td>
    	</tr>

    	<tr>
    		<th>Remark</th>
    		<td>
    			{{isset($customer)?$customer->remark:''}}
    		</td>
    	</tr>
    </table>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default float-right" data-dismiss="modal">
        Close
    </button>
</div>