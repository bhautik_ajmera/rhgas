<select class="form-control select2 {{ $errors->has('cus_id') ? 'is-invalid' : '' }}" name="cus_id" id="cus_id" >
	<option value="">Please Select</option>

	@if(isset($customers))
	    @foreach($customers as $id => $cname)
	    	@if(isset($cylIssue) && $cylIssue->cus_id == $id)
	    		<option value="{{ $id }}" selected="selected">
	        		{{ $cname }}
	    		</option>
	    	@else
	        	<option value="{{ $id }}" {{ old('cus_id') == $id ? 'selected' : '' }}>
	        		{{ $cname }}
	        	</option>
	    	@endif
	    @endforeach
	@endif
</select>