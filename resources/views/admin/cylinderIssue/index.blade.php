@extends('layouts.admin')

@section('styles')
    <style type="text/css">
        .buttons-select-all,.buttons-select-none{
            display: none;
        }

        table.dataTable tbody td.select-checkbox:before, table.dataTable tbody th.select-checkbox:before{
            display: none;
        }
    </style>
@endsection

@section('content')
@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif

@if(session()->has('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
@endif

<div class="card">
    <div class="card-header">
        <span>
            {{'Cylinder Issue List'}}
        </span>

        @can('cylinder_issue_create')
            <a class="btn btn-success float-right" href="{{route('admin.cylinder-issue.create')}}">
                <i class="fa fa-plus"></i>
            </a>
        @endcan
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover datatable datatable-Customer">
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            Branch/Distributor
                        </th>
                        <th>
                            Customer
                        </th>
                        <th>
                            PO Number
                        </th>
                        <th>
                            PO Date
                        </th>
                        <th>
                            System Challan No
                        </th>
                        <th>
                            Delivery Date
                        </th>
                        <th width="10%">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($result as $key => $value)
                        <tr data-entry-id="{{ $value->id }}">
                            <td>
                                {{ $value->id ?? '' }}
                            </td>
                            <td>
                                {{ $value->getBranch($value->branch_id) ?? '' }}
                            </td>
                            <td>
                                {{ $value->cname ?? '' }}
                            </td>
                            <td>
                                {{ $value->po_num ?? '' }}
                            </td>
                            <td>
                                @if(!is_null($value->po_date))
                                    {{date('d-m-Y',strtotime($value->po_date))}}
                                @else
                                    {{'-'}}
                                @endif
                            </td>
                            <td>
                                {{ $value->sys_chal_no ?? '' }}
                            </td>
                            <td>
                                {{ date('d-m-Y',strtotime($value->delivery_date)) }}
                            </td>
                            <td>
                                @can('cylinder_issue_show')
                                    <a class="btn btn-xs btn-primary" href="{{route('admin.cylinder-issue.show', $value->id)}}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                @endcan

                                @if(!$value->is_invoice_created)
                                    @can('cylinder_issue_edit')
                                        <a class="btn btn-xs btn-info" href="{{route('admin.cylinder-issue.edit', $value->id)}}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    @endcan

                                    @can('cylinder_issue_delete')
                                        <form action="{{ route('admin.cylinder-issue.destroy', $value->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit" class="btn btn-xs btn-danger">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    @endcan
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@parent
<script>
$(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    
    // @can('cylinder_issue_delete')
    // let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
    // let deleteButton = {
    //     text: deleteButtonTrans,
    //     url: "{{ route('admin.cylinderIssue.massDestroy') }}",
    //     className: 'btn-danger',
    //     action: function (e, dt, node, config) {
    //         var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
    //             return $(entry).data('entry-id')
    //         });

    //         if (ids.length === 0) {
    //             alert('{{ trans('global.datatables.zero_selected') }}')
    //             return
    //         }

    //         if (confirm('{{ trans('global.areYouSure') }}')) {
    //             $.ajax({
    //                 headers: {'x-csrf-token': _token},
    //                 method: 'POST',
    //                 url: config.url,
    //                 data: { ids: ids, _method: 'DELETE' }})
    //                     .done(function () { location.reload() })
    //         }
    //     }
    // }
    // dtButtons.push(deleteButton)
    // @endcan

    $.extend(true, $.fn.dataTable.defaults, {
        orderCellsTop: true,
        order: [[ 0, 'desc' ]],
        pageLength: 10,
    });
    
    let table = $('.datatable-Customer:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
        .columns.adjust();
    }); 
})
</script>
@endsection