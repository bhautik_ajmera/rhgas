@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        {{'Edit Cylinder Issue'}}
    </div>

    <div class="card-body">
    	<form method="POST" action="{{ route("admin.cylinder-issue.update",[$cylIssue->id]) }}" id="cylIssueFrm">
        @method('PUT')
        @csrf
            <input type="hidden" name="cylIssId" id="cylIssId" value="{{$cylIssue->id}}" />
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required">
                            {{'Branch/Distributor'}}
                        </label>
                        <select class="form-control select2 {{ $errors->has('branch_id') ? 'is-invalid' : '' }}" name="branch_id" id="branch_id" >
                            @foreach($branches as $id => $branch)
                                <option value="{{ $id }}" {{ (old('branch_id') ? old('branch_id') : $cylIssue->branch_id ?? '') == $id ? 'selected' : '' }}>{{ $branch }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('branch_id'))
                            <span class="text-danger">{{ 'The branch/distributor field is required.' }}</span>
                        @endif
                        <div id="branchError"></div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required">
                            {{'Customer'}}
                        </label>

                        <i class="float-right fas fa-eye mr-1" id="view_cus" title="View Customer Detail" data-toggle="modal"></i>

                        <div id="renderCus">
                            @include('admin.cylinderIssue.customer')
                        </div>

                        @if($errors->has('cus_id'))
                            <span class="text-danger">{{ 'The customer field is required.' }}</span>
                        @endif
                        <div id="cusError"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'PO Number'}}
                        </label>
                        <input class="form-control" type="text" name="po_num" id="po_num" autocomplete="off" readonly="readonly" value="{{$cylIssue->po_num}}" />
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'PO Date'}}
                        </label>

                        <input class="form-control datepick" type="text" name="po_date" id="po_date" autocomplete="off" readonly="readonly" value="{{ old('po_date', !is_null($cylIssue->po_date)?date('d-m-Y',strtotime($cylIssue->po_date)):'') }}" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'System Challan No'}}
                        </label>
                        <input class="form-control" type="text" name="sys_chal_no" id="sys_chal_no" autocomplete="off" readonly="readonly" value="{{$cylIssue->sys_chal_no}}" />
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required">
                            {{'Delivery Date'}}
                        </label>
                        
                        <input class="form-control ddate {{ $errors->has('delivery_date') ? 'is-invalid' : '' }}" type="text" name="delivery_date" id="delivery_date" value="{{ old('delivery_date', date('d-m-Y',strtotime($cylIssue->delivery_date))) }}" autocomplete="off" readonly="readonly" />


                        @if($errors->has('delivery_date'))
                            <span class="text-danger">{{ $errors->first('delivery_date') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'Transportation'}}
                        </label>
                        <input class="form-control" type="text" name="trans" id="trans" value="{{ old('trans', $cylIssue->trans) }}" autocomplete="off" readonly="readonly" />
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'Vehicle No'}}
                        </label>
                        <input class="form-control text-uppercase {{ $errors->has('vehicle_no') ? 'is-invalid' : '' }}" type="text" name="vehicle_no" id="vehicle_no" value="{{ old('vehicle_no', $cylIssue->veh_no) }}" autocomplete="off" maxlength="15" />
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'Mobile Number'}}
                        </label>
                        <input class="form-control" type="text" name="mob_no" id="mob_no" value="{{ old('mob_no', $cylIssue->mob_no) }}" autocomplete="off" readonly="readonly" />
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'Address'}}
                        </label>
                        <textarea class="form-control" name="address" id="address" readonly="readonly">{{ old('address',$cylIssue->address) }}</textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'Remarks'}}
                        </label>
                        <textarea class="form-control" name="remark" id="remark">{{ old('remark', $cylIssue->remark) }}</textarea>
                    </div>
                </div>
            </div>
            <hr/>
            
            <ul>
                @if($errors->has('cylNo.*'))
                    <li>
                        <span class="text-danger">{{ 'The Cylinder No field is required.' }}</span>
                    </li>
                @endif
                
                @if($errors->has('gas.*'))
                    <li>
                        <span class="text-danger">{{ 'The Gas field is required.' }}</span>
                    </li>
                @endif
            </ul>
            
            <div id="test">
                <input type="hidden" name="rowCount" id="rowCount" value="{{(count($cylIssueDetails)-1)}}" />

                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-striped table-sm" id="listing">
                            <thead>
                                <tr>
                                    <th scope="col" width="25%">
                                        <center>
                                            Gas<span class="text text-danger">*</span>
                                        </center>
                                    </th>
                                    
                                    <th scope="col" width="40%">
                                        <center>
                                            Cylinder No<span class="text text-danger">*</span>
                                        </center>
                                    </th>
                              
                                    <th scope="col" width="25%">
                                        <center>
                                            Basic Rate
                                        </center>
                                    </th>
                                    
                                    <th scope="col" width="10%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($cylIssueDetails as $key => $cylIssueDetail)
                                    @php
                                        $allowToChange = $cylIssueDetail::isProFurther($cylIssueDetail->cyl_id,$cylIssueDetail->updated_at,$cylIssueDetail->cstatus);
                                    @endphp

                                    <tr id="{{'storedDetail_'.$cylIssueDetail->id}}" class="{{($allowToChange)?'table-success':''}}">
                                        <td>
                                            <select class="form-control gas {{($allowToChange)?'disable_select' : ''}}" name="gas[]" id="{{'gas_'.$key}}">
                                                @foreach($gases as $index => $gas)
                                                    @if($cylIssueDetail->gas_id == $index)
                                                        <option value="{{$index}}" selected="selected">
                                                            {{$gas}}
                                                        </option>
                                                    @else
                                                        <option value="{{$index}}">{{$gas}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>

                                        <td>
                                            <select class="form-control cyl_no text-uppercase {{($allowToChange)?'disable_select' : ''}}" name="cylNo[]" id="{{'cyl_'.$key}}">
                                                <option value="">Select Cylinder No</option>
                                                @foreach($cylIssueDetail['cylinderes'] as $id => $cylinder)
                                                    @if($cylIssueDetail->cyl_id == $id)
                                                        <option value="{{$id}}" selected="selected">
                                                            {{$cylinder}}
                                                        </option>
                                                    @else
                                                        <option value="{{$id}}">
                                                            {{$cylinder}}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>

                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control allownumericwithdecimal rate" name="rate[]" id="{{'rate_'.$key}}" value="{{sprintf('%.2f',$cylIssueDetail['rate'])}}" autocomplete="off" readonly="readonly" />

                                                <input type="hidden" name="rent[]" id="{{'rent_'.$key}}" value="{{$cylIssueDetail['rent']}}" />
                                                <input type="hidden" name="startRent[]" id="{{'srent_'.$key}}" value="{{$cylIssueDetail['start_rent']}}" />

                                                <div class="input-group-append">
                                                    <span class="input-group-text">RS</span>
                                                </div>
                                            </div>
                                        </td>

                                        <input type="hidden" name="status[]" value="{{$cylIssueDetail['status']}}" />
                                        <td>
                                             @if($key == 0)
                                                <button type="button" class="btn btn-warning" id="addMore" title="Add More">
                                                    <i class="far fas fa-plus-circle"></i>
                                                </button>
                                            @elseif(!$allowToChange)
                                                <button type="button" class="btn btn-danger delete" id="{{$cylIssueDetail->id}}" data-row="{{$key}}" title="Delete">
                                                    <i class="far fas fa-trash"></i>
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach

                                <tr>
                                    <td colspan="2">
                                        <div class="text text-right mt-2">
                                            <strong>Total</strong>
                                        </div>
                                    </td>
                                    <td >
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="total" id="total" autocomplete="off" value="0.00" readonly="readonly" placeholder="Total" />
                                            
                                            <div class="input-group-append">
                                                <span class="input-group-text">RS</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.cylinder-issue.index')}}" class="btn btn-default">
                    {{ trans('global.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="customer-modal">
    <div class="modal-dialog">
        <div class="modal-content" id="renderCusDetail">
            @include('admin.cylinderIssue.view_cus_detail')
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/cylinder-issue/add_edit.js?'.time())}}" ></script>
@endsection