@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        {{'Create Cylinder Issue'}}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.cylinder-issue.store") }}" enctype="multipart/form-data" id="cylIssueFrm">
        @csrf
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required">
                            {{'Branch/Distributor'}}
                        </label>
                        <select class="form-control select2 {{ $errors->has('branch_id') ? 'is-invalid' : '' }}" name="branch_id" id="branch_id" >
                            @foreach($branches as $id => $branch)
                                <option value="{{ $id }}" {{ old('branch_id') == $id ? 'selected' : '' }}>{{ $branch }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('branch_id'))
                            <span class="text-danger">{{ 'The branch/distributor field is required.' }}</span>
                        @endif
                        <div id="branchError"></div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required">
                            {{'Customer'}}                            
                        </label>
                        
                        <span class="float-right">
                            <a href="javascript:void(0);" id="cyl_report">
                                <i class="fas fa-file mr-1" title="View Cylinder Report"></i>
                            </a>

                            <a href="javascript:void(0);">
                                <i class="fas fa-eye mr-1" id="view_cus" title="View Customer Detail" data-toggle="modal"></i>
                            </a>
                        </span>

                        <div id="renderCus">
                            @include('admin.cylinderIssue.customer')
                        </div>
                        
                        @if($errors->has('cus_id'))
                            <span class="text-danger">{{ 'The customer field is required.' }}</span>
                        @endif
                        <div id="cusError"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'PO Number'}}
                        </label>
                        
                        <div>
                            <i>{{"Auto Generate"}}</i>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'PO Date'}}
                        </label>
                        
                        <input class="form-control datepick" type="text" name="po_date" id="po_date" value="{{ old('po_date') }}" autocomplete="off" readonly="readonly" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'System Challan No'}}
                        </label>
                        
                        <div>
                            <i>{{"Auto Generate"}}</i> 
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required">
                            {{'Delivery Date'}}
                        </label>
                        <input class="form-control ddate {{ $errors->has('delivery_date') ? 'is-invalid' : '' }}" type="text" name="delivery_date" id="delivery_date" value="{{ old('delivery_date') }}" autocomplete="off" readonly="readonly" />
                        @if($errors->has('delivery_date'))
                            <span class="text-danger">{{ $errors->first('delivery_date') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'Transportation'}}
                        </label>
                        <input class="form-control" type="text" name="trans" id="trans" value="{{ old('trans', '') }}" autocomplete="off" readonly="readonly" />
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'Vehicle No'}}
                        </label>
                        <input class="form-control text-uppercase {{ $errors->has('vehicle_no') ? 'is-invalid' : '' }}" type="text" name="vehicle_no" id="vehicle_no" value="{{ old('vehicle_no', '') }}" autocomplete="off" maxlength="15" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'Mobile Number'}}
                        </label>
                        <input class="form-control" type="text" name="mob_no" id="mob_no" value="{{ old('mob_no', '') }}" autocomplete="off" readonly="readonly" />
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'Address'}}
                        </label>
                        <textarea class="form-control" name="address" id="address" readonly="readonly">{{ old('address') }}</textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{'Remarks'}}
                        </label>
                        <textarea class="form-control" name="remark" id="remark">{{ old('remark') }}</textarea>
                    </div>
                </div>
            </div>

            <hr/>

            <ul>
                @if($errors->has('cylNo.*'))
                    <li>
                        <span class="text-danger">{{ 'The Cylinder No field is required.' }}</span>
                    </li>
                @endif
                
                @if($errors->has('gas.*'))
                    <li>
                        <span class="text-danger">{{ 'The Gas field is required.' }}</span>
                    </li>
                @endif
            </ul>

            <div id="test">
                <input type="hidden" name="rowCount" id="rowCount" value="0" />
                
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-striped table-sm" id="listing">
                            <thead>
                                <tr>
                                    <th scope="col" width="25%">
                                        <center>
                                            Gas<span class="text text-danger">*</span>
                                        </center>
                                    </th>
                                    
                                    <th scope="col" width="40%">
                                        <center>
                                            Cylinder No<span class="text text-danger">*</span>
                                        </center>
                                    </th>
                              
                                    <th scope="col" width="25%">
                                        <center>
                                            Basic Rate
                                        </center>
                                    </th>
                                    
                                    <th scope="col" width="10%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <select class="form-control gas" name="gas[]" id="gas_0">
                                            <option value="">Select Gas</option>
                                        </select>
                                    </td>

                                    <td>
                                        <select class="form-control cyl_no" name="cylNo[]" data-id="0" id="cyl_0">
                                            <option value="">Select Cylinder No</option>
                                        </select>
                                    </td>

                                    <td>
                                        <div class="input-group">
                                            <input type="text" class="form-control allownumericwithdecimal rate" name="rate[]" id="rate_0" value="0.00" autocomplete="off" readonly="readonly" />
                                            
                                            <input type="hidden" name="rent[]" id="rent_0" value="" />
                                            <input type="hidden" name="startRent[]" id="srent_0" value="" />

                                            <div class="input-group-append">
                                                <span class="input-group-text">RS</span>
                                            </div>
                                        </div>
                                    </td>

                                    <input type="hidden" name="status[]" value="0" />
                                    <td>
                                        <button type="button" class="btn btn-warning" id="addMore" title="Add More">
                                            <i class="far fas fa-plus-circle"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="text text-right mt-2">
                                            <strong>Total</strong>
                                        </div>
                                    </td>
                                    <td >
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="total" id="total" autocomplete="off" value="0.00" readonly="readonly" placeholder="Total" />
                                            
                                            <div class="input-group-append">
                                                <span class="input-group-text">RS</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.cylinder-issue.index')}}" class="btn btn-default">
                    {{ trans('global.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="customer-modal">
    <div class="modal-dialog">
        <div class="modal-content" id="renderCusDetail">
            @include('admin.cylinderIssue.view_cus_detail')
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/cylinder-issue/add_edit.js?'.time())}}" ></script>
@endsection