@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.cylinder.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.cylinders.update", [$cylinder->id]) }}" enctype="multipart/form-data" id="cylFrm">
            @method('PUT')
            @csrf

            <div class="row">
                <?php /*
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="supplier_id">{{ trans('cruds.cylinder.fields.supplier') }}</label>
                        <select class="form-control select2 {{ $errors->has('supplier_id') ? 'is-invalid' : '' }}" name="supplier_id" id="supplier_id">
                            @foreach($suppliers as $id => $supplier)
                                <option value="{{ $id }}" {{ (old('supplier_id') ? old('supplier_id') : $cylinder->supplier->id ?? '') == $id ? 'selected' : '' }}>{{ $supplier }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('supplier_id'))
                            <span class="text-danger">{{ 'The supplier field is required.' }}</span>
                        @endif
                        <div id="companyError"></div>
                        <span class="help-block">{{ trans('cruds.cylinder.fields.supplier_helper') }}</span>
                    </div>
                </div>
                */ ?>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="company_cyl_id">
                            {{ trans('cruds.cylinder.fields.cyl_company') }}
                        </label>
                        <select class="form-control select2 {{ $errors->has('company_cyl_id') ? 'is-invalid' : '' }}" name="company_cyl_id" id="company_cyl_id">
                            @foreach($cylCompanies as $id => $company)
                                <option value="{{ $id }}" {{ (old('company_cyl_id') ? old('company_cyl_id') : $cylinder->company_cyl_id ?? '') == $id ? 'selected' : '' }}>{{ $company }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('company_cyl_id'))
                            <span class="text-danger">{{ 'The company cylinder field is required.' }}</span>
                        @endif
                        <div id="cmpCylError"></div>
                        <span class="help-block">{{ trans('cruds.cylinder.fields.cyl_company_helper') }}</span>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="company_cyl_no">{{ trans('cruds.cylinder.fields.company_cyl_no') }}</label>
                        <input class="form-control text-uppercase {{ $errors->has('company_cyl_no') ? 'is-invalid' : '' }}" type="text" name="company_cyl_no" id="company_cyl_no" value="{{ old('company_cyl_no', $cylinder->company_cyl_no) }}" autocomplete="off" />
                        @if($errors->has('company_cyl_no'))
                            <span class="text-danger">{{ 'The company cyllinder no field is required.' }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.cylinder.fields.company_cyl_no_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="branch_cyl_no">{{ trans('cruds.cylinder.fields.branch_cyl_no') }}</label>
                        <input class="form-control text-uppercase {{ $errors->has('branch_cyl_no') ? 'is-invalid' : '' }}" type="text" name="branch_cyl_no" id="branch_cyl_no" value="{{ old('branch_cyl_no', $cylinder->branch_cyl_no) }}" autocomplete="off" />
                        @if($errors->has('branch_cyl_no'))
                            <span class="text-danger">{{ 'The branch cylinder number field is required.' }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.cylinder.fields.branch_cyl_no_helper') }}</span>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="capacity">{{ trans('cruds.cylinder.fields.capacity') }}</label>
                        <input class="form-control {{ $errors->has('capacity') ? 'is-invalid' : '' }}" type="text" name="capacity" id="capacity" value="{{ old('capacity', $cylinder->capacity) }}" autocomplete="off" />
                        @if($errors->has('capacity'))
                            <span class="text-danger">{{ $errors->first('capacity') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.cylinder.fields.capacity_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="manufacturing_date">{{ trans('cruds.cylinder.fields.manufacturing_date') }}</label>
                        <input class="form-control datepick {{ $errors->has('manufacturing_date') ? 'is-invalid' : '' }}" type="text" name="manufacturing_date" id="manufacturing_date" value="{{ old('manufacturing_date', $cylinder->manufacturing_date) }}" autocomplete="off" readonly="readonly" />
                        @if($errors->has('manufacturing_date'))
                            <span class="text-danger">{{ $errors->first('manufacturing_date') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.cylinder.fields.manufacturing_date_helper') }}</span>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="last_hydrotest_date">{{ trans('cruds.cylinder.fields.last_hydrotest_date') }}</label>
                        <input class="form-control datepick {{ $errors->has('last_hydrotest_date') ? 'is-invalid' : '' }}" type="text" name="last_hydrotest_date" id="last_hydrotest_date" value="{{ old('last_hydrotest_date', $cylinder->last_hydrotest_date) }}" autocomplete="off" readonly="readonly" />
                        @if($errors->has('last_hydrotest_date'))
                            <span class="text-danger">{{ $errors->first('last_hydrotest_date') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.cylinder.fields.last_hydrotest_date_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="gas_id">{{ trans('cruds.cylinder.fields.gas') }}</label>
                        <select class="form-control select2 {{ $errors->has('gas_id') ? 'is-invalid' : '' }}" name="gas_id" id="gas_id" >
                            @foreach($gases as $id => $gas)
                                <option value="{{ $id }}" {{ (old('gas_id') ? old('gas_id') : $cylinder->gas->id ?? '') == $id ? 'selected' : '' }}>{{ $gas }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('gas_id'))
                            <span class="text-danger">{{ 'The alloted to field is required.' }}</span>
                        @endif
                        <div id="allotedtoError"></div>
                        <span class="help-block">{{ trans('cruds.cylinder.fields.gas_helper') }}</span>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="used_for_id">{{ trans('cruds.cylinder.fields.used_for') }}</label>
                        <select class="form-control select2 {{ $errors->has('used_for_id') ? 'is-invalid' : '' }}" name="used_for_id" id="used_for_id">
                            @foreach($used_fors as $id => $used_for)
                                <option value="{{ $id }}" {{ (old('used_for_id') ? old('used_for_id') : $cylinder->used_for->id ?? '') == $id ? 'selected' : '' }}>{{ $used_for }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('used_for_id'))
                            <span class="text-danger">{{ 'The used for field is required.' }}</span>
                        @endif
                        <div id="usedforError"></div>
                        <span class="help-block">{{ trans('cruds.cylinder.fields.used_for_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>
                            {{trans('global.status')}}
                        </label>
                        
                        <input type="text" class="form-control text-capitalize" name="status" id="status" value="{{ old('status', $cylinder->status) }}" readonly="readonly" autocomplete="off" />
                    </div>
                </div>
            </div>            

            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.cylinders.index')}}" class="btn btn-default">
                    {{ trans('global.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/cylinder/add_edit.js?'.time())}}" ></script>
@endsection