@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.cylinder.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                @if($cylinder->status == 'empty')
                    <a class="btn btn-info" href="{{ route('admin.cylinders.edit', $cylinder->id) }}">
                        {{ trans('global.back_to_edit') }}
                    </a>
                @endif

                <a class="btn btn-default" href="{{ route('admin.cylinders.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.cylinder.fields.id') }}
                        </th>
                        <td>
                            {{ $cylinder->id }}
                        </td>
                    </tr>
                    <?php /*
                    <tr>
                        <th>
                            {{ trans('cruds.cylinder.fields.supplier') }}
                        </th>
                        <td>
                            {{ $cylinder->supplier->company_name ?? '' }}
                        </td>
                    </tr>
                    */ ?>
                    <tr>
                        <th>
                            {{ trans('cruds.cylinder.fields.cyl_company') }}
                        </th>
                        <td>
                            {{($cylCom)?$cylCom->name:'-'}}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.cylinder.fields.company_cyl_no') }}
                        </th>
                        <td class="text-uppercase">
                            {{ $cylinder->company_cyl_no }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.cylinder.fields.branch_cyl_no') }}
                        </th>
                        <td class="text-uppercase">
                            {{ !is_null($cylinder->branch_cyl_no)?$cylinder->branch_cyl_no:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.cylinder.fields.capacity') }}
                        </th>
                        <td>
                            {{ $cylinder->capacity }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.cylinder.fields.manufacturing_date') }}
                        </th>
                        <td>
                            {{ $cylinder->manufacturing_date }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.cylinder.fields.last_hydrotest_date') }}
                        </th>
                        <td>
                            {{ $cylinder->last_hydrotest_date }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.cylinder.fields.gas') }}
                        </th>
                        <td>
                            {{ $cylinder->gas->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.cylinder.fields.used_for') }}
                        </th>
                        <td>
                            {{ $cylinder->used_for->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{trans('global.status')}}
                        </th>
                        <td class="text-capitalize">
                            {{ $cylinder->status }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{'Discontinue Cylinder'}}
                        </th>
                        <td class="text-capitalize">
                            {{($cylinder->dis_cyl)?'Yes':'No'}}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{'Discontinue Note'}}
                        </th>
                        <td class="text-capitalize">
                            {{!is_null($cylinder->dis_note)?$cylinder->dis_note:'-'}}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                @if($cylinder->status == 'empty')
                    <a class="btn btn-info" href="{{ route('admin.cylinders.edit', $cylinder->id) }}">
                        {{ trans('global.back_to_edit') }}
                    </a>
                @endif
                
                <a class="btn btn-default" href="{{ route('admin.cylinders.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>
@endsection