@extends('layouts.admin')

@section('styles')
    <style type="text/css">
        .buttons-select-all,.buttons-select-none{
            display: none;
        }

        table.dataTable tbody td.select-checkbox:before, table.dataTable tbody th.select-checkbox:before{
            display: none;
        }
    </style>
@endsection

@section('content')
@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif

@if(session()->has('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
@endif

<div class="card">
    <div class="card-header">
        <span>
            {{ trans('cruds.cylinder.title_singular') }} {{ trans('global.list') }}
        </span>

        @can('cylinder_create')
            <a class="btn btn-success float-right" href="{{ route('admin.cylinders.create') }}">
                <i class="fa fa-plus"></i>
            </a>
        @endcan
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Cylinder">
                <thead>
                    <tr>
                        <th>
                            {{ trans('cruds.cylinder.fields.id') }}
                        </th>
                        <?php /*
                        <th>
                            {{ trans('cruds.cylinder.fields.supplier') }}
                        </th>
                        */ ?>
                        <th>
                            {{'Company'}}
                        </th>
                        <th>
                            {{ trans('cruds.cylinder.fields.branch_cyl_no') }}
                        </th>
                        <th>
                            {{ trans('cruds.cylinder.fields.capacity') }}
                        </th>
                        <!-- <th>
                            {{ trans('cruds.cylinder.fields.manufacturing_date') }}
                        </th> -->
                        <th>
                            {{ trans('cruds.cylinder.fields.last_hydrotest_date') }}
                        </th>
                        <th>
                            {{ trans('cruds.cylinder.fields.gas') }}
                        </th>
                        <th>
                            {{ trans('cruds.cylinder.fields.used_for') }}
                        </th>
                        <th>
                            {{'Discontinue Status'}}
                        </th>
                        <th>
                            {{trans('global.status')}}
                        </th>
                        <th width="10%">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($cylinders as $key => $cylinder)
                        <tr data-entry-id="{{ $cylinder->id }}" class="{{($cylinder->dis_cyl)?'table-danger':''}}">
                            <td>
                                {{ $cylinder->id ?? '' }}
                            </td>
                            <?php /*
                            <td>
                                {{ $cylinder->supplier->company_name ?? '' }}
                            </td>
                            */ ?>
                            <td>
                                {{array_key_exists($cylinder->company_cyl_id,$cylCompanies)?$cylCompanies[$cylinder->company_cyl_id]:null}}
                            </td>
                            <td class="text-uppercase">
                                {{ $cylinder->branch_cyl_no ?? '' }}
                            </td>
                            <td>
                                {{ $cylinder->capacity ?? '' }}
                            </td>
                            <!-- <td>
                                {{ $cylinder->manufacturing_date ?? '' }}
                            </td> -->
                            <td>
                                {{ $cylinder->last_hydrotest_date ?? '' }}
                            </td>
                            <td>
                                {{ $cylinder->gas->name ?? '' }}
                            </td>
                            <td>
                                {{ $cylinder->used_for->name ?? '' }}
                            </td>
                            <td>
                                {{($cylinder->dis_cyl)?'Yes':'No'}}
                            </td>
                            <td class="text-capitalize">
                                @if($cylinder->dis_cyl)
                                    {{'(Discontinue Cylinder)'}}
                                    <br/>
                                @endif
                                
                                {{$cylinder->status}}
                            </td>
                            <td>
                                @can('cylinder_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.cylinders.show', $cylinder->id) }}" title="View">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                @endcan

                                @if($cylinder->status == 'empty')
                                    @can('cylinder_edit')
                                        <a class="btn btn-xs btn-info" href="{{ route('admin.cylinders.edit', $cylinder->id) }}" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    @endcan

                                    @can('cylinder_delete')
                                        <form action="{{ route('admin.cylinders.destroy', $cylinder->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit" class="btn btn-xs btn-danger" title="Delete">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    @endcan
                                @endif

                                <a class="btn btn-xs btn-warning btndis" href="javascript:void(0);" title="Discontinue Cylinder" id="{{$cylinder->id}}" data-status="{{$cylinder->dis_cyl}}" data-note="{{$cylinder->dis_note}}">
                                    <i class="fa fa-minus-circle"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="disCylModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Discontinue Cylinder</h5>
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <form method="post" action="{{route('admin.cylinders.discontinue')}}">
            @csrf
            <input type="hidden" name="hidcid" id="hidcid" value="" />
                <div class="modal-body">
                    <div class="form-group">
                        <div class="form-check form-check-inline">
                            <label>{{'Discontine'}}</label>
                            <input type="checkbox" class="form-check-input ml-1 mb-1" name="dis_cyl" id="dis_cyl" />
                        </div>

                        <textarea class="form-control" placeholder="Note" name="dis_note" id="dis_note"></textarea>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@parent
<script>
    $(function () {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
        
    //     @can('cylinder_delete')
    //         let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
    //         let deleteButton = {
    //             text: deleteButtonTrans,
    //             url: "{{ route('admin.cylinders.massDestroy') }}",
    //             className: 'btn-danger',
    //             action: function (e, dt, node, config) {
    //               var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
    //                   return $(entry).data('entry-id')
    //               });

    //               if (ids.length === 0) {
    //                 alert('{{ trans('global.datatables.zero_selected') }}')

    //                 return
    //               }

    //               if (confirm('{{ trans('global.areYouSure') }}')) {
    //                 $.ajax({
    //                   headers: {'x-csrf-token': _token},
    //                   method: 'POST',
    //                   url: config.url,
    //                   data: { ids: ids, _method: 'DELETE' }})
    //                   .done(function () { location.reload() })
    //               }
    //         }
    //     }
    //     dtButtons.push(deleteButton)
    // @endcan

    $.extend(true, $.fn.dataTable.defaults, {
        orderCellsTop: true,
        order: [[ 0, 'desc' ]],
        pageLength: 10,
    });
    
    let table = $('.datatable-Cylinder:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });
     
    $(document).on("click", ".btndis", function(){
        $('#hidcid').val($(this).attr('id'));
        $('#dis_cyl').prop('checked', ($(this).attr('data-status') == 1)?true:false);
        $('#dis_note').text($(this).attr('data-note'));
        $('#disCylModal').modal('show');
    });
})

</script>
@endsection