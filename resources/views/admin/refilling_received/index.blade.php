@extends('layouts.admin')

@section('content')
	@if(session()->has('success'))
	    <div class="alert alert-success">
	        {{ session()->get('success') }}
	    </div>
	@endif

	@if(session()->has('error'))
	    <div class="alert alert-danger">
	        {{ session()->get('error') }}
	    </div>
	@endif

	<div class="card">
	    <div class="card-header">
	        <span>
	            {{'Refilling Received'}}
	        </span>
	    </div>

	    <div class="card-body">
	    	<form action="javascript:void(0);" method="post" id="searchFrm">
	    	@csrf
		        <div class="row">
		        	<div class="col-sm-3">
		                <div class="form-group">
		                    <label class="required">Select Supplier</label>

		                    <select class="form-control select2" name="supId" id="supId">
		                    	<option value="">Select</option>
		                    	@foreach($supplier as $id => $value)
		                    		<option value="{{$id}}">
		                    			{{$value}}
		                    		</option>
		                    	@endforeach
		                    </select>

		                    <div id="supError"></div>
		                    @if($errors->has('supId'))
		                        <span class="text-danger">{{ 'The supplier field is required.' }}</span>
		                    @endif
	                    </div>
		            </div>

		        	<div class="col-sm-3">
		                <div class="form-group">
		                    <label class="required">Select System Challan No</label>

		                    <div id="renderChalNo">
		                   		@include('admin.refilling_received.challan_no')
		                    </div>

		                    <div id="chaError"></div>
		                    @if($errors->has('sysChalNo'))
		                        <span class="text-danger">{{ 'The system challan no field is required.' }}</span>
		                    @endif
	                    </div>
		            </div>

		            <div class="col-sm-1">
		            	<label>&nbsp;</label>
		            	<div class="form-group">
		                    <input type="submit" class="btn btn-success" id="search" value="Search">
		                </div>
		            </div>
		        </div>
	    	</form>

	    	<div id="renderList">
                @include('admin.refilling_received.list')
            </div>
	    </div>
	</div>
@endsection

@section('scripts')
	@parent
    <script src="{{ asset('js/ref_rec/refilling_received.js') }}"></script>
@endsection