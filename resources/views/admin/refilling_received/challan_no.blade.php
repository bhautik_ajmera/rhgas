@if(isset($refilling))
	<select class="form-control select2" name="sysChalNo" id="sysChalNo">
		<option value="">Select</option>
		@foreach($refilling as $index => $value)
			<option value="{{$value}}">
				{{$value}}
			</option>
		@endforeach
	</select>
@else
	<select class="form-control select2" name="sysChalNo" id="sysChalNo">
		<option value="">Select</option>
	</select>
@endif