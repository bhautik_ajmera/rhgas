@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.ga.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-info" href="{{ route('admin.gas.edit', $ga->id) }}">
                    {{ trans('global.back_to_edit') }}
                </a>

                <a class="btn btn-default" href="{{ route('admin.gas.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.ga.fields.id') }}
                        </th>
                        <td>
                            {{ $ga->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.ga.fields.code') }}
                        </th>
                        <td>
                            {{ $ga->code }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.ga.fields.name') }}
                        </th>
                        <td>
                            {{ $ga->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.ga.fields.hsn') }}
                        </th>
                        <td>
                            {{ $ga->hsn_code }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{'GST'}}
                        </th>
                        <td>
                            {{ !is_null($ga->gst)?$ga->gst.'%':'-' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-info" href="{{ route('admin.gas.edit', $ga->id) }}">
                    {{ trans('global.back_to_edit') }}
                </a>

                <a class="btn btn-default" href="{{ route('admin.gas.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link active show" href="#gas_cylinders" role="tab" data-toggle="tab">
                {{ trans('cruds.cylinder.fields.tab_allocated') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#used_for_cylinders" role="tab" data-toggle="tab">
                {{ trans('cruds.cylinder.fields.tab_usedfor') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active show" role="tabpanel" id="gas_cylinders">
            @includeIf('admin.gas.relationships.gasCylinders', ['cylinders' => $ga->gasCylinders])
        </div>
        <div class="tab-pane" role="tabpanel" id="used_for_cylinders">
            @includeIf('admin.gas.relationships.usedForCylinders', ['cylinders' => $ga->usedForCylinders])
        </div>
    </div>
</div>
@endsection