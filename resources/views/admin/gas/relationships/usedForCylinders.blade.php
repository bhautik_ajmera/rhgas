<div class="m-3">
    <!-- @can('cylinder_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success float-right" href="{{ route('admin.cylinders.create') }}">
                    {{ trans('global.add') }} {{ trans('cruds.cylinder.title_singular') }}
                </a>
            </div>
        </div>
    @endcan -->
    
    <div class="card">
        <div class="card-header">
            {{ trans('cruds.cylinder.title_singular') }} {{ trans('global.list') }}
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover datatable datatable-usedForCylinders">
                    <thead>
                        <tr>
                            <th width="10">

                            </th>
                            <th>
                                {{ trans('cruds.cylinder.fields.id') }}
                            </th>
                            <th>
                                {{ trans('cruds.cylinder.fields.supplier') }}
                            </th>
                            <th>
                                {{ trans('cruds.cylinder.fields.branch_cyl_no') }}
                            </th>
                            <th>
                                {{ trans('cruds.cylinder.fields.capacity') }}
                            </th>
                            <th>
                                {{ trans('cruds.cylinder.fields.manufacturing_date') }}
                            </th>
                            <th>
                                {{ trans('cruds.cylinder.fields.last_hydrotest_date') }}
                            </th>
                            <th>
                                {{ trans('cruds.cylinder.fields.gas') }}
                            </th>
                            <th>
                                {{ trans('cruds.cylinder.fields.used_for') }}
                            </th>
                            <th>
                                &nbsp;
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cylinders as $key => $cylinder)
                            <tr data-entry-id="{{ $cylinder->id }}">
                                <td>

                                </td>
                                <td>
                                    {{ $cylinder->id ?? '' }}
                                </td>
                                <td>
                                    {{ $cylinder->supplier->company_name ?? '' }}
                                </td>
                                <td class="text-uppercase">
                                    {{ $cylinder->branch_cyl_no ?? '' }}
                                </td>
                                <td>
                                    {{ $cylinder->capacity ?? '' }}
                                </td>
                                <td>
                                    {{ $cylinder->manufacturing_date ?? '' }}
                                </td>
                                <td>
                                    {{ $cylinder->last_hydrotest_date ?? '' }}
                                </td>
                                <td>
                                    {{ $cylinder->gas->name ?? '' }}
                                </td>
                                <td>
                                    {{ $cylinder->used_for->name ?? '' }}
                                </td>
                                <td>
                                    @can('cylinder_show')
                                        <a class="btn btn-xs btn-primary" href="{{ route('admin.cylinders.show', $cylinder->id) }}">
                                            {{ trans('global.view') }}
                                        </a>
                                    @endcan

                                    @can('cylinder_edit')
                                        <a class="btn btn-xs btn-info" href="{{ route('admin.cylinders.edit', $cylinder->id) }}">
                                            {{ trans('global.edit') }}
                                        </a>
                                    @endcan

                                    @can('cylinder_delete')
                                        <form action="{{ route('admin.cylinders.destroy', $cylinder->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                        </form>
                                    @endcan

                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('cylinder_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.cylinders.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 10,
  });
  let table = $('.datatable-usedForCylinders:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection