@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.ga.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.gas.store") }}" enctype="multipart/form-data" id="gasFrm" class="form-horizontal">
            @csrf
            <div class="form-group row">
                <label class="required col-sm-2 col-form-label">
                    {{ trans('cruds.ga.fields.code') }}
                </label>

                <div class="col-sm-3">
                    <i>{{"Auto Generate"}}</i>
                </div>
            </div>

            <div class="form-group row">
                <label class="required col-sm-2 col-form-label" for="name">{{ trans('cruds.ga.fields.name') }}</label>
                <div class="col-sm-3">
                    <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" autocomplete="off" />
                    @if($errors->has('name'))
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.ga.fields.name_helper') }}</span>
                </div>
            </div>

            <div class="form-group row">
                <label class="required col-sm-2 col-form-label" for="name">{{ trans('cruds.ga.fields.hsn') }}</label>
                <div class="col-sm-3">
                    <input class="form-control allownumericwithoutdecimal {{ $errors->has('hsn_code') ? 'is-invalid' : '' }}" type="text" name="hsn_code" id="hsn_code" value="{{ old('hsn_code', '') }}" maxlength="6" autocomplete="off" />
                    @if($errors->has('hsn_code'))
                        <span class="text-danger">{{ $errors->first('hsn_code') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.ga.fields.hsn_helper') }}</span>
                </div>
            </div>

            <div class="form-group row">
                <label class="required col-sm-2 col-form-label" for="gst">{{'GST'}}</label>
                <div class="col-sm-3">
                    <select class="form-control {{ $errors->has('gst') ? 'is-invalid' : '' }}" name="gst" id="gst">
                        @foreach(config('constant.gasGST') as $key => $value)
                            <option value="{{$key}}">{{$value}}</option>
                        @endforeach
                    </select>

                    @if($errors->has('gst'))
                        <span class="text-danger">{{ $errors->first('gst') }}</span>
                    @endif

                    <div id="gstError"></div>
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.gas.index')}}" class="btn btn-default">
                    {{ trans('global.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/gas/add_edit.js?'.time())}}" ></script>
@endsection