@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.cylinderCompany.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.cylinder-companies.update", [$cylinderCompany->id]) }}" enctype="multipart/form-data" id="cyliComFrm">
            @method('PUT')
            @csrf

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="code">{{ trans('cruds.cylinderCompany.fields.code') }}</label>
                        <input class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}" type="text" name="code" id="code" value="{{ old('code', $cylinderCompany->code) }}" readonly="readonly" autocomplete="off" />
                        @if($errors->has('code'))
                            <span class="text-danger">{{ $errors->first('code') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.cylinderCompany.fields.code_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="name">{{ trans('cruds.cylinderCompany.fields.name') }}</label>
                        <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $cylinderCompany->name) }}" autocomplete="off" />
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.cylinderCompany.fields.name_helper') }}</span>
                    </div>
                </div>
            </div>

            <?php /*
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="mobile_no">{{ trans('cruds.cylinderCompany.fields.mobile_no') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('mobile_no') ? 'is-invalid' : '' }}" type="text" name="mobile_no" id="mobile_no" value="{{ old('mobile_no', $cylinderCompany->mobile_no) }}" maxlength="10" autocomplete="off" />
                        @if($errors->has('mobile_no'))
                            <span class="text-danger">{{ $errors->first('mobile_no') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.cylinderCompany.fields.mobile_no_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="address">{{ trans('cruds.cylinderCompany.fields.address') }}</label>
                        <textarea class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" name="address" id="address">{{ old('address', $cylinderCompany->address) }}</textarea>
                        @if($errors->has('address'))
                            <span class="text-danger">{{ $errors->first('address') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.cylinderCompany.fields.address_helper') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="pin_code">{{ trans('cruds.cylinderCompany.fields.pin_code') }}</label>
                        <input class="form-control allownumericwithoutdecimal {{ $errors->has('pin_code') ? 'is-invalid' : '' }}" type="text" name="pin_code" id="pin_code" value="{{ old('pin_code', $cylinderCompany->pin_code) }}" maxlength="6" autocomplete="off" />
                        @if($errors->has('pin_code'))
                            <span class="text-danger">{{ $errors->first('pin_code') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.cylinderCompany.fields.pin_code_helper') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="state_id">{{ 'State' }}</label>
                        <select class="form-control select2 {{ $errors->has('state_id') ? 'is-invalid' : '' }}" name="state_id" id="state_id" />
                            @foreach($states as $id => $state)
                                <option value="{{ $id }}" {{ (old('state_id') ? old('state_id') : $cylinderCompany->state_id ?? '') == $id ? 'selected' : '' }}>{{ $state }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('state_id'))
                            <span class="text-danger">{{ 'The state field is required.' }}</span>
                        @endif
                        <div id="stateError"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="required" for="city_id">{{ 'City' }}</label>
                        <select class="form-control select2 {{ $errors->has('city_id') ? 'is-invalid' : '' }}" name="city_id" id="city_id" />
                            @foreach($cities as $id => $city)
                                <option value="{{ $id }}" {{ (old('city_id') ? old('city_id') : $cylinderCompany->city_id ?? '') == $id ? 'selected' : '' }}>{{ $city }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('city_id'))
                            <span class="text-danger">{{ 'The city field is required.' }}</span>
                        @endif
                        <div id="cityError"></div>
                    </div>
                </div>
            </div>          
            */ ?>

            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.cylinder-companies.index')}}" class="btn btn-default">
                    {{ trans('global.cancel') }}
                </a>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/cylinder_company/add_edit.js?'.time())}}" ></script>
@endsection
