@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.cylinderCompany.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-info" href="{{ route('admin.cylinder-companies.edit', $cylinderCompany->id) }}">
                    {{ trans('global.back_to_edit') }}
                </a>

                <a class="btn btn-default" href="{{ route('admin.cylinder-companies.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.cylinderCompany.fields.id') }}
                        </th>
                        <td>
                            {{ $cylinderCompany->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.cylinderCompany.fields.code') }}
                        </th>
                        <td>
                            {{ $cylinderCompany->code }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.cylinderCompany.fields.name') }}
                        </th>
                        <td>
                            {{ $cylinderCompany->name }}
                        </td>
                    </tr>

                    <?php /*
                    <tr>
                        <th>
                            {{ trans('cruds.cylinderCompany.fields.mobile_no') }}
                        </th>
                        <td>
                            {{ $cylinderCompany->mobile_no }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.cylinderCompany.fields.address') }}
                        </th>
                        <td>
                            {{ $cylinderCompany->address }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.cylinderCompany.fields.pin_code') }}
                        </th>
                        <td>
                            {{ !is_null($cylinderCompany->pin_code)?$cylinderCompany->pin_code:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ 'State' }}
                        </th>
                        <td>
                            {{ ($state)?$state->name:'-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ 'City' }}
                        </th>
                        <td>
                            {{ ($city)?$city->name:'-' }}
                        </td>
                    </tr>
                    */ ?>
                    
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-info" href="{{ route('admin.cylinder-companies.edit', $cylinderCompany->id) }}">
                    {{ trans('global.back_to_edit') }}
                </a>
                
                <a class="btn btn-default" href="{{ route('admin.cylinder-companies.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection