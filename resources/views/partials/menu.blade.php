<aside class="main-sidebar sidebar-dark-primary elevation-4" style="min-height: 917px;">
    <a href="{{ route("admin.home") }}" class="brand-link">
        <img src="{{asset('images/logo.jpeg')}}" class="rounded-circle float-left" alt="{{ trans('panel.site_title') }}" height="30px" />
        <span class="brand-text font-weight-light ml-1">{{ trans('panel.site_title') }}</span>
    </a>

    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route("admin.home") }}">
                        <i class="fas fa-fw fa-tachometer-alt nav-icon">
                        </i>
                        <p>
                            {{ trans('global.dashboard') }}
                        </p>
                    </a>
                </li>

                @can('user_management_access')
                    <li class="nav-item has-treeview {{ request()->is("admin/permissions*") ? "menu-open" : "" }} {{ request()->is("admin/roles*") ? "menu-open" : "" }} {{ request()->is("admin/users*") ? "menu-open" : "" }}">
                        <a class="nav-link nav-dropdown-toggle" href="javascript:void(0);">
                            <i class="fa-fw nav-icon fas fa-users">

                            </i>
                            <p>
                                {{ trans('cruds.userManagement.title') }}
                                <i class="right fa fa-fw fa-angle-left nav-icon"></i>
                            </p>
                        </a>

                        <ul class="nav nav-treeview" style="margin-left:10px;">
                            @can('permission_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is("admin/permissions") || request()->is("admin/permissions/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon fas fa-unlock-alt">

                                        </i>
                                        <p>
                                            {{ trans('cruds.permission.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan

                            @can('role_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is("admin/roles") || request()->is("admin/roles/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon fas fa-briefcase">

                                        </i>
                                        <p>
                                            {{ trans('cruds.role.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            
                            @can('user_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is("admin/users") || request()->is("admin/users/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon fas fa-user">

                                        </i>
                                        <p>
                                            {{ trans('cruds.user.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan

                @can('configuration_access')
                    <li class="nav-item has-treeview {{ request()->is("admin/states*") ? "menu-open" : "" }} {{ request()->is("admin/cities*") ? "menu-open" : "" }} {{ request()->is("admin/branches*") ? "menu-open" : "" }}">
                        <a class="nav-link nav-dropdown-toggle" href="javascript:void(0);">
                            <i class="fa-fw nav-icon fas fa-cogs">

                            </i>
                            <p>
                                {{ trans('cruds.configuration.title') }}
                                <i class="right fa fa-fw fa-angle-left nav-icon"></i>
                            </p>
                        </a>

                        <ul class="nav nav-treeview" style="margin-left:10px;">
                            @can('state_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.states.index") }}" class="nav-link {{ request()->is("admin/states") || request()->is("admin/states/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon fas fa-adjust">

                                        </i>
                                        <p>
                                            {{ trans('cruds.state.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan

                            @can('city_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.cities.index") }}" class="nav-link {{ request()->is("admin/cities") || request()->is("admin/cities/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon far fa-circle">

                                        </i>
                                        <p>
                                            {{ trans('cruds.city.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan

                            @can('branch_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.branches.index") }}" class="nav-link {{ request()->is("admin/branches") || request()->is("admin/branches/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon fas fa-bezier-curve">

                                        </i>
                                        <p>
                                            {{ trans('cruds.branch.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan

                @can('customer_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.customers.index") }}" class="nav-link {{ request()->is("admin/customers") || request()->is("admin/customers/*") ? "active" : "" }}">
                            <i class="fa-fw nav-icon fas fa-child">

                            </i>
                            <p>
                                {{ trans('cruds.customer.title') }}
                            </p>
                        </a>
                    </li>
                @endcan

                @can('supplier_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.suppliers.index") }}" class="nav-link {{ request()->is("admin/suppliers") || request()->is("admin/suppliers/*") ? "active" : "" }}">
                            <i class="fa-fw nav-icon fas fa-diagnoses">

                            </i>
                            <p>
                                {{ trans('cruds.supplier.title') }}
                            </p>
                        </a>
                    </li>
                @endcan

                @can('ga_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.gas.index") }}" class="nav-link {{ request()->is("admin/gas") || request()->is("admin/gas/*") ? "active" : "" }}">
                            <i class="fa-fw nav-icon fab fa-hotjar">

                            </i>
                            <p>
                                {{ trans('cruds.ga.title') }}
                            </p>
                        </a>
                    </li>
                @endcan

                @can('cylinder_management_access')
                    <li class="nav-item has-treeview {{ request()->is("admin/cylinder-companies*") ? "menu-open" : "" }} {{ request()->is("admin/cylinders*") ? "menu-open" : "" }} {{ request()->is("admin/cylinder-issue*") ? "menu-open" : "" }} {{ request()->is("admin/cylinder/refilling") ? "menu-open" : "" }} {{ request()->is("admin/cylinder/refilling/*") ? "menu-open" : "" }} {{ request()->is("admin/cyl/refilling/received") ? "menu-open" : "" }} {{ request()->is("admin/cyl-issue/received") ? "menu-open" : "" }}">
                        <a class="nav-link nav-dropdown-toggle" href="javascript:void(0);">
                            <i class="fa-fw nav-icon fas fa-burn"></i>
                            <p>
                                {{'Cylinder Management'}}
                                <i class="right fa fa-fw fa-angle-left nav-icon"></i>
                            </p>
                        </a>

                        <ul class="nav nav-treeview" style="margin-left:10px;">
                            @can('cylinder_company_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.cylinder-companies.index") }}" class="nav-link {{ request()->is("admin/cylinder-companies") || request()->is("admin/cylinder-companies/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon fas fa-assistive-listening-systems">

                                        </i>
                                        <p>
                                            {{ trans('cruds.cylinderCompany.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            
                            @can('cylinder_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.cylinders.index") }}" class="nav-link {{ request()->is("admin/cylinders") || request()->is("admin/cylinders/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon fas fa-band-aid">

                                        </i>
                                        <p>
                                            {{ trans('cruds.cylinder.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan

                            @can('refilling_access')
                            <li class="nav-item">
                                <a href="{{route('admin.cylinder.refilling')}}" class="nav-link {{ request()->is("admin/cylinder/refilling") ? "active" : "" }} {{ request()->is("admin/cylinder/refilling/*") ? "active" : "" }}">
                                    <i class="fa-fw nav-icon fas fa-battery-empty">
                                        
                                    </i>
                                    <p>
                                        {{'Refilling '}}
                                    </p>
                                </a>
                            </li>
                            @endcan

                            @can('refilling_received_access')
                            <li class="nav-item">
                                <a href="{{route('admin.cyl.refilling.received')}}" class="nav-link {{ request()->is("admin/cyl/refilling/received") ? "active" : "" }}">
                                    <i class="fa-fw nav-icon fas fa-battery-full">
                                        
                                    </i>
                                    <p>
                                        {{'Refilling Received'}}
                                    </p>
                                </a>
                            </li>
                            @endcan

                            @can('cylinder_issue_access')
                            <li class="nav-item">
                                <a href="{{ route("admin.cylinder-issue.index") }}" class="nav-link {{ request()->is("admin/cylinder-issue") || request()->is("admin/cylinder-issue/*") ? "active" : "" }}">
                                    <i class="fa-fw nav-icon far fa-clipboard">

                                    </i>
                                    <p>
                                        {{'Cylinder Issue'}}
                                    </p>
                                </a>
                            </li>
                            @endcan

                            @can('cylinder_issue_received_access')
                            <li class="nav-item">
                                <a href="{{route('admin.cylinderIssue.received')}}" class="nav-link {{ request()->is("admin/cyl-issue/received") ? "active" : "" }}">
                                    <i class="fa-fw nav-icon fa fa-address-card"></i>
                                    <p>
                                        {{'Cylinder Issue Received'}}
                                    </p>
                                </a>
                            </li>
                            @endcan
                        </ul>
                    </li>
                @endcan

                @can('income_expense_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.income-expenses.index") }}" class="nav-link {{ request()->is("admin/income-expenses") || request()->is("admin/income-expenses/*") ? "active" : "" }}">
                            <i class="fa-fw nav-icon far fa-money-bill-alt">

                            </i>
                            <p>
                                {{ trans('cruds.incomeExpense.title') }}
                            </p>
                        </a>
                    </li>
                @endcan

                @can('purchase_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.purchases.index") }}" class="nav-link {{ request()->is("admin/purchases") || request()->is("admin/purchases/*") ? "active" : "" }}">
                            <i class="fa-fw nav-icon fas fa-money-check">

                            </i>
                            <p>
                                {{ trans('cruds.purchase.title') }}
                            </p>
                        </a>
                    </li>
                @endcan

                @can('report_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/report/*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="javascript:void(0);">
                            <i class="fa-fw nav-icon fas fa-burn"></i>
                            <p>
                                {{'Report'}}
                                <i class="right fa fa-fw fa-angle-left nav-icon"></i>
                            </p>
                        </a>

                        <ul class="nav nav-treeview" style="margin-left:10px;">
                            @can('income_expense_report_access')
                                <li class="nav-item">
                                    <a href="{{ route('admin.report.income-expense') }}" class="nav-link {{ request()->is('admin/report/income-expense') ? 'active' : ''}}">
                                        <i class="fa-fw nav-icon fas fa-assistive-listening-systems"></i>
                                        <p>
                                            {{'Income Expense'}}
                                        </p>
                                    </a>
                                </li>
                            @endcan

                            @can('cylinder_report_access')
                                <li class="nav-item">
                                    <a href="{{ route('admin.report.cylinder') }}" class="nav-link {{ request()->is('admin/report/cylinder') ? 'active' : ''}}">
                                        <i class="fa-fw nav-icon fas fa-band-aid"></i>
                                        <p>
                                            {{'Cylinder'}}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan

                @can('invoice_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.invoice") }}" class="nav-link {{ request()->is("admin/invoice") ? "active" : "" }} {{ request()->is("admin/invoice/create") ? "active" : "" }}">
                            <i class="fa-fw nav-icon fas fa-file-invoice"></i>
                            <p>
                                {{'Invoice'}}
                            </p>
                        </a>
                    </li>
                @endcan
            </ul>
        </nav>
    </div>
</aside>