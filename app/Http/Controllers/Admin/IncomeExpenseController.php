<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyIncomeExpenseRequest;
use App\Http\Requests\StoreIncomeExpenseRequest;
use App\Http\Requests\UpdateIncomeExpenseRequest;
use App\Models\Branch;
use App\Models\IncomeExpense;
use App\Models\IncomeDetail;
use App\Models\ExpenseDetail;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use DB;
use Validator;

class IncomeExpenseController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('income_expense_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $incomeExpenses = IncomeExpense::with(['branch'])->get();

        return view('admin.incomeExpenses.index', compact('incomeExpenses'));
    }

    public function create()
    {
        abort_if(Gate::denies('income_expense_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $branches = Branch::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.incomeExpenses.create', compact('branches'));
    }

    public function store(StoreIncomeExpenseRequest $request)
    {
        DB::beginTransaction();
        try{
            $incomeExpense = IncomeExpense::create($request->all());

            $incDesc = $request->input('incDesc');
            $incRate = $request->input('incRate');
            foreach($incRate as $key => $value){
                $inObj = new IncomeDetail();
                $inObj->income_expense_id = $incomeExpense->id;
                $inObj->desc = (isset($incDesc[$key]) && !empty($incDesc[$key]))?$incDesc[$key]:null;
                $inObj->rate = $value;
                $inObj->save();
            }

            $expDesc = $request->input('expDesc');
            $expRate = $request->input('expRate');
            foreach($expRate as $key => $value){
                $exObj = new ExpenseDetail();
                $exObj->income_expense_id = $incomeExpense->id;
                $exObj->desc = (isset($expDesc[$key]) && !empty($expDesc[$key]))?$expDesc[$key]:null;
                $exObj->rate = $value;
                $exObj->save();
            }

            DB::commit();
            return redirect()->route('admin.income-expenses.index')->with('success','Income Expense Added Successfully!');
        }catch(\Exception $e) {
            DB::rollback();
            return redirect()->route('admin.income-expenses.index')->with('error','Opps! Something went wrong,Please try again...');
        }
    }

    public function edit(IncomeExpense $incomeExpense)
    {
        abort_if(Gate::denies('income_expense_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $branches = Branch::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $incomeExpense->load('branch');

        $inDetail = IncomeDetail::where('income_expense_id',$incomeExpense->id)->get();
        $exDetail = ExpenseDetail::where('income_expense_id',$incomeExpense->id)->get();

        return view('admin.incomeExpenses.edit', compact('branches', 'incomeExpense', 'inDetail', 'exDetail'));
    }

    public function update(UpdateIncomeExpenseRequest $request, IncomeExpense $incomeExpense)
    {
        DB::beginTransaction();
        try{
            $incomeExpense->update($request->all());

            $incDesc = $request->input('incDesc');
            $incRate = $request->input('incRate');
            IncomeDetail::where('income_expense_id',$incomeExpense->id)->forceDelete();
            foreach($incRate as $key => $value){
                $inObj = new IncomeDetail();
                $inObj->income_expense_id = $incomeExpense->id;
                $inObj->desc = (isset($incDesc[$key]) && !empty($incDesc[$key]))?$incDesc[$key]:null;
                $inObj->rate = $value;
                $inObj->save();
            }

            $expDesc = $request->input('expDesc');
            $expRate = $request->input('expRate');
            ExpenseDetail::where('income_expense_id',$incomeExpense->id)->forceDelete();
            foreach($expRate as $key => $value){
                $exObj = new ExpenseDetail();
                $exObj->income_expense_id = $incomeExpense->id;
                $exObj->desc = (isset($expDesc[$key]) && !empty($expDesc[$key]))?$expDesc[$key]:null;
                $exObj->rate = $value;
                $exObj->save();
            }

            DB::commit();
            return redirect()->route('admin.income-expenses.index')->with('success','Income Expense Updated Successfully!');
        }catch(\Exception $e) {
            DB::rollback();
            return redirect()->route('admin.income-expenses.index')->with('error','Opps! Something went wrong,Please try again...');
        }
    }

    public function show(IncomeExpense $incomeExpense)
    {
        abort_if(Gate::denies('income_expense_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $incomeExpense->load('branch');

        $branches = Branch::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        $inDetail = IncomeDetail::where('income_expense_id',$incomeExpense->id)->get();
        $exDetail = ExpenseDetail::where('income_expense_id',$incomeExpense->id)->get();

        return view('admin.incomeExpenses.show', compact('incomeExpense','inDetail','exDetail','branches'));
    }

    public function destroy(IncomeExpense $incomeExpense)
    {
        abort_if(Gate::denies('income_expense_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $incomeExpense->delete();
        IncomeDetail::where('income_expense_id',$incomeExpense->id)->delete();
        ExpenseDetail::where('income_expense_id',$incomeExpense->id)->delete();

        return back()->with('success','Income Expense Deleted Successfully!');
    }

    public function massDestroy(MassDestroyIncomeExpenseRequest $request)
    {
        IncomeExpense::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function addMore($type,Request $request){
        $count  = $request->input('count');
        $result = view('admin.incomeExpenses.addmore.'.$type)
                ->with('count',$count)
                ->render();

        return \Response::json(array(
            'result' => $result
        ));
    }

    public function deleteIncExp($type,Request $request){
        $id = $request->input('id');
        ($type == 'income')?IncomeDetail::find($id)->delete():ExpenseDetail::find($id)->delete();
                
        return \Response::json(array(
            'status' => 'success'
        ));
    }

    public function report(Request $request){
        $branches = Branch::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        if($request->isMethod('post')){
            $rules = array(
                'startEndDate' => 'required',
                'branch_id'    => 'required'
            );

            $validator = Validator::make($request->all(),$rules);

            if($validator->fails()){
                $errors = $validator->messages();
                return redirect()->back()->withErrors($errors)->withInput();
            }

            $selDate      = $request->input('startEndDate');
            $selBranchId  = $request->input('branch_id');

            $startEndDate = explode("-",$request->input('startEndDate'));
            $startDate    = date('Y-m-d',strtotime(str_replace("/","-",$startEndDate[0])));
            $endDate      = date('Y-m-d',strtotime(str_replace("/","-",$startEndDate[1])));

            $incomeExp = IncomeExpense::whereDate('date','>=',$startDate)
                        ->whereDate('date','<=',$endDate)
                        ->where('branch_id',$selBranchId)
                        ->orderBy('date','desc')
                        ->pluck('id')
                        ->toArray();

            $incomeDetail  = IncomeDetail::whereIn('income_expense_id',$incomeExp)->get();
            $expenseDetail = ExpenseDetail::whereIn('income_expense_id',$incomeExp)->get();

            return view('admin.report.income_expense.index',compact('incomeDetail','expenseDetail','selDate','branches','selBranchId'));
        }else{
            return view('admin.report.income_expense.index',compact('branches'));
        }
    }
}
