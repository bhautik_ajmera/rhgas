<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Refilling;
use App\Models\RefillingDetail;
use App\Models\Cylinder;
use App\Models\Supplier;
use App\Models\SupAssignGas;
use Validator;
use Redirect;
use Response;
use DB;

class RefillingReceivedController extends Controller
{
    public function index(Request $request){
    	$supplier = Refilling::leftjoin('suppliers','refilling.supplier_id','=','suppliers.id')
    			->pluck('suppliers.company_name','suppliers.id')
    			->toArray();

    	return view('admin.refilling_received.index',compact('supplier'));    	
    }

    public function getSupChal(Request $request){
    	$sid = $request->input('sid');
    	
    	$refilling = Refilling::where('supplier_id',$sid)
                    ->orderBy('id','desc')
		    		->pluck('sys_chal_no','id')
		    		->toArray();

    	$result = view('admin.refilling_received.challan_no',compact('refilling'))->render();

        return Response::json(array(
            'result' => $result
        ));
    }

    public function refillingList(Request $request){
        $status    = ['empty','refilling','in-stock','issued'];
        $supId     = $request->input('sid');
        $sysChalNo = $request->input('chNo');

        $refilling = Refilling::where('supplier_id',$supId)
                    ->where('sys_chal_no',$sysChalNo)
                    ->first();

        // $refDetail = RefillingDetail::where('refilling_id',$refilling->id)->get();
        $refDetail = RefillingDetail::leftjoin('cylinders','refilling_detail.cyl_id','cylinders.id')
        ->where('refilling_detail.refilling_id',$refilling->id)
        ->get(['refilling_detail.*','cylinders.dis_cyl']);

        // $gases    = Cylinder::supplierGas($refilling->supplier_id,$status);
        $gases    = SupAssignGas::supplierGas($refilling->supplier_id);
        $cylinder = Cylinder::whereIn('status',$status)
                    // ->where('supplier_id',$refilling->supplier_id)
                    ->get(['id','branch_cyl_no','used_for_id']);

        $result = view('admin.refilling_received.list',compact('refilling','refDetail','gases',
                'cylinder'))->render();

        return Response::json(array(
            'result' => $result
        ));
    }

    public function changeStatus(Request $request){
        DB::beginTransaction();

        try{
            $rdid     = $request->input('rdid');
            $received = $request->input('received');

            // 
            $detail         = RefillingDetail::find($rdid);
            $detail->status = $received;
            $detail->save();

            // 
            RefillingDetail::refillingStatus($detail->refilling_id);

            // 
            $cylinder = Cylinder::find($detail->cyl_id);
            if($cylinder){
                $cylinder->status = ($detail->status)?'in-stock':'refilling';
                $cylinder->save();
            }
            
            DB::commit();
            return Response::json(array(
                'status'  => 'success',
                'message' => 'Cylinder Received Successfully.'
            ));
        }catch(\Exception $e) {
            DB::rollback();
            return Response::json(array(
                'status'  => 'error',
                'message' => 'Opps! Something went wrong,Please try again...'
            ));
        }
    }
}
