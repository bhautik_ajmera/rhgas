<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroySupplierRequest;
use App\Http\Requests\StoreSupplierRequest;
use App\Http\Requests\UpdateSupplierRequest;
use App\Models\Supplier;
use App\Models\State;
use App\Models\City;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Ga;
use App\Models\SupAssignGas;
use Validator;
use Redirect;

class SupplierController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('supplier_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $suppliers = Supplier::all();

        return view('admin.suppliers.index', compact('suppliers'));
    }

    public function create()
    {
        abort_if(Gate::denies('supplier_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $states   = State::all()->pluck('name','id')->prepend(trans('global.pleaseSelect'), '');
        $cities   = City::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        $gases    = Ga::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.suppliers.create',compact('states','cities','gases'));
    }

    public function store(StoreSupplierRequest $request)
    {
        $supplier = Supplier::create($request->all());

        $code = Supplier::setSupCode($supplier->id);
        $supplier->code = $code;
        $supplier->save();

        return redirect()->route('admin.suppliers.index')->with('success','Supplier Added Successfully!');
    }

    public function edit(Supplier $supplier)
    {
        abort_if(Gate::denies('supplier_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $states   = State::all()->pluck('name','id')->prepend(trans('global.pleaseSelect'), '');
        $cities   = City::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        $gases    = Ga::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.suppliers.edit', compact('supplier','states','cities','gases'));
    }

    public function update(UpdateSupplierRequest $request, Supplier $supplier)
    {
        $supplier->update($request->all());

        return redirect()->route('admin.suppliers.index')->with('success','Supplier Updated Successfully!');
    }

    public function show(Supplier $supplier)
    {
        abort_if(Gate::denies('supplier_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // $supplier->load('supplierCylinders');

        $gas = Ga::find($supplier->gas_id);

        $assignGas = SupAssignGas::leftjoin('gas','sup_assign_gas.gas_id', '=', 'gas.id')
              ->where('supplier_id',$supplier->id)
              ->orderBy('sup_assign_gas.id','desc')
              ->get(["sup_assign_gas.*","gas.name as gname"]);

        return view('admin.suppliers.show', compact('supplier','gas','assignGas'));
    }

    public function destroy(Supplier $supplier)
    {
        abort_if(Gate::denies('supplier_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $supplier->delete();

        return back()->with('success','Supplier Deleted Successfully!');
    }

    public function massDestroy(MassDestroySupplierRequest $request)
    {
        Supplier::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function assignGas($supId){
        $gases = Ga::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        
        return view('admin.suppliers.gas.create')->with('supId',$supId)->with('gases',$gases);
    }

    public function editAssignGas($assignGID){
        $assignGas = SupAssignGas::find($assignGID);
        $gases    = Ga::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.suppliers.gas.edit',compact('assignGas','gases'));
    }

    public function saveAssignGas(Request $request){
        $rules = array(
            'gas_id'     => 'required',
            'rate'       => 'required|regex:/^\d+(\.\d{1,2})?$/'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            $errors = $validator->messages();
            return Redirect::back()->withErrors($errors);
        }

        $supId = $request->input('supId');

        $obj = ($request->has('assignGasId'))?SupAssignGas::find($request->input('assignGasId')):new SupAssignGas();
        $obj->supplier_id = $supId;
        $obj->gas_id      = $request->input('gas_id');
        $obj->rate        = $request->input('rate');
        $obj->save();

        return Redirect::route('admin.suppliers.show',$supId);
    }

    public function deleteAssignGas($assignGID){
        $assignGas = SupAssignGas::find($assignGID);
        $supId     = $assignGas->supplier_id;

        $assignGas->delete();

        return Redirect::route('admin.suppliers.show',$supId);
    }
}
