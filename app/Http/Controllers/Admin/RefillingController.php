<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Redirect;
use Response;
use App\Models\Supplier;
use App\Models\Refilling;
use App\Models\Cylinder;
use App\Models\SupAssignGas;
use App\Models\RefillingDetail;
use DB;
use App\Http\Requests\MassDestroyRefillingRequest;

class RefillingController extends Controller
{
    public function index(){
        $refilling = Refilling::leftjoin('suppliers','refilling.supplier_id','=','suppliers.id')
                    ->get(['refilling.*','suppliers.company_name as sname']);

    	return view('admin.refilling.index',compact('refilling'));
    }

    public function create(){
        $supplieres = Supplier::all()->pluck('company_name', 'id')
        			->prepend(trans('global.pleaseSelect'), '');

    	return view('admin.refilling.create',compact('supplieres'));
    }

    public function getSupDetail(Request $request){
		$supId     = $request->input('supId');
		$supdetail = Supplier::find($supId);

        $result = view('admin.refilling.view_sup_detail',compact('supdetail'))->render();

        return Response::json(array(
            'result' => $result
        ));
    }

    public function store(Request $request){
        DB::beginTransaction();
        
        try{
            $rules = array(
                'supplier_id' => 'required',
                'date'        => 'required',
                'gas.*'       => 'required',
                'cylNo.*'     => 'required'
            );

            $validator = Validator::make($request->all(),$rules);

            if($validator->fails()){
                $errors = $validator->messages();
                return redirect()->back()->withErrors($errors)->withInput();
            }

            $obj              = new Refilling();
            $obj->supplier_id = $request->input('supplier_id');
            $obj->date        = date('Y-m-d',strtotime($request->input('date')));
            $obj->vehicle_no  = $request->input('vehicle_no');
            $obj->remark      = $request->input('remark');
            $obj->total       = $request->input('total');
            $obj->save();

            $sysChNo = Refilling::setSysChNo($obj->id);
            $obj->sys_chal_no = $sysChNo;
            $obj->save();

            // 
            $gas    = $request->input('gas');
            $cylNo  = $request->input('cylNo');
            $rate   = $request->input('rate');
            $status = $request->input('status');
            $count  = count($cylNo);

            for($i=0;$i<$count;$i++){
                $detail               = new RefillingDetail();
                $detail->refilling_id = $obj->id;
                $detail->gas_id       = $gas[$i];
                $detail->cyl_id       = $cylNo[$i];
                $detail->rate         = $rate[$i];
                $detail->status       = $status[$i];
                $detail->save();

                $cylinder = Cylinder::find($detail->cyl_id);
                if($cylinder){
                    $cylinder->status = 'refilling';
                    $cylinder->save();
                }
            }

            DB::commit();
            return redirect()->route('admin.cylinder.refilling')->with('success','Refilling Added Successfully!');
        }catch(\Exception $e) {
            DB::rollback();
            return redirect()->route('admin.cylinder.refilling')->with('error','Opps! Something went wrong,Please try again...');
        }
    }

    public function edit($id){
        $supplieres = Supplier::all()->pluck('company_name', 'id')
                    ->prepend(trans('global.pleaseSelect'), '');

        $refilling = Refilling::find($id);
        // $refDetail = RefillingDetail::where('refilling_id',$id)->get();
        $refDetail = RefillingDetail::leftjoin('cylinders','refilling_detail.cyl_id','cylinders.id')
                    ->where('refilling_detail.refilling_id',$id)
                    ->get(['refilling_detail.*','cylinders.status as cstatus']);

        $status   = ['empty','refilling','in-stock','issued'];
        // $gases    = Cylinder::supplierGas($refilling->supplier_id,$status);
        $gases = SupAssignGas::supplierGas($refilling->supplier_id);
        $cylinder = Cylinder::whereIn('status',$status)
                    // ->where('supplier_id',$refilling->supplier_id)
                    ->get(['id','branch_cyl_no','used_for_id']);

        return view('admin.refilling.edit',compact('supplieres','refilling','refDetail','gases','cylinder'));
    }

    public function update(Request $request){
        DB::beginTransaction();

        try{
            $rules = array(
                'supplier_id' => 'required',
                'date'        => 'required',
                'gas.*'       => 'required',
                'cylNo.*'     => 'required'
            );

            $validator = Validator::make($request->all(),$rules);

            if($validator->fails()){
                $errors = $validator->messages();
                return redirect()->back()->withErrors($errors)->withInput();
            }

            $refillingId = $request->input('refillingId');

            $obj              = Refilling::find($refillingId);
            $obj->supplier_id = $request->input('supplier_id');
            $obj->date        = date('Y-m-d',strtotime($request->input('date')));
            $obj->vehicle_no  = $request->input('vehicle_no');
            $obj->remark      = $request->input('remark');
            $obj->total       = $request->input('total');
            $obj->save();

            //
            RefillingDetail::where('refilling_id',$refillingId)->forceDelete(); 
            $gas    = $request->input('gas');
            $cylNo  = $request->input('cylNo');
            $rate   = $request->input('rate');
            $status = $request->input('status');
            $count  = count($cylNo);

            for($i=0;$i<$count;$i++){
                $detail               = new RefillingDetail();
                $detail->refilling_id = $refillingId;
                $detail->gas_id       = $gas[$i];
                $detail->cyl_id       = $cylNo[$i];
                $detail->rate         = $rate[$i];
                $detail->status       = $status[$i];
                $detail->save();

                $cylinder = Cylinder::find($detail->cyl_id);
                if($cylinder){
                    $cylinder->status = ($detail->status)?'in-stock':'refilling';
                    $cylinder->save();
                }
            }

            //
            RefillingDetail::refillingStatus($refillingId);
            
            DB::commit();
            return redirect()->route('admin.cylinder.refilling')->with('success','Refilling Updated Successfully!');
        }catch(\Exception $e) {
            DB::rollback();
            return redirect()->route('admin.cylinder.refilling')->with('error','Opps! Something went wrong,Please try again...');
        }
    }

    public function show($id){
        $supplieres = Supplier::all()->pluck('company_name', 'id')
                    ->prepend(trans('global.pleaseSelect'), '');

        $refilling = Refilling::find($id);
        $refDetail = RefillingDetail::where('refilling_id',$id)->get();

        $status   = ['empty','refilling','in-stock','issued'];
        // $gases    = Cylinder::supplierGas($refilling->supplier_id,$status);
        $gases    = SupAssignGas::supplierGas($refilling->supplier_id);
        $cylinder = Cylinder::whereIn('status',$status)
                    // ->where('supplier_id',$refilling->supplier_id)
                    ->get(['id','branch_cyl_no','used_for_id']);

        return view('admin.refilling.show',compact('supplieres','refilling','refDetail','gases','cylinder'));        
    }

    public function addMore(Request $request){
        $count  = $request->input('count');
        $result = view('admin.refilling.add_more',compact('count'))->render();

        return Response::json(array(
            'result' => $result
        ));
    }

    public function getSupplierGas(Request $request){
        $sid   = $request->input('sid');
        $ftype = $request->input('ftype');

        $gases = SupAssignGas::supplierGas($sid);

        // $gases = Cylinder::supplierGas($sid,($ftype == 'create')?['empty']:['empty','refilling','in-stock']);

        return Response::json(array(
            'gases' => $gases
        ));
    }

    public function getGasCylinder(Request $request){
        $gid   = $request->input('gid');
        $sid   = $request->input('sid');
        $ftype = $request->input('ftype');
        $rate  = '0.00';

        $cylinder = Cylinder::leftjoin('gas','cylinders.used_for_id','gas.id')
                // ->where('cylinders.supplier_id',$sid)
                ->where('cylinders.dis_cyl',0)
                ->where('cylinders.used_for_id',$gid)
                ->whereIn('cylinders.status',($ftype == 'create')?['empty']:['empty','refilling','in-stock'])
                ->whereNull('gas.deleted_at')
                ->get(['cylinders.branch_cyl_no','cylinders.id']);

        
        if(!empty($gid)){
            $supAssignGas = SupAssignGas::where('supplier_id',$sid)
                    ->where('gas_id',$gid)
                    ->first();
            
            $rate = ($supAssignGas)?sprintf('%.2f',$supAssignGas->rate):'0.00';
        }

        return Response::json(array(
            'cylinder' => $cylinder,
            'rate'     => $rate
        ));
    }

    public function deleteRefillingDetail(Request $request){
        DB::beginTransaction();
        
        try{
            $id          = $request->input('id');
            $result      = RefillingDetail::find($id);
            $refillingId = $result->refilling_id;
            $cylId       = $result->cyl_id;

            $result->delete();
            RefillingDetail::refillingStatus($refillingId);
            Cylinder::cylinderStatus([$cylId],'empty');

            DB::commit();
            return Response::json(array(
                'status'  => 'success',
                'message' => 'Deleted Successfully'
            ));
        }catch(\Exception $e) {
            DB::rollback();
            return Response::json(array(
                'status'  => 'error',
                'message' => 'Opps! Something went wrong,Please try again...'
            ));
        }
    }

    public function delete($id){
        DB::beginTransaction();

        try{
            Refilling::find($id)->delete();
            
            $cylIds = RefillingDetail::where('refilling_id',$id)
                    ->pluck('cyl_id')
                    ->toArray();
            Cylinder::cylinderStatus($cylIds,'empty');
            
            RefillingDetail::where('refilling_id',$id)->delete();
            DB::commit();
            return back()->with('success','Refilling Deleted Successfully!');
        }catch(\Exception $e) {
            DB::rollback();
            return back()->with('error','Opps! Something went wrong,Please try again...');
        }
    }

    public function massDestroy(MassDestroyRefillingRequest $request){
        DB::beginTransaction();

        try{
            Refilling::whereIn('id', request('ids'))->delete();

            $cylIds = RefillingDetail::whereIn('refilling_id',request('ids'))
                    ->pluck('cyl_id')
                    ->toArray();
            Cylinder::cylinderStatus($cylIds,'empty');

            RefillingDetail::whereIn('refilling_id',request('ids'))->delete();
            DB::commit();
            return back()->with('success','Refilling Deleted Successfully!');
        }catch(\Exception $e) {
            DB::rollback();
            return back()->with('error','Opps! Something went wrong,Please try again...');
        }
    }
}
