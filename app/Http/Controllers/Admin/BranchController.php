<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyBranchRequest;
use App\Http\Requests\StoreBranchRequest;
use App\Http\Requests\UpdateBranchRequest;
use App\Models\Branch;
use App\Models\City;
use App\Models\State;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BranchController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('branch_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $branches = Branch::with(['city'])->get();

        return view('admin.branches.index', compact('branches'));
    }

    public function create()
    {
        abort_if(Gate::denies('branch_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $states = State::all()->pluck('name','id')->prepend(trans('global.pleaseSelect'), '');
        $cities = City::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.branches.create', compact('states','cities'));
    }

    public function store(StoreBranchRequest $request)
    {
        $branch = Branch::create($request->all());

        $code = Branch::setBranchCode($branch->id,$branch->name);
        $branch->code = $code;
        $branch->save();

        return redirect()->route('admin.branches.index')->with('success','Branch Added Successfully!');
    }

    public function edit(Branch $branch)
    {
        abort_if(Gate::denies('branch_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cities = City::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        $states = State::all()->pluck('name','id')->prepend(trans('global.pleaseSelect'), '');

        $branch->load('city');

        return view('admin.branches.edit', compact('cities', 'branch', 'states'));
    }

    public function update(UpdateBranchRequest $request, Branch $branch)
    {
        $branch->update($request->all());
        
        return redirect()->route('admin.branches.index')->with('success','Branch Updated Successfully!');
    }

    public function show(Branch $branch)
    {
        abort_if(Gate::denies('branch_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $branch->load('city', 'branchCustomers');
        $state = State::find($branch->state_id);

        return view('admin.branches.show', compact('branch','state'));
    }

    public function destroy(Branch $branch)
    {
        abort_if(Gate::denies('branch_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $branch->delete();

        return back()->with('success','Branch Deleted Successfully!');
    }

    public function massDestroy(MassDestroyBranchRequest $request)
    {
        Branch::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    // public function getStateCities(Request $request){
    //     $stateId = $request->input('stateId');

    //     $cities = City::where('state_id',$stateId)->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        
    //     $result = view('admin.branches.cities')->with('cities',$cities)->render();

    //     return \Response::json(array(
    //         'status' => 'success',
    //         'result' => $result
    //     ));
    // }
}
