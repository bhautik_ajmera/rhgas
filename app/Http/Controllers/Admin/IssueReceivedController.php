<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Branch;
use App\Models\CylinderIssue;
use App\Models\CylinderIssueDetail;
use App\Models\Ga;
use App\Models\Cylinder;
use Response;
use DB;

class IssueReceivedController extends Controller
{
    public function index(){
        $branches = Branch::all()->pluck('name','id');

    	return view('admin.issue_received.index',compact('branches'));
    }

    public function getChallan(Request $request){
    	$bid = $request->input('bid');
    	$cid = $request->input('cid');

    	$cissues = CylinderIssue::where('branch_id',$bid)
			    	->where('cus_id',$cid)
                    ->orderBy('id','desc')
			    	->pluck('sys_chal_no','id')
			    	->toArray();

		$result = view('admin.issue_received.challan_no')->with('cissues',$cissues)->render();

        return Response::json(array(
            'result' => $result
        ));
    }

    public function issuedList(Request $request){
        $bid    = $request->input('bid');
        $cid    = $request->input('cid');
        $chNo   = $request->input('chNo');
        $status = ['empty','refilling','in-stock','issued'];

        $issues      = CylinderIssue::where('branch_id',$bid)->where('cus_id',$cid)
                        ->where('sys_chal_no',$chNo)->first();

        // $issueDetail = CylinderIssueDetail::where('cylinder_issue_id',$issues->id)->get();
        $issueDetail = CylinderIssueDetail::leftjoin('cylinders','cylinder_issue_detail.cyl_id','cylinders.id')
        ->where('cylinder_issue_detail.cylinder_issue_id',$issues->id)
        ->get(['cylinder_issue_detail.*','cylinders.dis_cyl']);

        $gases    = Ga::all()->pluck('name', 'id');
        $cylinder = Cylinder::whereIn('status',$status)
                    ->get(['id','branch_cyl_no','used_for_id']);

        $result = view('admin.issue_received.list',compact('issues','issueDetail','gases','cylinder'))
                ->render();
        
        return Response::json(array(
            'result' => $result
        ));
    }

    public function changeStatus(Request $request){
        DB::beginTransaction();

        try{
            $cyIssDetailId = $request->input('cyIssDetailId');
            $received      = $request->input('received');

            // 
            $detail         = CylinderIssueDetail::find($cyIssDetailId);
            $detail->status = $received;
            $detail->save();

            // 
            CylinderIssueDetail::cylIssueStatus($detail->cylinder_issue_id);

            // 
            $cylinder = Cylinder::find($detail->cyl_id);
            if($cylinder){
                $cylinder->status = ($detail->status)?'empty':'issued';
                $cylinder->save();
            }
            
            DB::commit();
            return Response::json(array(
                'status'  => 'success',
                'message' => 'Cylinder Issue Received Successfully.'
            ));
        }catch(\Exception $e) {
            DB::rollback();
            return Response::json(array(
                'status'  => 'error',
                'message' => 'Opps! Something went wrong,Please try again...'
            ));
        }
    }
}
