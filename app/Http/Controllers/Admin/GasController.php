<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyGaRequest;
use App\Http\Requests\StoreGaRequest;
use App\Http\Requests\UpdateGaRequest;
use App\Models\Ga;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class GasController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ga_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $gas = Ga::all();

        return view('admin.gas.index', compact('gas'));
    }

    public function create()
    {
        abort_if(Gate::denies('ga_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.gas.create');
    }

    public function store(StoreGaRequest $request)
    {
        $ga = Ga::create($request->all());

        $code = Ga::setGasCode($ga->id);
        $ga->code = $code;
        $ga->save();

        return redirect()->route('admin.gas.index')->with('success','Gas Added Successfully!');
    }

    public function edit(Ga $ga)
    {
        abort_if(Gate::denies('ga_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.gas.edit', compact('ga'));
    }

    public function update(UpdateGaRequest $request, Ga $ga)
    {
        $ga->update($request->all());

        return redirect()->route('admin.gas.index')->with('success','Gas Updated Successfully!');
    }

    public function show(Ga $ga)
    {
        abort_if(Gate::denies('ga_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ga->load('gasCylinders', 'usedForCylinders');
        
        return view('admin.gas.show', compact('ga'));
    }

    public function destroy(Ga $ga)
    {
        abort_if(Gate::denies('ga_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ga->delete();

        return back()->with('success','Gas Deleted Successfully!');
    }

    public function massDestroy(MassDestroyGaRequest $request)
    {
        Ga::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
