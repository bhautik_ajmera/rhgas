<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCustomerRequest;
use App\Http\Requests\StoreCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Models\Branch;
use App\Models\Customer;
use App\Models\State;
use App\Models\City;
use App\Models\AssignGas;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Ga;
use Validator;
use Redirect;

class CustomerController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('customer_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $customers = Customer::with(['branch'])->get();

        return view('admin.customers.index', compact('customers'));
    }

    public function create()
    {
        abort_if(Gate::denies('customer_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $branches = Branch::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        $states   = State::all()->pluck('name','id')->prepend(trans('global.pleaseSelect'), '');
        $cities   = City::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        $gases    = Ga::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.customers.create', compact('branches','states','cities','gases'));
    }

    public function store(StoreCustomerRequest $request)
    {        
        $branch   = Branch::find($request->input('branch_id'));
        $customer = Customer::create($request->all());

        $code = Customer::setCusCode($customer->id,$branch);
        $customer->code = $code;
        $customer->save();

        return redirect()->route('admin.customers.index')->with('success','Customer Added Successfully!');
    }

    public function edit(Customer $customer)
    {
        abort_if(Gate::denies('customer_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $branches = Branch::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        $states   = State::all()->pluck('name','id')->prepend(trans('global.pleaseSelect'), '');
        $cities   = City::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        $gases    = Ga::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $customer->load('branch');

        return view('admin.customers.edit', compact('branches', 'customer', 'states', 'cities',  'gases'));
    }

    public function update(UpdateCustomerRequest $request, Customer $customer)
    {
        $customer->update($request->all());

        return redirect()->route('admin.customers.index')->with('success','Customer Updated Successfully!');
    }

    public function show(Customer $customer)
    {
        abort_if(Gate::denies('customer_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $customer->load('branch');
        $state = State::find($customer->state_id);
        $city  = City::find($customer->city_id);
        $gas   = Ga::find($customer->gas_id);

        $assignGas = AssignGas::leftjoin('gas','assign_gas.gas_id', '=', 'gas.id')
              ->where('customer_id',$customer->id)
              ->orderBy('assign_gas.id','desc')
              ->get(["assign_gas.*","gas.name as gname"]);

        return view('admin.customers.show', compact('customer','state','city','gas','assignGas'));
    }

    public function destroy(Customer $customer)
    {
        abort_if(Gate::denies('customer_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $customer->delete();

        return back()->with('success','Customer Deleted Successfully!');
    }

    public function massDestroy(MassDestroyCustomerRequest $request)
    {
        Customer::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function assignGas($cusId){
        $gases    = Ga::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        
        return view('admin.customers.gas.create')->with('cusId',$cusId)->with('gases',$gases);
    }

    public function editAssignGas($assignGID){
        $assignGas = AssignGas::find($assignGID);
        $gases    = Ga::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.customers.gas.edit',compact('assignGas','gases'));
    }

    public function saveAssignGas(Request $request){
        $rules = array(
            'gas_id'     => 'required',
            'rate'       => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'rent'       => 'required|numeric',
            'start_rent' => 'required|numeric'
        );

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            $errors = $validator->messages();
            return Redirect::back()->withErrors($errors);
        }

        $cusId = $request->input('cusId');

        $obj = ($request->has('assignGasId'))?AssignGas::find($request->input('assignGasId')):new AssignGas();
        $obj->customer_id = $cusId;
        $obj->gas_id      = $request->input('gas_id');
        $obj->rate        = $request->input('rate');
        $obj->rent        = $request->input('rent');
        $obj->start_rent  = $request->input('start_rent');
        $obj->save();

        return Redirect::route('admin.customers.show',$cusId);
    }

    public function deleteAssignGas($assignGID){
        $assignGas = AssignGas::find($assignGID);
        $cusId     = $assignGas->customer_id;

        $assignGas->delete();

        return Redirect::route('admin.customers.show',$cusId);
    }
}
