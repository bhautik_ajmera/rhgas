<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Branch;
use App\Models\CylinderCompany;
use App\Models\CylinderIssue;
use App\Models\Ga;
use App\Models\Cylinder;
use App\Models\Customer;
use App\Models\CylinderIssueDetail;
use App\Models\AssignGas;
use Validator;
use Redirect;
use Response;
use App\Http\Requests\MassDestroyCylinderIssueRequest;
use DB;

class CylinderIssueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $result = CylinderIssue::all();
        $result = CylinderIssue::leftjoin('customers','cylinder_issue.cus_id','customers.id')
                        ->whereNull('customers.deleted_at')
                        ->get(['cylinder_issue.*','customers.companny_name as cname']);

        return view('admin.cylinderIssue.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $branches = Branch::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        return view('admin.cylinderIssue.create', compact('branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            $rules = array(
                'branch_id'     => 'required',
                'cus_id'        => 'required',
                'delivery_date' => 'required',
                'cylNo.*'       => 'required',
                'gas.*'         => 'required'
            );

            $validator = Validator::make($request->all(),$rules);

            if($validator->fails()){
                $errors = $validator->messages();
                return redirect()->back()->withErrors($errors)->withInput();
            }

            $poDate = $request->input('po_date');

            $obj                = new CylinderIssue();
            $obj->branch_id     = $request->input('branch_id');
            $obj->cus_id        = $request->input('cus_id');
            $obj->po_date       = (!empty($poDate))?date('Y-m-d',strtotime($poDate)):null;
            $obj->delivery_date = date('Y-m-d',strtotime($request->input('delivery_date')));
            $obj->veh_no        = $request->input('vehicle_no');
            $obj->remark        = $request->input('remark');
            $obj->trans         = $request->input('trans');
            $obj->address       = $request->input('address');
            $obj->mob_no        = $request->input('mob_no');
            $obj->total         = $request->input('total');
            $obj->save();

            $poNum   = CylinderIssue::setPONum($obj->id);
            $sysChNo = CylinderIssue::setSysChNo($obj->id);

            $obj->po_num      = $poNum;
            $obj->sys_chal_no = $sysChNo;
            $obj->save();

            $gas       = $request->input('gas');
            $cylNo     = $request->input('cylNo');
            $rate      = $request->input('rate');
            $rent      = $request->input('rent');
            $startRent = $request->input('startRent');
            $status    = $request->input('status');
            $count     = count($cylNo);

            for($i=0;$i<$count;$i++){
                $cid                    = new CylinderIssueDetail();
                $cid->cylinder_issue_id = $obj->id;
                $cid->cyl_id            = $cylNo[$i];
                $cid->gas_id            = $gas[$i];
                $cid->rate              = $rate[$i];
                $cid->rent              = $rent[$i];
                $cid->start_rent        = $startRent[$i];
                $cid->status            = $status[$i];
                $cid->save();

                $cylinder = Cylinder::find($cid->cyl_id);
                if($cylinder){
                    $cylinder->status = 'issued';
                    $cylinder->save();
                }
            }

            DB::commit();
            return redirect()->route('admin.cylinder-issue.index')->with('success','Cylinder Issue Added Successfully!');
        }catch(\Exception $e) {
            DB::rollback();
            return redirect()->route('admin.cylinder-issue.index')->with('error','Opps! Something went wrong,Please try again...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cylIssue  = CylinderIssue::find($id);
        $branches  = Branch::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        $customers = Customer::where('branch_id',$cylIssue->branch_id)->pluck('companny_name', 'id');
        $gases     = Ga::all()->pluck('name', 'id')->prepend('Select Gas', '')->toArray();
        $status    = ['empty','refilling','in-stock','issued'];

        $cylIssueDetails = CylinderIssueDetail::leftjoin('cylinders','cylinder_issue_detail.cyl_id','cylinders.id')
                        ->where('cylinder_issue_detail.cylinder_issue_id',$id)
                        ->get(['cylinder_issue_detail.*','cylinders.status as cstatus']);
        foreach($cylIssueDetails as $value){
            $cylinderes = Cylinder::where('used_for_id',$value->gas_id)
                        ->whereIn('status',$status)
                        ->pluck('branch_cyl_no','id')->toArray();

            $value->cylinderes =$cylinderes;
        }

        return view('admin.cylinderIssue.view', compact('cylIssue','branches','gases','cylIssueDetails','customers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cylIssue  = CylinderIssue::find($id);
        $branches  = Branch::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        $customers = Customer::where('branch_id',$cylIssue->branch_id)->pluck('companny_name', 'id');
        $gases     = Ga::all()->pluck('name', 'id')->prepend('Select Gas', '')->toArray();
        $status    = ['empty','refilling','in-stock','issued'];

        $cylIssueDetails = CylinderIssueDetail::leftjoin('cylinders','cylinder_issue_detail.cyl_id','cylinders.id')
                        ->where('cylinder_issue_detail.cylinder_issue_id',$id)
                        ->get(['cylinder_issue_detail.*','cylinders.status as cstatus']);

        foreach($cylIssueDetails as $value){
            $cylinderes = Cylinder::where('used_for_id',$value->gas_id)
                        ->whereIn('status',$status)
                        ->pluck('branch_cyl_no','id')->toArray();

            $value->cylinderes =$cylinderes;
        }

        return view('admin.cylinderIssue.edit', compact('cylIssue','branches','customers','gases','cylIssueDetails'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $rules = array(
                'branch_id'     => 'required',
                'cus_id'        => 'required',
                'delivery_date' => 'required',
                'cylNo.*'       => 'required',
                'gas.*'         => 'required'
            );

            $validator = Validator::make($request->all(),$rules);

            if($validator->fails()){
                $errors = $validator->messages();
                return redirect()->back()->withErrors($errors)->withInput();
            }

            $poDate = $request->input('po_date');


            $obj                = CylinderIssue::find($id);
            $obj->branch_id     = $request->input('branch_id');
            $obj->cus_id        = $request->input('cus_id');
            $obj->po_date       = (!empty($poDate))?date('Y-m-d',strtotime($poDate)):null;
            $obj->delivery_date = date('Y-m-d',strtotime($request->input('delivery_date')));
            $obj->veh_no        = $request->input('vehicle_no');
            $obj->remark        = $request->input('remark');
            $obj->trans         = $request->input('trans');
            $obj->total         = $request->input('total');
            $obj->save();

            CylinderIssueDetail::where('cylinder_issue_id',$id)->forceDelete();
            $gas       = $request->input('gas');
            $cylNo     = $request->input('cylNo');
            $rate      = $request->input('rate');
            $rent      = $request->input('rent');
            $startRent = $request->input('startRent');
            $status    = $request->input('status');
            $count     = count($cylNo);

            for($i=0;$i<$count;$i++){
                $cid                    = new CylinderIssueDetail();
                $cid->cylinder_issue_id = $obj->id;
                $cid->cyl_id            = $cylNo[$i];
                $cid->gas_id            = $gas[$i];
                $cid->rate              = $rate[$i];
                $cid->rent              = $rent[$i];
                $cid->start_rent        = $startRent[$i];
                $cid->status            = $status[$i];
                $cid->save();

                $cylinder = Cylinder::find($cid->cyl_id);
                if($cylinder){
                    $cylinder->status = ($cid->status)?'empty':'issued';
                    $cylinder->save();
                }
            }

            // 
            CylinderIssueDetail::cylIssueStatus($obj->id);

            DB::commit();
            return redirect()->route('admin.cylinder-issue.index')->with('success','Cylinder Issue Updated Successfully!');
        }catch(\Exception $e) {
            DB::rollback();
            return redirect()->route('admin.cylinder-issue.index')->with('error','Opps! Something went wrong,Please try again...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        DB::beginTransaction();

        try{
            CylinderIssue::find($id)->delete();

            $cylIds = CylinderIssueDetail::where('cylinder_issue_id',$id)
                    ->pluck('cyl_id')
                    ->toArray();
            Cylinder::cylinderStatus($cylIds,'in-stock');

            CylinderIssueDetail::where('cylinder_issue_id',$id)->delete();
            DB::commit();
            return back()->with('success','Cylinder Issue Deleted Successfully!');
        }catch(\Exception $e) {
            DB::rollback();
            return back()->with('error','Opps! Something went wrong,Please try again...');
        }
    }

    public function massDestroy(MassDestroyCylinderIssueRequest $request){
        DB::beginTransaction();

        try{
            CylinderIssue::whereIn('id', request('ids'))->delete();

            $cylIds = CylinderIssueDetail::whereIn('cylinder_issue_id',request('ids'))
                    ->pluck('cyl_id')
                    ->toArray();
            Cylinder::cylinderStatus($cylIds,'in-stock');

            CylinderIssueDetail::where('cylinder_issue_id',request('ids'))->delete();
            DB::commit();
            return back()->with('success','Cylinder Issue Deleted Successfully!');
        }catch(\Exception $e) {
            DB::rollback();
            return back()->with('error','Opps! Something went wrong,Please try again...');
        }
    }

    public function addMore(Request $request){
        $count  = $request->input('count');
        $result = view('admin.cylinderIssue.add_more',compact('count'))->render();

        return Response::json(array(
            'result' => $result
        ));
    }

    public function getCylNo(Request $request){
        $gasId = $request->input('gasId');
        $cusId = $request->input('cusId');
        $ftype = $request->input('ftype');
        $rate  = '0.00';

        $cylinder  = Cylinder::where('used_for_id',$gasId)
                    ->where('dis_cyl',0)
                    ->whereIn('status',($ftype == 'create')?['in-stock']:['in-stock','issued','empty'])
                    ->get();

        if(!empty($cusId) && !empty($gasId)){
            $assignGas = AssignGas::where('customer_id',$cusId)
                        ->where('gas_id',$gasId)
                        ->first();

            $rate       = ($assignGas)?sprintf('%.2f',$assignGas->rate):'0.00';
            $rent       = ($assignGas)?$assignGas->rent:null;
            $start_rent = ($assignGas)?$assignGas->start_rent:null;
        }

        return Response::json(array(
            'result'     => $cylinder,
            'rate'       => $rate,
            'rent'       => $rent,
            'start_rent' => $start_rent
        ));
    }

    public function deleteIssueDetail(Request $request){
        DB::beginTransaction();
        try{
            $id         = $request->input('id');
            $result     = CylinderIssueDetail::find($id);
            $cylIssueId = $result->cylinder_issue_id;
            $cylId      = $result->cyl_id;

            $result->delete();
            CylinderIssueDetail::cylIssueStatus($cylIssueId);
            Cylinder::cylinderStatus([$cylId],'in-stock');
            
            DB::commit();
            return Response::json(array(
                'status'  => 'success',
                'message' => 'Deleted Successfully'
            ));
        }catch(\Exception $e) {
            DB::rollback();
            return Response::json(array(
                'status'  => 'error',
                'message' => 'Opps! Something went wrong,Please try again...'
            ));
        }
    }

    public function getBranchCus(Request $request){
        $mtype     = $request->input('mtype');
        $branchId  = $request->input('id');
        $customers = Customer::where('branch_id',$branchId)->pluck('companny_name', 'id');

        $view   = ($mtype == 'issue')?'admin.cylinderIssue.customer':'admin.issue_received.customer';
        $result = view($view)->with('customers',$customers)->render();

        return Response::json(array(
            'result' => $result
        ));
    }

    public function getCusDetail(Request $request){
        $cusId    = $request->input('cusId');
        $customer = Customer::custDetail($cusId);

        $result = view('admin.cylinderIssue.view_cus_detail')
                    ->with('customer',$customer)
                    ->render();

        return Response::json(array(
            'result' => $result
        ));
    }

    public function getCustomerGas(Request $request){
        $cusId = $request->input('cusId');

        // $gases = Ga::all()->pluck('name', 'id');
        $gases = AssignGas::leftjoin('gas','assign_gas.gas_id','gas.id')
                ->where('assign_gas.customer_id',$cusId)
                ->pluck('gas.name', 'gas.id');
                
        return Response::json(array(
            'gases' => $gases
        ));
    }

    public function getCustomerTrans(Request $request){
        $cusId    = $request->input('cusId');
        $customer = Customer::find($cusId);

        return Response::json(array(
            'info'       => $customer,
            'cusAddress' => ($customer)?$customer->address.' '.$customer->address2:''
        ));
    }
}
