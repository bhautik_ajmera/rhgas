<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCylinderCompanyRequest;
use App\Http\Requests\StoreCylinderCompanyRequest;
use App\Http\Requests\UpdateCylinderCompanyRequest;
use App\Models\CylinderCompany;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\State;
use App\Models\City;

class CylinderCompanyController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('cylinder_company_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cylinderCompanies = CylinderCompany::all();

        return view('admin.cylinderCompanies.index', compact('cylinderCompanies'));
    }

    public function create()
    {
        abort_if(Gate::denies('cylinder_company_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $states   = State::all()->pluck('name','id')->prepend(trans('global.pleaseSelect'), '');
        $cities   = City::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.cylinderCompanies.create',compact('states','cities'));
    }

    public function store(StoreCylinderCompanyRequest $request)
    {
        $cylinderCompany = CylinderCompany::create($request->all());

        $code = CylinderCompany::setSupCode($cylinderCompany->id);
        $cylinderCompany->code = $code;
        $cylinderCompany->save();
        
        return redirect()->route('admin.cylinder-companies.index')->with('success','Cylinder Company Added Successfully!');
    }

    public function edit(CylinderCompany $cylinderCompany)
    {
        abort_if(Gate::denies('cylinder_company_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $states   = State::all()->pluck('name','id')->prepend(trans('global.pleaseSelect'), '');
        $cities   = City::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.cylinderCompanies.edit', compact('cylinderCompany','states','cities'));
    }

    public function update(UpdateCylinderCompanyRequest $request, CylinderCompany $cylinderCompany)
    {
        $cylinderCompany->update($request->all());

        return redirect()->route('admin.cylinder-companies.index')->with('success','Cylinder Company Updated Successfully!');
    }

    public function show(CylinderCompany $cylinderCompany)
    {
        abort_if(Gate::denies('cylinder_company_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $state = State::find($cylinderCompany->state_id);
        $city  = City::find($cylinderCompany->city_id);

        return view('admin.cylinderCompanies.show', compact('cylinderCompany','state','city'));
    }

    public function destroy(CylinderCompany $cylinderCompany)
    {
        abort_if(Gate::denies('cylinder_company_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cylinderCompany->delete();

        return back()->with('success','Cylinder Company Deleted Successfully!');
    }

    public function massDestroy(MassDestroyCylinderCompanyRequest $request)
    {
        CylinderCompany::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
