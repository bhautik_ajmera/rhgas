<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\CylinderIssue;
use App\Models\CylinderIssueDetail;
use App\Models\AssignGas;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use Response;
use DB;
use PDF;

class InvoiceController extends Controller
{
    public function testpdf(){
        $invoice       = Invoice::find(41);
        $invoiceDetail = InvoiceDetail::where('invoice_id',41)->get();

        return view('admin.invoice.customer.pdf.invoice_pdf_nodata')->with('invoice',$invoice)->with('invoiceDetail',$invoiceDetail);
        
        $data = array();

        $pdf = PDF::loadView('admin.invoice.customer.pdf.invoice_pdf', $data);
        return $pdf->download('invoice.pdf');
    }

    public function index(){
        $invoices = Invoice::orderBy('id','desc')->get();
    	return view('admin.invoice.customer.index',compact('invoices'));
    }

    public function create(){
        $itype     = '1'; // Normal Invoice (Default)
        $customers = Customer::pluck('companny_name','id');

    	return view('admin.invoice.customer.create',compact('customers','itype'));
    }

    public function cusDetail(Request $request){
        $cid     = $request->input('cid');
        $invType = $request->input('invType');
        $colName = ($invType)?'is_invoice_created':'is_rent_invoice_created';

        $customer = Customer::custDetail($cid);
        $result   = view('admin.invoice.customer.from_to_address')
                    ->with('cdetail',$customer)
                    ->render();

        $cylIssue   = CylinderIssue::where('cus_id',$cid)
                    ->where(function($query) use ($invType){
                        if(!$invType){
                            $query->where('is_issue_received',1);
                        }
                    })
                    ->where($colName,0)
                    ->pluck('sys_chal_no','id');
        $chalResult = view('admin.invoice.customer.challan_no')
                    ->with('cylIssue',$cylIssue)
                    ->render();

        $rawData = view('admin.invoice.customer.listing')
                    ->with('cylIssueDetail',array())
                    ->with('itype',$invType)
                    ->render();

        return Response::json(array(
            'result'     => $result,
            'chalResult' => $chalResult,
            'rawData'    => $rawData
        ));
    }

    public function challanDetail(Request $request){
        $challanNo      = $request->input('challanNo');
        $cid            = $request->input('cid');
        $invType        = $request->input('invType');
        $cylIssueDetail = array();
        $allowToCreate  = false;

        if(is_array($challanNo) && count($challanNo) > 0){
            $cylIssueDetail = CylinderIssueDetail::leftjoin('gas','cylinder_issue_detail.gas_id','gas.id')
                        ->leftjoin('cylinders','cylinder_issue_detail.cyl_id','cylinders.id')
                        ->leftjoin('cylinder_issue','cylinder_issue_detail.cylinder_issue_id','cylinder_issue.id')
                        ->whereIn('cylinder_issue_detail.cylinder_issue_id',$challanNo)
                        ->get(['cylinder_issue_detail.*','gas.name as gname','gas.gst as ggst','gas.hsn_code','cylinders.branch_cyl_no','cylinder_issue.trans']);

            $allowToCreate = (count($cylIssueDetail) > 0)?true:false;
        }

        $rawData = view('admin.invoice.customer.listing')
                    ->with('cylIssueDetail',$cylIssueDetail)
                    ->with('itype',$invType)
                    ->render();

        return Response::json(array(
            'rawData'       => $rawData,
            'allowToCreate' => $allowToCreate
        ));
    }

    public function save(Request $request){
        DB::beginTransaction();        
        
        try{
            $invtype       = $request->input('invtype');
            $lastTotal     = $request->input('lastTotal');
            $lastRentTotal = $request->input('lastRentTotal');

            $invoice                     = new Invoice();
            $invoice->invoice_type       = $invtype;
            $invoice->invoice_date       = date('Y-m-d');
            $invoice->to_name            = $request->input('toName');
            $invoice->to_address         = $request->input('toAddress');
            $invoice->to_city            = $request->input('toCity');
            $invoice->to_state           = $request->input('toState');
            $invoice->to_pin_code        = $request->input('toPinCode');
            $invoice->to_mob             = $request->input('toMob');
            $invoice->to_alt_mo          = $request->input('toAltMob');
            $invoice->to_email           = $request->input('toEmail');
            $invoice->to_cus_code        = $request->input('toCusCode');
            $invoice->to_contact_person  = $request->input('toConPer');
            $invoice->to_gst_no          = $request->input('toGST');
            $invoice->to_aadhaar_card_no = $request->input('toAadhaar');
            $invoice->row_total          = ($invtype)?$lastTotal:$lastRentTotal;
            $invoice->cgst6              = sprintf('%.2f',$request->input('cgst6'));
            $invoice->sgst6              = sprintf('%.2f',$request->input('sgst6'));
            $invoice->cgst9              = sprintf('%.2f',$request->input('cgst9'));
            $invoice->sgst9              = sprintf('%.2f',$request->input('sgst9'));
            $invoice->net_total          = $request->input('netTotal');
            $invoice->save();

            $invoice->invoice_no = config('constant.invNoPrefix').sprintf("%03d",$invoice->id);
            $invoice->save();
            
            // 
            $chalNo      = $request->input('chalNo');
            $transDetail = $request->input('transDetail');
            $gas         = $request->input('gas');
            $hsnCode     = $request->input('hsnCode');
            $cylNo       = $request->input('cylNo');
            $issueDate   = $request->input('issueDate');
            $recDate     = $request->input('recDate');
            $total       = $request->input('total');
            $rentPerDay  = $request->input('rentPerDay');
            $startRent   = $request->input('startRent');
            $lateDays    = $request->input('lateDays');
            $rentTotal   = $request->input('rentTotal');
            $tax         = $request->input('tax');
            $count       = count($cylNo);

            for($i=0;$i<$count;$i++){
                $obj              = new InvoiceDetail();
                $obj->invoice_id  = $invoice->id;
                $obj->challan_no  = $chalNo[$i];
                $obj->trans       = $transDetail[$i];
                $obj->gas         = $gas[$i];
                $obj->hsn_code    = $hsnCode[$i];
                $obj->cylinder    = $cylNo[$i];
                $obj->issued_date = date('Y-m-d',strtotime(str_replace("/","-",$issueDate[$i])));

                if($invtype){
                    $obj->price_per_bottle = $total[$i];
                    $obj->qty              = 1;
                    $obj->total            = $total[$i];
                }else{
                    $obj->received_date = date('Y-m-d',strtotime(str_replace("/","-",$recDate[$i])));
                    $obj->rate_per_day  = $rentPerDay[$i];
                    $obj->start_rent    = $startRent[$i];
                    $obj->late_days     = $lateDays[$i];
                    $obj->rent_total    = $rentTotal[$i];
                }

                $obj->tax_per = $tax[$i];
                $obj->save();
            }

            $colName = ($invtype)?'is_invoice_created':'is_rent_invoice_created';
            CylinderIssue::whereIn('sys_chal_no',$chalNo)->update([$colName => 1]);

            DB::commit();
            return redirect()->route('admin.invoice')->with('success','Invoice Created Successfully!');
        }catch(\Exception $e) {
            DB::rollback();
            return redirect()->route('admin.invoice')->with('error','Opps! Something went wrong,Please try again...');
        }
    }

    public function view($invId){
        $invoice   = Invoice::find($invId);
        $invDetail = InvoiceDetail::where('invoice_id',$invId)->get();

        return view('admin.invoice.customer.view',compact('invoice','invDetail'));
    }

    public function download(Request $request,$invId = null){
        $invoiceCopy = 1;
        if(is_null($invId)){
            $invId       = $request->input('invoiceId');
            $invoiceCopy = $request->input('invoiceCopy');
        }

        $invoice   = Invoice::find($invId);
        $invDetail = InvoiceDetail::where('invoice_id',$invId)->get();
        $viewName  = ($invoice->invoice_type)?'admin.invoice.customer.pdf.normal_invoice_pdf':'admin.invoice.customer.pdf.rent_invoice_pdf';

        // $path = base_path('public/images/invoice_logo.PNG');
        // $type = pathinfo($path, PATHINFO_EXTENSION);
        // $data = file_get_contents($path);
        // $pic = 'data:image/'.$type.';base64,'.base64_encode($data);

        // return view($viewName)->with('invoice',$invoice)->with('invoiceDetail',$invDetail)->with('invoiceCopy',$invoiceCopy);
        
        $pdf = PDF::loadView($viewName, [
            'invoice'       => $invoice,
            'invoiceDetail' => $invDetail,
            'invoiceCopy'   => $invoiceCopy
        ])
        ->setPaper('a4', 'landscape')
        // ->setPaper('a4', 'portrait')
        // ->setPaper('a4')
        ->setOptions([
            'isHtml5ParserEnabled' => true, 
            'isRemoteEnabled'      => true,
            'chroot'  => public_path('images'),
            'dpi' => 130
        ]);

        $invNo    = $invoice->invoice_no;
        $fileName = $invNo.'_'.(($invoice->invoice_type)?'Normal':'Rent');

        return $pdf->download($fileName.'.pdf');
    }

    public function delete(Request $request){
        DB::beginTransaction();
        try{
            $invType = $request->input('invType');
            $invId   = $request->input('id');

            $colName   = ($invType)?'is_invoice_created':'is_rent_invoice_created';
            $invDetail = InvoiceDetail::where('invoice_id',$invId)->pluck('challan_no')->toArray();

            Invoice::find($invId)->delete();
            InvoiceDetail::where('invoice_id',$invId)->delete();
            CylinderIssue::whereIn('sys_chal_no',$invDetail)->update([$colName => 0]);

            DB::commit();
            return Response::json(array(
                'status'  => 'success',
                'message' => 'Deleted Successfully'
            ));
        }catch(\Exception $e) {
            DB::rollback();
            return Response::json(array(
                'status'  => 'error',
                'message' => 'Opps! Something went wrong,Please try again...'
            ));
        }
    }
}
