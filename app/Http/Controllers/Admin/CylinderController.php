<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCylinderRequest;
use App\Http\Requests\StoreCylinderRequest;
use App\Http\Requests\UpdateCylinderRequest;
use App\Models\Cylinder;
use App\Models\Ga;
use App\Models\Supplier;
use App\Models\CylinderCompany;
use App\Models\RefillingDetail;
use App\Models\Customer;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CylinderController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('cylinder_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cylinders    = Cylinder::with(['supplier', 'gas', 'used_for'])->get();
        $cylCompanies = CylinderCompany::all()->pluck('name', 'id')->toArray();

        return view('admin.cylinders.index', compact('cylinders','cylCompanies'));
    }

    public function create()
    {
        abort_if(Gate::denies('cylinder_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $suppliers = Supplier::all()->pluck('company_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $gases        = Ga::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        $used_fors    = Ga::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        $cylCompanies = CylinderCompany::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'),'');

        return view('admin.cylinders.create', compact('suppliers', 'gases', 'used_fors', 'cylCompanies'));
    }

    public function store(StoreCylinderRequest $request)
    {
        $cylinder = Cylinder::create($request->all());

        return redirect()->route('admin.cylinders.index')->with('success','Cylinder Added Successfully!');
    }

    public function edit(Cylinder $cylinder)
    {
        abort_if(Gate::denies('cylinder_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $suppliers = Supplier::all()->pluck('company_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $gases = Ga::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $used_fors = Ga::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $cylCompanies = CylinderCompany::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'),'');

        $cylinder->load('supplier', 'gas', 'used_for');

        return view('admin.cylinders.edit', compact('suppliers', 'gases', 'used_fors', 'cylinder', 'cylCompanies'));
    }

    public function update(UpdateCylinderRequest $request, Cylinder $cylinder)
    {
        $cylinder->update($request->all());

        return redirect()->route('admin.cylinders.index')->with('success','Cylinder Updated Successfully!');
    }

    public function show(Cylinder $cylinder)
    {
        abort_if(Gate::denies('cylinder_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cylinder->load('supplier', 'gas', 'used_for');

        $cylCom = CylinderCompany::find($cylinder->company_cyl_id);

        return view('admin.cylinders.show', compact('cylinder','cylCom'));
    }

    public function destroy(Cylinder $cylinder)
    {
        abort_if(Gate::denies('cylinder_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cylinder->delete();

        return back()->with('success','Cylinder Deleted Successfully!');
    }

    public function massDestroy(MassDestroyCylinderRequest $request)
    {
        Cylinder::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function report($cusId = null){
        $customer = Customer::find($cusId);
        
        if(!is_null($cusId) && !$customer){
            $statusCode = '404';
            $message    = 'No Record Found!';
            return view('admin.error_page.error',compact('statusCode','message'));
        }

        $data = Cylinder::leftjoin('refilling_detail','cylinders.id','refilling_detail.cyl_id')
                ->leftjoin('gas','refilling_detail.gas_id','gas.id')
                ->leftjoin('refilling','refilling_detail.refilling_id','refilling.id')
                ->leftjoin('suppliers','refilling.supplier_id','suppliers.id')
                ->orderBy('refilling_detail.id','desc')
                ->get([
                    'cylinders.branch_cyl_no','cylinders.status as cstatus','cylinders.dis_cyl','gas.name as gname',
                    'suppliers.company_name as sname','refilling.date','refilling.sys_chal_no as refchal',
                    'refilling_detail.updated_at as rd_ua','refilling_detail.status',
                    'refilling_detail.cyl_id','refilling_detail.id as refilling_detail_id'
                ]);

        return view('admin.report.cylinder.index',compact('data','customer'));
    }

    public function cylDiscontinue(Request $request){
        $cylinderId = $request->input('hidcid');
        $disCyl     = $request->input('dis_cyl');
        $status     = (isset($disCyl) &&  $disCyl == 'on')?1:0;
        $note       = ($status)?$request->input('dis_note'):null;

        $cylinder = Cylinder::find($cylinderId);
        if($cylinder){
            $cylinder->dis_cyl = $status;
            $cylinder->dis_note = $note;
            $cylinder->save();
        }

        return redirect()->route('admin.cylinders.index')->with('success','Cylinder Updated Successfully!');
    }
}
