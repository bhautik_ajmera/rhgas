<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyPurchaseRequest;
use App\Http\Requests\StorePurchaseRequest;
use App\Http\Requests\UpdatePurchaseRequest;
use App\Models\Branch;
use App\Models\Purchase;
use App\Models\PurchaseDetail;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use DB;
use File;

class PurchaseController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('purchase_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $purchases = Purchase::with(['branch'])->get();

        return view('admin.purchases.index', compact('purchases'));
    }

    public function create()
    {
        abort_if(Gate::denies('purchase_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $branches = Branch::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.purchases.create', compact('branches'));
    }

    public function store(StorePurchaseRequest $request)
    {
        DB::beginTransaction();
        try{
            $product = $request->input('product');
            $qty     = $request->input('qty');
            $rate    = $request->input('rate');
            $per     = $request->input('per');
            $tamount = $request->input('tamount');
            $count   = count($product); 

            $purchase = Purchase::create($request->all());

            // 
            $path = storage_path('app/public/purchase');
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777, true, true);
            }

            if($request->has('file')){
                $fileName = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT).'.'.$request->file->extension();
                $request->file->move(storage_path('app/public/purchase'), $fileName);
                $purchase->file = $fileName;
                $purchase->save();
            }
            // 

            for($i=0;$i<$count;$i++){
                $obj               = new PurchaseDetail();
                $obj->purchase_id  = $purchase->id;
                $obj->product      = $product[$i];
                $obj->qty          = $qty[$i];
                $obj->rate         = $rate[$i];
                $obj->per          = $per[$i];
                $obj->total_amount = $tamount[$i];
                $obj->save();
            }

            DB::commit();
            return redirect()->route('admin.purchases.index')->with('success','Purchase Added Successfully');
        }catch(\Exception $e) {
            DB::rollback();
            return redirect()->route('admin.purchases.index')->with('error','Opps! Something went wrong,Please try again...');
        }
    }

    public function edit(Purchase $purchase)
    {
        abort_if(Gate::denies('purchase_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $branches = Branch::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $purchase->load('branch');

        $purDetail = PurchaseDetail::where('purchase_id',$purchase->id)->get();

        return view('admin.purchases.edit', compact('branches', 'purchase', 'purDetail'));
    }

    public function update(UpdatePurchaseRequest $request, Purchase $purchase)
    {
        DB::beginTransaction();
        try{
            $purchase->update($request->all());

            //
            $path = storage_path('app/public/purchase');
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777, true, true);
            }

            if($request->has('file')){
                $fileName = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT).'.'.$request->file->extension();
                $request->file->move(storage_path('app/public/purchase'), $fileName);
                $purchase->file = $fileName;
                $purchase->save();
            }
            //

            PurchaseDetail::where('purchase_id',$purchase->id)->forceDelete();
            $product = $request->input('product');
            $qty     = $request->input('qty');
            $rate    = $request->input('rate');
            $per     = $request->input('per');
            $tamount = $request->input('tamount');
            $count   = count($product);

            for($i=0;$i<$count;$i++){
                $obj               = new PurchaseDetail();
                $obj->purchase_id  = $purchase->id;
                $obj->product      = $product[$i];
                $obj->qty          = $qty[$i];
                $obj->rate         = $rate[$i];
                $obj->per          = $per[$i];
                $obj->total_amount = $tamount[$i];
                $obj->save();
            }
            
            DB::commit();
            return redirect()->route('admin.purchases.index')->with('success','Purchase Updated Successfully');
        }catch(\Exception $e) {
            DB::rollback();
            return redirect()->route('admin.purchases.index')->with('error','Opps! Something went wrong,Please try again...');
        }
    }

    public function show(Purchase $purchase)
    {
        abort_if(Gate::denies('purchase_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $purchase->load('branch');

        $purDetail = PurchaseDetail::where('purchase_id',$purchase->id)->get();
        $branches  = Branch::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.purchases.show', compact('purchase','purDetail','branches'));
    }

    public function destroy(Purchase $purchase)
    {
        abort_if(Gate::denies('purchase_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $purchase->delete();
        PurchaseDetail::where('purchase_id',$purchase->id)->delete();

        return back()->with('success','Purchase Deleted Successfully');
    }

    public function massDestroy(MassDestroyPurchaseRequest $request)
    {
        Purchase::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function addMore(Request $request){
        $count    = $request->input('count');
        
        $result = view('admin.purchases.add_more')
                    ->with('count',$count)
                    ->render();

        return \Response::json(array(
            'result' => $result
        ));
    }

    public function deletePurchase(Request $request){
        $id = $request->input('id');
        PurchaseDetail::find($id)->delete();

        return \Response::json(array(
            'status' => 'success'
        ));
    }

    public function download($id){
        $purchase = Purchase::find($id);
        $file     = public_path()."/storage/purchase/".$purchase->file;
        return \Response::download($file,$purchase->file);
    }
}
