<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCylinderRequest;
use App\Http\Requests\UpdateCylinderRequest;
use App\Http\Resources\Admin\CylinderResource;
use App\Models\Cylinder;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CylinderApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('cylinder_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CylinderResource(Cylinder::with(['supplier', 'gas', 'used_for'])->get());
    }

    public function store(StoreCylinderRequest $request)
    {
        $cylinder = Cylinder::create($request->all());

        return (new CylinderResource($cylinder))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Cylinder $cylinder)
    {
        abort_if(Gate::denies('cylinder_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CylinderResource($cylinder->load(['supplier', 'gas', 'used_for']));
    }

    public function update(UpdateCylinderRequest $request, Cylinder $cylinder)
    {
        $cylinder->update($request->all());

        return (new CylinderResource($cylinder))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Cylinder $cylinder)
    {
        abort_if(Gate::denies('cylinder_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cylinder->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
