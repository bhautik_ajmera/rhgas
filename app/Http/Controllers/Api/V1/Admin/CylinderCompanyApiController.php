<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCylinderCompanyRequest;
use App\Http\Requests\UpdateCylinderCompanyRequest;
use App\Http\Resources\Admin\CylinderCompanyResource;
use App\Models\CylinderCompany;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CylinderCompanyApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('cylinder_company_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CylinderCompanyResource(CylinderCompany::all());
    }

    public function store(StoreCylinderCompanyRequest $request)
    {
        $cylinderCompany = CylinderCompany::create($request->all());

        return (new CylinderCompanyResource($cylinderCompany))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(CylinderCompany $cylinderCompany)
    {
        abort_if(Gate::denies('cylinder_company_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CylinderCompanyResource($cylinderCompany);
    }

    public function update(UpdateCylinderCompanyRequest $request, CylinderCompany $cylinderCompany)
    {
        $cylinderCompany->update($request->all());

        return (new CylinderCompanyResource($cylinderCompany))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(CylinderCompany $cylinderCompany)
    {
        abort_if(Gate::denies('cylinder_company_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cylinderCompany->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
