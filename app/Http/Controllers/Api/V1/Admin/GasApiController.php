<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreGaRequest;
use App\Http\Requests\UpdateGaRequest;
use App\Http\Resources\Admin\GaResource;
use App\Models\Ga;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class GasApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ga_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new GaResource(Ga::all());
    }

    public function store(StoreGaRequest $request)
    {
        $ga = Ga::create($request->all());

        return (new GaResource($ga))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Ga $ga)
    {
        abort_if(Gate::denies('ga_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new GaResource($ga);
    }

    public function update(UpdateGaRequest $request, Ga $ga)
    {
        $ga->update($request->all());

        return (new GaResource($ga))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Ga $ga)
    {
        abort_if(Gate::denies('ga_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ga->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
