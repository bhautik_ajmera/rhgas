<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreIncomeExpenseRequest;
use App\Http\Requests\UpdateIncomeExpenseRequest;
use App\Http\Resources\Admin\IncomeExpenseResource;
use App\Models\IncomeExpense;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class IncomeExpenseApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('income_expense_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new IncomeExpenseResource(IncomeExpense::with(['branch'])->get());
    }

    public function store(StoreIncomeExpenseRequest $request)
    {
        $incomeExpense = IncomeExpense::create($request->all());

        return (new IncomeExpenseResource($incomeExpense))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(IncomeExpense $incomeExpense)
    {
        abort_if(Gate::denies('income_expense_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new IncomeExpenseResource($incomeExpense->load(['branch']));
    }

    public function update(UpdateIncomeExpenseRequest $request, IncomeExpense $incomeExpense)
    {
        $incomeExpense->update($request->all());

        return (new IncomeExpenseResource($incomeExpense))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(IncomeExpense $incomeExpense)
    {
        abort_if(Gate::denies('income_expense_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $incomeExpense->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
