<?php

namespace App\Http\Requests;

use App\Models\Supplier;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateSupplierRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('supplier_edit');
    }

    public function rules()
    {
        return [
            'code'             => [
                'required',
            ],
            'company_name'     => [
                'required',
            ],
            'contact_person'   => [
                'required',
            ],
            'email'            => [
                'nullable',
            ],
            'mob_no'           => [
                'numeric',
                'required',
            ],
            'alternate_mob_no' => [
                'numeric',
                'nullable',
            ],
            'office_address'   => [
                'required',
            ],
            'office_pin_code'  => [
                'numeric',
                'nullable',
            ],
            'office_state'     => [
                'required',
            ],
            'office_city'      => [
                'required',
            ],
            'plant_address'    => [
                'required',
            ],
            'plant_pin_code'   => [
                'numeric',
                'nullable',
            ],
            'plant_state'      => [
                'required',
            ],
            'plant_city'       => [
                'required',
            ],
            'gst_no'           => [
                'required',
            ],
            'pan_no'           => [
                'nullable',
            ],
        ];
    }
}
