<?php

namespace App\Http\Requests;

use App\Models\Customer;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateCustomerRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('customer_edit');
    }

    public function rules()
    {
        return [
            'companny_name'       => [
                'required',
                'unique:customers,companny_name,' . request()->route('customer')->id,
            ],
            'code'                => [
                'required',
            ],
            'email'               => [
                'email',
                'nullable',
            ],
            'branch_id'           => [
                'required',
                'integer',
            ],
            'deposit'             => [
                'required',
                'numeric',
            ],
            'pin_code'            => [
                'numeric',
                'nullable',
            ],
            'mob_no'              => [
                'numeric',
                'nullable',
            ],
            'alternate_mob_no'    => [
                'numeric',
                'nullable',
            ],
            'addhar_card_no'      => [
                'numeric',
                'nullable',
            ],
        ];
    }
}
