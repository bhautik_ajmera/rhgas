<?php

namespace App\Http\Requests;

use App\Models\CylinderCompany;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateCylinderCompanyRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('cylinder_company_edit');
    }

    public function rules()
    {
        return [
            'code'      => [
                'required',
            ],
            'name'      => [
                'required',
            ],
            // 'mobile_no' => [
            //     'required',
            //     'numeric'
            // ],
            // 'address'   => [
            //     'required',
            // ],
            // 'pin_code'  => [
            //     'nullable',
            //     'numeric'
            // ],
            // 'state_id'  => [
            //     'required',
            // ],
            // 'city_id'   => [
            //     'required',
            // ]
        ];
    }
}
