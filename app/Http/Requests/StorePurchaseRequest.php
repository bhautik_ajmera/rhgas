<?php

namespace App\Http\Requests;

use App\Models\Purchase;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StorePurchaseRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('purchase_create');
    }

    public function rules()
    {
        return [
            'branch_id' => [
                'required'
            ],
            'supplier_name' => [
                'string',
                'required',
            ],
            'date'          => [
                'required',
                'date_format:' . config('panel.date_format'),
            ],
            'invoice_no'    => [
                'string',
                'required',
            ],
            'product.*'       => [
                'required'
            ],
            'qty.*'         => [
                'required'
            ],
            'rate.*'        => [
                'required'
            ],
            'tamount.*'     => [
                'required'
            ]
        ];
    }
}
