<?php

namespace App\Http\Requests;

use App\Models\CylinderCompany;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyCylinderCompanyRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('cylinder_company_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:cylinder_companies,id',
        ];
    }
}
