<?php

namespace App\Http\Requests;

use App\Models\Cylinder;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreCylinderRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('cylinder_create');
    }

    public function rules()
    {
        return [
            // 'supplier_id'         => [
            //     'required',
            //     'integer',
            // ],
            'company_cyl_id'      => [
                'required'
            ],
            'company_cyl_no'      => [
                'required',
            ],
            'branch_cyl_no'       => [
                'required',
            ],
            'capacity'            => [
                'required',
            ],
            'manufacturing_date'  => [
                'required',
                'date_format:' . config('panel.date_format'),
            ],
            'last_hydrotest_date' => [
                'required',
                'date_format:' . config('panel.date_format'),
            ],
            'gas_id'              => [
                'required',
                'integer',
            ],
            'used_for_id'         => [
                'required',
                'integer',
            ],
        ];
    }
}
