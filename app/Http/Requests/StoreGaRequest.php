<?php

namespace App\Http\Requests;

use App\Models\Ga;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreGaRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('ga_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'required',
            ],
            'hsn_code' => [
                'required',
                'numeric'
            ],
            'gst' => [
                'required',
            ]
        ];
    }
}
