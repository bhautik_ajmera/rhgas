<?php

namespace App\Http\Requests;

use App\Models\Ga;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateGaRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('ga_edit');
    }

    public function rules()
    {
        return [
            'code' => [
                'required',
            ],
            'name' => [
                'required',
            ],
            'hsn_code' => [
                'required',
                'numeric'
            ],
            'gst' => [
                'required',
            ]
        ];
    }
}
