<?php

namespace App\Http\Requests;

use App\Models\Branch;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateBranchRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('branch_edit');
    }

    public function rules()
    {
        return [
            'name'            => [
                'required',
                'unique:branches,name,' . request()->route('branch')->id,
            ],
            'code'            => [
                'required',
            ],
            'contact_person'  => [
                'required',
            ],
            'company_mob_no'  => [
                'numeric',
                'required',
            ],
            'personal_mob_no' => [
                'numeric',
                'nullable',
            ],
            'address'         => [
                'required',
            ],
            'pin_code'        => [
                'required',
            ],
            'state_id'        => [
                'required',
            ],
            'city_id'         => [
                'required',
            ],
        ];
    }
}
