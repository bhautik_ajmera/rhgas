<?php

namespace App\Http\Requests;

use App\Models\User;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('user_edit');
    }

    public function rules()
    {
        return [
            'name'    => [
                'required',
            ],
            'email'   => [
                'required',
                'email',
                'unique:users,email,' . request()->route('user')->id,
            ],
            'roles.*' => [
                'integer',
            ],
            'roles'   => [
                'required',
                'array',
            ],
            'code'           => [
                'required',
            ],
            'mobile_no'      => [
                'required',
            ],
            'address'        => [
                'required',
            ],
            'city_id'        => [
                'required',
                'integer',
            ],
            'pin_code'       => [
                'required',
            ],
            'state_id'       => [
                'required',
                'integer',
            ],
            'addhar_card_no' => [
                'nullable',
            ],
            'ref_con_name'   => [
                'nullable',
            ],
            'ref_con_no'     => [
                'nullable',
            ],
            'relation'       => [
                'nullable',
            ],
        ];
    }
}
