<?php

namespace App\Http\Requests;

use App\Models\IncomeExpense;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyIncomeExpenseRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('income_expense_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:income_expenses,id',
        ];
    }
}
