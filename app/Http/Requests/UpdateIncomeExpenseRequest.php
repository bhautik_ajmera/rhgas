<?php

namespace App\Http\Requests;

use App\Models\IncomeExpense;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateIncomeExpenseRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('income_expense_edit');
    }

    public function rules()
    {
        return [
            'branch_id' => [
                'required',
                'integer',
            ],
            'date'      => [
                'required',
                'date_format:' . config('panel.date_format'),
            ],
            'incRate.*'   => [
                'required'
            ],
            'expRate.*'   => [
                'required'
            ]
        ];
    }
}
