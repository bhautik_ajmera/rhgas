<?php

namespace App\Http\Requests;

use App\Models\CylinderCompany;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreCylinderCompanyRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('cylinder_company_create');
    }

    public function rules()
    {
        return [
            'name'      => [
                'required',
            ],
            // 'mobile_no' => [
            //     'required',
            //     'numeric'
            // ],
            // 'address'   => [
            //     'required',
            // ],
            // 'pin_code'  => [
            //     'nullable',
            //     'numeric'
            // ],
            // 'state_id'  => [
            //     'required',
            // ],
            // 'city_id'   => [
            //     'required',
            // ]
        ];
    }
}
