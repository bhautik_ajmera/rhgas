<?php

namespace App\Http\Requests;

use App\Models\Refilling;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRefillingRequest extends FormRequest
{
    // public function authorize()
    // {
    //     abort_if(Gate::denies('ga_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

    //     return true;
    // }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            // 'ids.*' => 'exists:gas,id',
        ];
    }
}
