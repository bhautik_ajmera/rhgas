<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceDetail extends Model
{
    use SoftDeletes,HasFactory;

    public $table = 'invoice_detail';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'invoice_id',
        'challan_no',
        'trans',
        'gas',
        'hsn_code',
        'cylinder',
        'issued_date',
        'received_date',
        'price_per_bottle',
        'qty',
        'total',
        'rate_per_day',
        'start_rent',
        'late_days',
        'rent_total',
        'tax_per'
    ];
}
