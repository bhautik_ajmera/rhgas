<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class CylinderCompany extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'cylinder_companies';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'code',
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public static function setSupCode($id){
        $number = sprintf("%03d",$id);
        $code   = substr(strtoupper(config('constant.cylinderCompanyPrefix')), 0, 3);
        
        return $code.$number;
    }
}
