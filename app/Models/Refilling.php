<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Refilling extends Model
{
    use SoftDeletes,HasFactory;

    public $table = 'refilling';

    protected $dates = [
        'date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'supplier_id',
        'sys_chal_no',
        'date',
        'vehicle_no',
        'remark',
        'is_refield',
        'total'
    ];

    public static function setSysChNo($id){
        $number = sprintf("%03d",$id);
        $code   = substr(strtoupper(config('constant.refSysChalNoPrefix')), 0, 4);
        
        return $code.$number;
    }
}
