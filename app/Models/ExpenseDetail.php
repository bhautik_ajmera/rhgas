<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpenseDetail extends Model
{
    use HasFactory,SoftDeletes;

    public $table = 'expense_detail';

    protected $fillable = [
        'income_expense_id',
        'desc',
        'rate'
    ];
}
