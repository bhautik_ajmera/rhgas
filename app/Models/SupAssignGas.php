<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupAssignGas extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'sup_assign_gas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'supplier_id',
        'gas_id',
        'rate',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public static function supplierGas($sid){
        $gases = SupAssignGas::leftjoin('gas','sup_assign_gas.gas_id','gas.id')
            ->where('sup_assign_gas.supplier_id',$sid)
            ->where('sup_assign_gas.rate','>',0)
            ->pluck('gas.name','gas.id')
            ->toArray();

        return $gases;
    }
}
