<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Ga extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'gas';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'code',
        'hsn_code',
        'gst',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public static function setGasCode($id){
        $number = sprintf("%03d",$id);
        $code   = substr(strtoupper(config('constant.gasCodePrefix')), 0, 3);
        
        return $code.$number;
    }

    public function gasCylinders()
    {
        return $this->hasMany(Cylinder::class, 'gas_id', 'id');
    }

    public function usedForCylinders()
    {
        return $this->hasMany(Cylinder::class, 'used_for_id', 'id');
    }
}
