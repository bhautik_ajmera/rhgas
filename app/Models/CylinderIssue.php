<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;
use App\Models\Branch;
use App\Models\CylinderCompany;

class CylinderIssue extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'cylinder_issue';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'branch_id',
        'cus_id',
        'po_num',
        'po_date',
        'sys_chal_no',
        'delivery_date',
        'veh_no',
        'remark',
        'is_issue_received',
        'trans',
        'total',
        'is_invoice_created',
        'is_rent_invoice_created',
        'address',
        'mob_no'
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public static function setPONum($id){
        $number = sprintf("%03d",$id);
        $code   = substr(strtoupper(config('constant.poNumPrefix')), 0, 3);
        
        return $code.$number;
    }

    public static function setSysChNo($id){
        $number = sprintf("%03d",$id);
        $code   = substr(strtoupper(config('constant.sysChalNoPrefix')), 0, 3);
        
        return $code.$number;
    }

    public static function getBranch($id){
        $branch = Branch::find($id);
        return ($branch)?$branch->name:'';
    }

    public static function getCylCom($id){
        $cylCom = CylinderCompany::find($id);
        return ($cylCom)?$cylCom->name:'';
    }
}
