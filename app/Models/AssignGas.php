<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignGas extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'assign_gas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'gas_id',
        'rate',
        'rent',
        'start_rent',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
