<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Customer extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'customers';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'companny_name',
        'code',
        'email',
        'branch_id',
        'contact_person',
        'address',
        'address2',
        'pin_code',
        'state_id',
        'city_id',
        'mob_no',
        'alternate_mob_no',
        'gstno',
        'addhar_card_no',
        'addhar_card_address',
        'remark',
        'trans',
        'deposit',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public static function setCusCode($id,$branch){
        $branchName = $branch->name;
        $code       = substr(strtoupper($branchName), 0, 3);
        $number     = sprintf("%03d",$id);
        
        return config('constant.cusCodePrefix').$code.$number;
    }

    public static function custDetail($cusId){
        $customer = Customer::leftjoin('branches','customers.branch_id','=','branches.id')
                    ->leftjoin('states','customers.state_id','=','states.id')
                    ->leftjoin('cities','customers.city_id','=','cities.id')
                    ->whereNull('branches.deleted_at')
                    ->whereNull('states.deleted_at')
                    ->whereNull('cities.deleted_at')
                    ->where('customers.id','=',$cusId)
                    ->select(['customers.*','branches.name as bname','states.name as sname','cities.name as cname'])
                    ->first();

        return $customer;
    }
}
