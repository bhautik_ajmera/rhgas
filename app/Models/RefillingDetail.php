<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Refilling;
use App\Models\CylinderIssueDetail;
use App\Models\CylinderIssue;

class RefillingDetail extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'refilling_detail';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'refilling_detail',
        'gas_id',
        'cyl_id',
        'rate',
        'status'
    ];

    public static function refillingStatus($rid){
        $allRefilled = RefillingDetail::where('refilling_id',$rid)
                            ->where('status',0)
                            ->exists();

        $refilling = Refilling::find($rid);
        $refilling->is_refield = (!$allRefilled)?1:0;
        $refilling->save();
    }

    // This is for refilling received
    public static function isLocked($refillingId,$cylId,$updatedAt){
        $data1 = RefillingDetail::where('refilling_id','>',$refillingId)
                    ->where('cyl_id',$cylId)
                    ->exists();

        $data2 = CylinderIssueDetail::where('cyl_id',$cylId)
                    ->where('created_at','>',$updatedAt)
                    ->first();

        return ($data1 || $data2)?true:false;
    }

    // This is for refilling
    public static function isProFurther($cylId,$updatedAt,$cstatus){
        $data = CylinderIssueDetail::where('cyl_id',$cylId)
                ->where('created_at','>',$updatedAt)
                ->first();

        return ($data || $cstatus == 'in-stock')?true:false;
    } 

    // public static function customerCycle($value){
    //     $data = CylinderIssueDetail::where('cyl_id',$value->cyl_id)
    //                 ->where('created_at','>',$value->rd_ua)
    //                 ->first();

    //     if($data){
    //         $info = CylinderIssue::leftjoin('customers','cylinder_issue.cus_id','customers.id')
    //                     ->where('cylinder_issue.id',$data->cylinder_issue_id)
    //                     ->first(['customers.companny_name','cylinder_issue.delivery_date']);

    //         $value->cname         = ($info)?$info->companny_name:'';
    //         $value->issue_date    = ($info)?date('d/m/Y',strtotime($info->delivery_date)):'';
    //         $value->received_date = ($data->status)?date('d/m/Y',strtotime($data->updated_at)):'';
    //         $value->status1       = ($data->status)?'Empty':'Issued';
    //     }

    //     return $value;
    // }
}
