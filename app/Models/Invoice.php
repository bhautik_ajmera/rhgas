<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes,HasFactory;

	public $table = 'invoice';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'invoice_type',
        'invoice_no',
        'date',
        'to_name',
        'to_address',
        'to_city',
        'to_state',
        'to_pin_code',
        'to_mob',
        'to_alt_mo',
        'to_email',
        'to_cus_code',
        'to_contact_person',
        'to_gst_no',
        'to_aadhaar_card_no',
        'row_total',
        'cgst6',
        'sgst6',
        'cgst9',
        'sgst9',
        'net_total'
    ];
}
