<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IncomeDetail extends Model
{
    use HasFactory,SoftDeletes;

    public $table = 'income_detail';

    protected $fillable = [
        'income_expense_id',
        'desc',
        'rate'
    ];
}
