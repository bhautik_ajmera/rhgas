<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;
use App\Models\CylinderIssueDetail;
use App\Models\CylinderIssue;
use App\Models\RefillingDetail;

class Cylinder extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'cylinders';

    protected $dates = [
        'manufacturing_date',
        'last_hydrotest_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        // 'supplier_id',
        'company_cyl_id',
        'company_cyl_no',
        'branch_cyl_no',
        'capacity',
        'manufacturing_date',
        'last_hydrotest_date',
        'gas_id',
        'used_for_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'status',
        'dis_cyl',
        'dis_note'
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function getManufacturingDateAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setManufacturingDateAttribute($value)
    {
        $this->attributes['manufacturing_date'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

    public function getLastHydrotestDateAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setLastHydrotestDateAttribute($value)
    {
        $this->attributes['last_hydrotest_date'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

    public function gas()
    {
        return $this->belongsTo(Ga::class, 'gas_id');
    }

    public function used_for()
    {
        return $this->belongsTo(Ga::class, 'used_for_id');
    }

    public static function supplierGas($sid,$status){
        $gases = Cylinder::leftjoin('gas','cylinders.used_for_id','gas.id')
                // ->where('cylinders.supplier_id',$sid)
                ->whereIn('cylinders.status',$status)
                ->whereNull('gas.deleted_at')
                ->pluck('gas.name','gas.id')
                ->toArray();

        return $gases;
    }

    public static function cylinderStatus($cylIds,$status){
        foreach($cylIds as $index => $cylId){
            $cylinder = Cylinder::find($cylId);

            if($cylinder){
                $cylinder->status = $status;
                $cylinder->save();
            }
        }

        return true;
    }

    public static function customerCycle($value){
        $value->cname                 = '-';
        $value->issue_date            = '-';
        $value->received_date         = '-';
        $value->status1               = '-';
        $value->issue_chal            = '-';
        $value->rent_per_day          = '0.00';
        $value->start_rent_after_days = '-';
        $value->holiding_days         = '-';
        $value->holding_charge        = '0.00';
        $value->disp_as_dis_cyl       = false;

        // Manage Cylinder Discontinue Status
        if($value->dis_cyl){
            $refDetail = RefillingDetail::where('cyl_id',$value->cyl_id)
                        ->orderBy('id','desc')
                        ->first('id');

            if($refDetail && $refDetail->id == $value->refilling_detail_id){
                $value->disp_as_dis_cyl = true;
            }

            if(!$refDetail && $value->dis_cyl){
                $value->disp_as_dis_cyl = true;
            }
        }

        if(isset($value->cyl_id)){
            $data = CylinderIssueDetail::where('cyl_id',$value->cyl_id)
                        ->where('created_at','>',$value->rd_ua)
                        ->first();

            if($data){
                $info = CylinderIssue::leftjoin('customers','cylinder_issue.cus_id','customers.id')
                            ->where('cylinder_issue.id',$data->cylinder_issue_id)
                            ->first(['customers.companny_name','cylinder_issue.delivery_date','cylinder_issue.sys_chal_no as issue_chal']);

                $value->cname         = ($info)?$info->companny_name:'-';
                $value->issue_date    = ($info)?date('d/m/Y',strtotime($info->delivery_date)):'-';
                $value->received_date = ($data->status)?date('d/m/Y',strtotime($data->updated_at)):'-';
                $value->status1       = ($data->status)?'Empty':'Issued';
                $value->issue_chal    = ($info)?$info->issue_chal:'-';

                // 
                $issuedDate   = ($info)?date('Y-m-d',strtotime($info->delivery_date)):null;
                $receivedDate = ($data->status)?date('Y-m-d',strtotime($data->updated_at)):date('Y-m-d');
                $rentPerDay   = (!is_null($data->rent))?$data->rent:0;
                $startRent    = (!is_null($data->start_rent))?$data->start_rent:0;

                if(!is_null($issuedDate)){
                    $calResult = $data->calRent($issuedDate,$receivedDate,$rentPerDay,$startRent,true);

                    $value->rent_per_day          = sprintf('%.2f',$rentPerDay);
                    $value->start_rent_after_days = $startRent;                
                    $value->holiding_days         = $calResult['holiding_days'];
                    $value->holding_charge        = sprintf('%.2f',$calResult['holding_charge']);
                }
            }            
        }

        return $value;
    }
}
