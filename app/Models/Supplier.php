<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;
use App\Models\State;
use App\Models\City;

class Supplier extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'suppliers';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'code',
        'company_name',
        'contact_person',
        'email',
        'mob_no',
        'alternate_mob_no',
        'office_address',
        'office_pin_code',
        'office_state',
        'office_city',
        'plant_address',
        'plant_pin_code',
        'plant_state',
        'plant_city',
        'gst_no',
        'pan_no',
        'remark',
        'gas_id',
        'rate',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public static function setSupCode($id){
        $number = sprintf("%03d",$id);
        $code   = substr(strtoupper(config('constant.supCodePrefix')), 0, 3);
        
        return $code.$number;
    }

    public function supplierCylinders()
    {
        return $this->hasMany(Cylinder::class, 'supplier_id', 'id');
    }

    public function getState($id){
        $state = State::find($id);
        return ($state)?$state->name:'';
    }

    public function getCity($id){
        $city = City::find($id);
        return ($city)?$city->name:'';
    }
}
