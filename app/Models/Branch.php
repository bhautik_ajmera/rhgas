<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Branch extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'branches';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'code',
        'contact_person',
        'company_mob_no',
        'personal_mob_no',
        'address',
        'pin_code',
        'state_id',
        'city_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function branchCustomers()
    {
        return $this->hasMany(Customer::class, 'branch_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public static function setBranchCode($id,$branchName){
        $number = sprintf("%03d",$id);
        $code   = substr(strtoupper($branchName), 0, 3);
        
        return config('constant.branchCodePrefix').$code.$number;
    }
}
