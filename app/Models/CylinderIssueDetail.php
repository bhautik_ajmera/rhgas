<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\CylinderIssue;
use App\Models\RefillingDetail;
use DateTime;

class CylinderIssueDetail extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'cylinder_issue_detail';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cylinder_issue_id',
        'cyl_id',
        'gas_id',
        'rate',
        'status',
        'rent',
        'start_rent'
    ];

    public static function cylIssueStatus($cid){
        $info = CylinderIssueDetail::where('cylinder_issue_id',$cid)
                ->where('status',0)
                ->exists();

        $data = CylinderIssue::find($cid);
        $data->is_issue_received = (!$info)?1:0;
        $data->save();
    }

    // This is for cylinder issue received
    public static function isLocked($cylIssueId,$cylId,$updatedAt){
        $data1 = CylinderIssueDetail::where('cylinder_issue_id','>',$cylIssueId)
                    ->where('cyl_id',$cylId)
                    ->exists();

        $data2 = RefillingDetail::where('cyl_id',$cylId)
                    ->where('created_at','>',$updatedAt)
                    ->first();

        return ($data1 || $data2)?true:false;
    }

    // This is for cylinder issue
    public static function isProFurther($cylId,$updatedAt,$cstatus){
        $data = RefillingDetail::where('cyl_id',$cylId)
                ->where('created_at','>',$updatedAt)
                ->first();

        return ($data || $cstatus == 'empty')?true:false;
    }

    public static function getIssedReceivedDate($id){
        $data = CylinderIssue::find($id);
        return $data;
    }
    
    public static function calRent($issuedDate,$receivedDate,$rentPerDay,$startRent,$repReq = false){
        $issuedDate   = new DateTime(date('Y-m-d',strtotime($issuedDate)));
        $receivedDate = new DateTime(date('Y-m-d',strtotime($receivedDate)));
        // $receivedDate = new DateTime();
        // $receivedDate->modify('+3 day');

        $dayDifferent = $issuedDate->diff($receivedDate)->format("%d");
        $temp         = $total = 0;
        $info         = array();

        if($dayDifferent > $startRent){
            $temp  = $dayDifferent - $startRent;
            $total = $temp * $rentPerDay;
        }

        $info['holiding_days']  = $temp;
        $info['holding_charge'] = $total;

        return ($repReq)?$info:$total;
    }
}
