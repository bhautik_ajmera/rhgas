<?php

Route::get('artisan_command',function(){
    // \Artisan::call('migrate --seed');
    // \Artisan::call('config:cache');
    // \Artisan::call('config:clear');
    // \Artisan::call('config:cache');

    echo "Done";
    exit;
});

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Cities
    Route::delete('cities/destroy', 'CityController@massDestroy')->name('cities.massDestroy');
    Route::resource('cities', 'CityController');

    // Branches
    Route::delete('branches/destroy', 'BranchController@massDestroy')->name('branches.massDestroy');
    Route::resource('branches', 'BranchController');
    // Route::get('get/state/cities','BranchController@getStateCities')->name('getStateCities');

    // States
    Route::delete('states/destroy', 'StateController@massDestroy')->name('states.massDestroy');
    Route::resource('states', 'StateController');

    // Customers
    Route::delete('customers/destroy', 'CustomerController@massDestroy')->name('customers.massDestroy');
    Route::resource('customers', 'CustomerController');
    Route::get('customers/assign/gas/{cusId}','CustomerController@assignGas')->name('customers.assign_gas');
    Route::get('customers/edit/assign/gas/{assignGID}','CustomerController@editAssignGas')->name('customers.edit.assign_gas');
    Route::post('customers/save/assign/gas','CustomerController@saveAssignGas')->name('customers.save.assign_gas');
    Route::get('customers/delete/assign/gas/{assignGID}','CustomerController@deleteAssignGas');

    // Suppliers
    Route::delete('suppliers/destroy', 'SupplierController@massDestroy')->name('suppliers.massDestroy');
    Route::resource('suppliers', 'SupplierController');
    Route::get('suppliers/assign/gas/{supId}','SupplierController@assignGas')->name('suppliers.assign_gas');
    Route::get('suppliers/edit/assign/gas/{assignGID}','SupplierController@editAssignGas')->name('suppliers.edit.assign_gas');
    Route::post('suppliers/save/assign/gas','SupplierController@saveAssignGas')->name('suppliers.save.assign_gas');
    Route::get('suppliers/delete/assign/gas/{assignGID}','SupplierController@deleteAssignGas');
    

    // Gas
    Route::delete('gas/destroy', 'GasController@massDestroy')->name('gas.massDestroy');
    Route::resource('gas', 'GasController');

    // Cylinder Companies
    Route::delete('cylinder-companies/destroy', 'CylinderCompanyController@massDestroy')->name('cylinder-companies.massDestroy');
    Route::resource('cylinder-companies', 'CylinderCompanyController');

    // Cylinders
    Route::delete('cylinders/destroy', 'CylinderController@massDestroy')->name('cylinders.massDestroy');
    Route::resource('cylinders', 'CylinderController');
    Route::post('cylinders/discontinue', 'CylinderController@cylDiscontinue')->name('cylinders.discontinue');

    // Refilling
    Route::get('cylinder/refilling','RefillingController@index')->name('cylinder.refilling');
    Route::get('cylinder/refilling/create','RefillingController@create')->name('cylinder.refilling.create');
    Route::post('cylinder/refilling/store','RefillingController@store')->name('cylinder.refilling.store');
    Route::get('cylinder/refilling/{id}/edit','RefillingController@edit')->name('cylinder.refilling.edit');
    Route::post('cylinder/refilling/update','RefillingController@update')->name('cylinder.refilling.update');
    Route::delete('cylinder/refilling/destroy/{id}','RefillingController@delete')->name('cylinder.refilling.destroy');
    Route::any('cylinder/refilling/mass/destroy', 'RefillingController@massDestroy')->name('cylinder.refilling.mass.destroy');
    Route::get('cylinder/refilling/{id}','RefillingController@show')->name('cylinder.refilling.show');
    
    Route::get('get/supplier/detail','RefillingController@getSupDetail');
    Route::get('cyl-refi/add_more','RefillingController@addMore');
    Route::get('cylinder/refilling/supplier/gas','RefillingController@getSupplierGas');
    Route::get('cylinder/refilling/gas/cyl_no','RefillingController@getGasCylinder');
    Route::get('delete/refilling_detail','RefillingController@deleteRefillingDetail');

    // Refilling Received
    Route::get('cyl/refilling/received','RefillingReceivedController@index')->name('cyl.refilling.received');
    Route::get('get/supplier/challan','RefillingReceivedController@getSupChal');
    Route::get('get/refilling/list','RefillingReceivedController@refillingList');
    Route::get('change/refilling/status','RefillingReceivedController@changeStatus');

    // Cylinder Issue
    Route::any('cylinder-issue/destroy', 'CylinderIssueController@massDestroy')->name('cylinderIssue.massDestroy');
    Route::resource('cylinder-issue', 'CylinderIssueController');
    Route::get('cyl-issue/add_more','CylinderIssueController@addMore');
    Route::get('cylinder-issue/get/cyl-no','CylinderIssueController@getCylNo');
    Route::get('get/branch/customer','CylinderIssueController@getBranchCus');
    Route::get('get/customer/detail','CylinderIssueController@getCusDetail');
    Route::get('delete/issue_detail','CylinderIssueController@deleteIssueDetail');
    Route::get('cylinder-issue/customer/gas','CylinderIssueController@getCustomerGas');
    Route::get('cylinder-issue/customer/trans','CylinderIssueController@getCustomerTrans');

    // Cylinder Issue Received
    Route::get('cyl-issue/received','IssueReceivedController@index')->name('cylinderIssue.received');
    Route::get('cyl-issue/received/get/challan','IssueReceivedController@getChallan');
    Route::get('get/issued/list','IssueReceivedController@issuedList');
    Route::get('change/issue/status','IssueReceivedController@changeStatus');

    // Income Expenses
    Route::delete('income-expenses/destroy', 'IncomeExpenseController@massDestroy')->name('income-expenses.massDestroy');
    Route::resource('income-expenses', 'IncomeExpenseController');
    Route::get('income-expenses/{type}/add_more','IncomeExpenseController@addMore');
    Route::get('income-expenses/delete/{type}','IncomeExpenseController@deleteIncExp');

    // Purchases
    Route::delete('purchases/destroy', 'PurchaseController@massDestroy')->name('purchases.massDestroy');
    Route::resource('purchases', 'PurchaseController');
    Route::get('purchase/add_more','PurchaseController@addMore');
    Route::get('purchase/download/{id}','PurchaseController@download')->name('purchase.download');
    Route::get('delete/purchase','PurchaseController@deletePurchase');

    // Income Expense Report
    Route::match(['GET','POST'],'report/income-expense','IncomeExpenseController@report')->name('report.income-expense');
    Route::get('report/cylinder/{cusId?}','CylinderController@report')->name('report.cylinder');

    // Invoice
    Route::get('invoice','InvoiceController@index')->name('invoice');
    Route::get('invoice/create','InvoiceController@create')->name('invoice.create');
    Route::get('invoice/customer/detail','InvoiceController@cusDetail');
    Route::get('invoice/get/challan_detail','InvoiceController@challanDetail');
    Route::post('invoice/save','InvoiceController@save')->name('invoice.save');
    Route::get('invoice/delete','InvoiceController@delete')->name('invoice.delete');
    Route::get('invoice/view/{invId}','InvoiceController@view')->name('invoice.view');

    Route::match(['get','post'],'invoice/download/{invId?}','InvoiceController@download')->name('invoice.download');

    Route::get('testpdf','InvoiceController@testpdf');
});

Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
        Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
        Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
    }
});
