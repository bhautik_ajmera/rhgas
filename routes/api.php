<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:sanctum']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Cities
    Route::apiResource('cities', 'CityApiController');

    // Branches
    Route::apiResource('branches', 'BranchApiController');

    // States
    Route::apiResource('states', 'StateApiController');

    // Customers
    Route::apiResource('customers', 'CustomerApiController');

    // Suppliers
    Route::apiResource('suppliers', 'SupplierApiController');

    // Gas
    Route::apiResource('gas', 'GasApiController');

    // Cylinders
    Route::apiResource('cylinders', 'CylinderApiController');

    // Cylinder Companies
    Route::apiResource('cylinder-companies', 'CylinderCompanyApiController');

    // Income Expenses
    Route::apiResource('income-expenses', 'IncomeExpenseApiController');

    // Purchases
    Route::apiResource('purchases', 'PurchaseApiController');
});
