$(document).ready(function(){
    $('#cyliComFrm').validate({
        rules: {
            name: {
                required: true
            },
            // mobile_no: {
            //     required: true,
            //     minlength: 10
            // },
            // address: {
            //     required: true
            // },
            // pin_code: {
            //     minlength: 6
            // },
            // state_id: {
            //     required: true
            // },
            // city_id: {
            //     required: true
            // }
        },messages: {
            name: {
                required: "Please enter name"
            },
            // mobile_no: {
            //     required: "Please enter mobile number",
            //     minlength: "Please enter 10 digit mobile number"
            // },
            // address: {
            //     required: "Please enter address"
            // },
            // pin_code: {
            //     minlength: "Please enter 6 digit pin code"
            // },
            // state_id: {
            //     required: "Please select state"
            // },
            // city_id: {
            //     required: "Please select city"
            // }
        },errorPlacement: function(error, element) {
            error.insertAfter(element);
            
            // if(element.attr("name") == "state_id") {
            //     error.insertAfter("#stateError");
            // }else if(element.attr("name") == "city_id"){
            //     error.insertAfter("#cityError");
            // }else{
            //     error.insertAfter(element);
            // }
        }
    });
});