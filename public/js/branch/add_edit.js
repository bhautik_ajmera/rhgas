$(document).ready(function(){
    $('#branchFrm').validate({
        rules: {
            name: {
                required: true
            },
            contact_person: {
                required: true
            },
            company_mob_no: {
                required: true,
                minlength: 10
            },
            personal_mob_no: {
                minlength: 10
            },
            address: {
                required: true
            },
            pin_code: {
                required: true,
                minlength: 6
            },
            state_id: {
                required: true
            },
            city_id: {
                required: true
            }
        },messages: {
            name: {
                required: "Please enter name"
            },
            contact_person: {
                required: "Please enter contact person"
            },
            company_mob_no: {
                required: "Please enter company mobile no",
                minlength: "Please enter 10 digit mobile no"
            },
            personal_mob_no: {
                minlength: "Please enter 10 digit mobile no"
            },
            address: {
                required: "Please enter address"
            },
            pin_code: {
                required: "Please enter pin code",
                minlength: "Please enter 6 digit pin code"
            },
            state_id: {
                required: "Please select state"
            },
            city_id: {
                required: "Please select city"
            }
        },errorPlacement: function(error, element) {
            if(element.attr("name") == "city_id") {
                error.insertAfter("#cityError");
            }else if(element.attr("name") == "state_id") {
                error.insertAfter("#stateError");
            }else{
                error.insertAfter(element);
            }
        }
    });

    // $(document).on("change", "#state_id", function(){
    //     var stateId = $(this).val();

    //     $.ajax({
    //         type: "GET",
    //         url: getsiteurl()+'/admin/get/state/cities',
    //         data:{'stateId':stateId},
    //         success: function(response) {
    //             if(response.status == 'success'){
    //                 $('#renderCities').empty();
    //                 $("#renderCities").append(response.result);

    //                 $('#city_id').select2();
    //             }
    //         }
    //     });
    // });
});