$(document).ready(function(){
    var sid = '';

  	$('#searchFrm').validate({
	    rules: {
	    	supId: {
	    		required: true
	    	},
	    	sysChalNo: {
	      		required: true
	    	}
	    },messages: {
	    	supId: {
	    		required: "Please select supplier"
	    	},
	    	sysChalNo: {
	      		required: "Please select challan no"
	    	}
	    },errorPlacement: function(error, element) {
            if(element.attr("name") == "supId") {
                error.insertAfter("#supError");
            }else if(element.attr("name") == "sysChalNo") {
                error.insertAfter("#chaError");
            } 
        },submitHandler: function (form) {
            var chNo = $('#sysChalNo').val();

            $.ajax({
                type: "GET",
                url: getsiteurl()+'/admin/get/refilling/list',
                data:{'sid':sid,'chNo':chNo},
                success: function(response) {
                    $('#renderList').empty();
                    $("#renderList").append(response.result);
                    rateTotal();
                }
            });
        }
	});

    $(document).on("change", "#supId", function(event) {
    	sid = $(this).val();

    	$.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/get/supplier/challan',
            data:{'sid':sid},
            success: function(response) {
                $('#renderChalNo').empty();
                $("#renderChalNo").append(response.result);
  				$('.select2').select2();
            }
        });
	});

    $(document).on("change", ".status", function(event) {
        var rdid     = $(this).attr('id');
        var received = $(this).prop("checked")?1:0;

        $.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/change/refilling/status',
            data:{'rdid':rdid,'received':received},
            success: function(response) {
                Swal.fire({
                    type: response.status,
                    title: response.message,
                    showConfirmButton:false,
                    timer: 1000
                }); 
            }
        });
    });

    function rateTotal(){
        var sum = 0.00;

        $('.rate').each(function() {
            sum = sum + parseFloat($(this).val());
        });

        $('#total').val(sum.toFixed(2));
    }
});