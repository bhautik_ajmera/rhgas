$(document).ready(function(){
    $('#cityFrm').validate({
        rules: {
            state_id: {
                required: true
            },
            name: {
                required: true
            }
        },messages: {
            state_id: {
                required: "Please select state"
            },
            name: {
                required: "Please enter name"
            }
        },errorPlacement: function(error, element) {
            if(element.attr("name") == "state_id") {
                error.insertAfter("#stateError");
            }else{
                error.insertAfter(element);
            }
        }
    });
});