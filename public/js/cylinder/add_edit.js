$(document).ready(function(){
    $('#cylFrm').validate({
        rules: {
            // supplier_id: {
            //     required: true
            // },
            company_cyl_id: {
                required: true
            },
            company_cyl_no: {
                required: true
            },
            branch_cyl_no: {
                required: true
            },
            capacity: {
                required: true
            },
            manufacturing_date: {
                required: true
            },
            last_hydrotest_date: {
                required: true
            },
            gas_id: {
                required: true
            },
            used_for_id: {
                required: true
            }
        },messages: {
            // supplier_id: {
            //     required: "Please select supplier"
            // },
            company_cyl_id: {
                required: "Please select company cylinder"
            },
            company_cyl_no: {
                required: "Please enter company cylinder number"
            },
            branch_cyl_no: {
                required: "Please enter branch cylinder number"
            },
            capacity: {
                required: "Please enter capacity"
            },
            manufacturing_date: {
                required: "Please select manufacturing date"
            },
            last_hydrotest_date: {
                required: "Please select last hydrotest date"
            },
            gas_id: {
                required: "Please select alloted to"
            },
            used_for_id: {
                required: "Please select used for"
            }
        },errorPlacement: function(error, element) {
            // if(element.attr("name") == "supplier_id") {
            //     error.insertAfter("#companyError");
            // }

            if(element.attr("name") == "gas_id"){
                error.insertAfter("#allotedtoError");
            }else if(element.attr("name") == "used_for_id"){
                error.insertAfter("#usedforError");
            }else if(element.attr("name") == "company_cyl_id"){
                error.insertAfter("#cmpCylError");
            }else{
                error.insertAfter(element);
            }
        }
    });

    $(document).on("change", "#dis_cyl", function(){
        $('#dis_note').attr('readonly', !$(this).prop("checked"));

        if(!$(this).prop("checked")){
            $('#dis_note').val('');
        }
    });
});