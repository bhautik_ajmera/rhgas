$(document).ready(function(){
  	let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
	let inTable = $('.incomeListing:not(.ajaxTable)').DataTable({ buttons: dtButtons })
	let exTable = $('.expenseListing:not(.ajaxTable)').DataTable({ buttons: dtButtons })

	$.extend(true, $.fn.dataTable.defaults, {
    	orderCellsTop: true,
    	order: [[ 1, 'desc' ]],
    	pageLength: 10,
  	});

	$('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
	    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
	});

    $('#startEndDate').daterangepicker({
    	autoUpdateInput: false,
        locale: {
            format: 'DD/MM/YYYY',
            cancelLabel: 'Clear'
        }
    }, function(start, end, label) {
    	// 
  	});

    $('input[name="startEndDate"]').on('apply.daterangepicker', function(ev, picker) {
    	$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
  	});

  	$('#searchFrm').validate({
	    rules: {
	    	branch_id: {
	      		required: true
	    	},
	      	startEndDate: {
	      		required: true
	      	}
	    },messages: {
	    	branch_id: {
	      		required: "Please select branch"
	    	},
	      	startEndDate: {
	      		required: "Please select date"
	      	}
	    },errorPlacement: function(error, element) {
            if(element.attr("name") == "branch_id") {
                error.insertAfter("#branchError");
            }else{
                error.insertAfter(element);
            }
        }
	});
});