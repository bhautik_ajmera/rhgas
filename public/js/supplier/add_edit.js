$(document).ready(function(){
    jQuery.validator.addMethod("valid_email", function (value, element) {
        return this.optional(element) || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
    }, 'Please enter valid email');

    $('#supFrm').validate({
        rules: {
            company_name: {
                required: true
            },
            contact_person: {
                required: true
            },
            email: {
                valid_email: true
            },
            mob_no: {
                required: true,
                minlength: 10
            },
            alternate_mob_no: {
                minlength: 10
            },
            office_address: {
                required: true
            },
            office_pin_code: {
                minlength: 6
            },
            office_state: {
                required: true
            },
            office_city: {
                required: true
            },
            plant_address: {
                required: true
            },
            plant_pin_code: {
                minlength: 6
            },
            plant_state: {
                required: true
            },
            plant_city: {
                required: true
            },
            gst_no: {
                required: true,
                minlength: 15
            },
            pan_no: {
                minlength: 10
            }
        },messages: {
            code: {
                required: "Please enter code"
            },
            company_name: {
                required: "Please enter company name"
            },
            contact_person: {
                required: "Please enter contact person"
            },
            mob_no: {
                required: "Please enter mobile number",
                minlength: "Please enter 10 digit mobile number"
            },
            alternate_mob_no: {
                minlength: "Please enter 10 digit mobile number"
            },
            office_address: {
                required: "Please enter office address"
            },
            office_pin_code: {
                minlength: "Please enter 6 digit pin code"
            },
            office_state: {
                required: "Please select state"
            },
            office_city: {
                required: "Please select city"
            },
            plant_address: {
                required: "Please enter plant address"
            },
            plant_pin_code: {
                minlength: "Please enter 6 digit pin code"
            },
            plant_state: {
                required: "Please select state"
            },
            plant_city: {
                required: "Please select city"
            },
            gst_no: {
                required: "Please enter GST number",
                minlength: "Please enter 15 digit GST number"
            },
            pan_no: {
                minlength: "Please enter 10 digit PAN number"
            }
        },errorPlacement: function(error, element) {
            if(element.attr("name") == "office_state"){
                error.insertAfter("#officeStateError");
            }else if(element.attr("name") == "office_city"){
                error.insertAfter("#officeCityError");
            }else if(element.attr("name") == "plant_state"){
                error.insertAfter("#plantStateError");
            }else if(element.attr("name") == "plant_city"){
                error.insertAfter("#planCityError");
            }else{
                error.insertAfter(element);
            }
        }
    });
});