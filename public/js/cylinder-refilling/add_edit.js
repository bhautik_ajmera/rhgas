$(document).ready(function(){
    var supplierId = '';
    var ftype = 'create';

    if($("#refillingId").length){
        supplierId = $('#supplier_id').val();
        ftype = 'edit';
        rateTotal();
    }

    $(".rdate").datepicker({
        dateFormat: 'dd-mm-yy',
        maxDate: new Date()
    });
    
    $('#cylRefiFrm').validate({
        rules: {
            supplier_id: {
                required: true
            },
            date: {
                required: true
            }
        },messages: {
            supplier_id: {
                required: "Please select supplier"
            },
            date: {
                required: "Please select date"
            }
        },errorPlacement: function(error, element) {
            if(element.attr("name") == "supplier_id") {
                error.insertAfter("#suppError");
            }else{
                error.insertAfter(element);
            }
        },submitHandler: function (form) {
            var result = $("#test select").filter(function () {
                if($(this).attr('name') == 'gas[]' || $(this).attr('name') == 'cylNo[]'){
                    ($(this).val() == '')?$(this).addClass('is-invalid'):$(this).removeClass('is-invalid');
                    return $(this).val() == ''
                }
            }).length == 0;

            if(result == true){
                form.submit();
            }
        }
    });

    $(document).on("click", "#view_sup", function(){
        supplierId = $('#supplier_id').val();

        if(supplierId != ''){
            $.ajax({
                type: "GET",
                url: getsiteurl()+'/admin/get/supplier/detail',
                data:{'supId':supplierId},
                success: function(response) {
                    $('#renderSupDetail').empty();
                    $("#renderSupDetail").append(response.result);
                    $('#supplier-modal').modal('show');
                }
            });
        }
    });

    $(document).on("click", "#addMore", function(){
        var count = $('#rowCount').val();
        count++;
        $('#rowCount').val(count);

        $.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/cyl-refi/add_more',
            data:{'count':count},
            success: function(response) {
                $("#listing tbody").closest('table').find('tr:last').prev().after(response.result);
                // $("#listing tbody").append(response.result);

                if(supplierId != ''){
                    supplierGas(count)
                }
            }
        });
    });

    $(document).on("click", ".remove", function(){
        var id = $(this).attr('id');
        $('#newRef_'+id).remove();
        rateTotal();
    });

    $(document).on("change", "#supplier_id", function(){
        supplierId = $(this).val();

        $('.gas'+' option').remove();
        $('.gas').append($("<option></option>").attr("value", '').text('Select Gas'));

        $('.cyl_no'+' option').remove();
        $('.cyl_no').append($("<option></option>").attr("value", '').text('Select Cylinder No'));
        
        $('.rate').val('0.00');
        
        supplierGas(null);
    });

    function supplierGas(count){
        $.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/cylinder/refilling/supplier/gas',
            data:{'sid':supplierId,'ftype':ftype},
            success: function(response) {
                if(count == null){
                    Object.values(response.gases).map((gname,index) => {
                        $('.gas')
                        .append($("<option></option>")
                        .attr("value", Object.keys(response.gases)[index])
                        .text(gname));
                    });
                }else{
                    Object.values(response.gases).map((gname,index) => {
                        $('#gas_'+count)
                        .append($("<option></option>")
                        .attr("value", Object.keys(response.gases)[index])
                        .text(gname));
                    });
                }
            }
        });
    }

    $(document).on("change", ".gas", function(){
        var gid       = $(this).val();
        var elementId = $(this).attr('id');        
        var val       = elementId.substr(elementId.indexOf('_')+1);
        
        $('#cyl_'+val+' option').remove();
        $('#cyl_'+val).append($("<option></option>").attr("value", '').text('Select Cylinder No'));

        $.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/cylinder/refilling/gas/cyl_no',
            data:{'gid':gid,'sid':supplierId,'ftype':ftype},
            success: function(response) {
                if(response.cylinder.length > 0){
                    Object.values(response.cylinder).map((value) => {
                        $('#cyl_'+val)
                        .append($("<option></option>")
                        .attr("value", value.id)
                        .text((value.branch_cyl_no).toUpperCase()));
                    });
                }
                
                $('#rate_'+val).val(response.rate);
                rateTotal();
            }
        });
    });

    $(document).on("change", ".cyl_no", function(){
        var ab = $(this).val();
        var cd = $(this).attr('data-id');

        $(".cyl_no").each(function(){
            if($(this).attr('data-id') != cd && $(this).val() != '' && $(this).val() == ab){
                $('#cyl_'+cd).prop("selectedIndex", 0);

                Swal.fire({
                    type: 'info',
                    title: 'Opps!Cylinder No is already selected,Please select different'
                });
                
                return false;
            }
        });
    });

    $(document).on("click", ".delete", function(){
        var id  = $(this).attr('id');
        var row = $(this).attr('data-row');

        Swal.fire({
          title: 'Are you sure you want to delete?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then((result) => {
            if(result.value) {
                $.ajax({
                    type: "GET",
                    url: getsiteurl()+'/admin/delete/refilling_detail',
                    data:{'id':id},
                    success: function(response) {
                        if(response.status == 'success'){
                            $('#storedDetail_'+id).remove();
                            rateTotal();
                        }
                    }
                });
            }
        });
    });

    function rateTotal(){
        var sum = 0.00;

        $('.rate').each(function() {
            sum = sum + parseFloat($(this).val());
        });

        $('#total').val(sum.toFixed(2));
    }
});