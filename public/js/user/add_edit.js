$(document).ready(function(){
    jQuery.validator.addMethod("valid_email", function (value, element) {
        return this.optional(element) || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
    }, 'Please enter valid email');

    $('#userCreateFrm').validate({
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                valid_email: true
            },
            password: {
                required: true
            },
            mobile_no: {
                required: true,
                minlength: 10
            },
            address: {
                required: true
            },
            pin_code: {
                required: true,
                minlength: 6
            },
            state_id: {
                required: true
            },
            city_id: {
                required: true
            },
            addhar_card_no: {
                minlength: 12
            },
            ref_con_no: {
                minlength: 10
            }
        },messages: {
            name: {
                required: "Please enter name"
            },
            email: {
                required: "Please enter email"
            },
            password: {
                required: "Please enter password"
            },
            mobile_no: {
                required: "Please enter mobile number",
                minlength: "Please enter 10 digit mobile number"
            },
            address: {
                required: "Please enter address"
            },
            pin_code: {
                required: "Please enter pin code",
                minlength: "Please enter 6 digit pin code"
            },
            state_id: {
                required: "Please select state"
            },
            city_id: {
                required: "Please select city"
            },
            addhar_card_no: {
                minlength: "Please enter 12 digit addhar card no"
            },
            ref_con_no: {
                minlength: "Please enter 10 digit mobile number"
            }
        },errorPlacement: function(error, element) {
            if(element.attr("name") == "state_id"){
                error.insertAfter("#stateError");
            }else if(element.attr("name") == "city_id"){
                error.insertAfter("#cityError");
            }else{
                error.insertAfter(element);
            }
        }
    });

    $('#userEditFrm').validate({
        rules: {
            code: {
                required: true
            },
            name: {
                required: true
            },
            email: {
                required: true,
                valid_email: true
            },
            mobile_no: {
                required: true,
                minlength: 10
            },
            address: {
                required: true
            },
            pin_code: {
                required: true,
                minlength: 6
            },
            state_id: {
                required: true
            },
            city_id: {
                required: true
            },
            addhar_card_no: {
                minlength: 12
            },
            ref_con_no: {
                minlength: 10
            }
        },messages: {
            code: {
                required: "Please enter code"
            },
            name: {
                required: "Please enter name"
            },
            email: {
                required: "Please enter email"
            },
            mobile_no: {
                required: "Please enter mobile number",
                minlength: "Please enter 10 digit mobile number"
            },
            address: {
                required: "Please enter address"
            },
            pin_code: {
                required: "Please enter pin code",
                minlength: "Please enter 6 digit pin code"
            },
            state_id: {
                required: "Please select state"
            },
            city_id: {
                required: "Please select city"
            },
            addhar_card_no: {
                minlength: "Please enter 12 digit addhar card no"
            },
            ref_con_no: {
                minlength: "Please enter 10 digit mobile number"
            }
        },errorPlacement: function(error, element) {
            if(element.attr("name") == "state_id"){
                error.insertAfter("#stateError");
            }else if(element.attr("name") == "city_id"){
                error.insertAfter("#cityError");
            }else{
                error.insertAfter(element);
            }
        }
    });
});