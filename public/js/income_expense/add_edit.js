$(document).ready(function(){
    $('#inExFrm').validate({
        rules: {
            branch_id: {
                required: true
            },
            date: {
                required: true
            }
        },messages: {
            branch_id: {
                required: "Please select branch"
            },
            date: {
                required: "Please select date"
            }
        },errorPlacement: function(error, element) {
            if(element.attr("name") == "branch_id") {
                error.insertAfter("#branchError");
            }else{
                error.insertAfter(element);
            }
        },submitHandler: function(form) {
            var result = $("#inExDiv input").filter(function () {
                if($(this).attr('type') == 'text' && ($(this).attr('name') == 'incRate[]' || $(this).attr('name') == 'expRate[]')){
                    ($.trim($(this).val()).length == 0)?$(this).addClass('is-invalid'):$(this).removeClass('is-invalid');
                    return $.trim($(this).val()).length == 0
                }
            }).length == 0;

            if(result == true){
                form.submit();
            }
        }
    });

    var countInc = 0;
    $(document).on("click", "#addMoreInc", function(){
        countInc++;

        $.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/income-expenses/income/add_more',
            data:{'count':countInc},
            success: function(response) {
                $('#addIncome').append(response.result); 
            }
        });
    });

    var countExp = 0;
    $(document).on("click", "#addMoreExp", function(){
        countExp++;

        $.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/income-expenses/expense/add_more',
            data:{'count':countExp},
            success: function(response) {
                $('#addExpense').append(response.result); 
            }
        });
    });

    $(document).on("click", ".remove_inc", function(){
        $('#newIncome_'+$(this).attr('id')).remove();
    });

    $(document).on("click", ".remove_exp", function(){
        $('#newExpense_'+$(this).attr('id')).remove();
    });

    $(document).on("click", ".deleteInc", function(){
        var id = $(this).attr('id');

        if(confirm('Are you sure you want to delete?')){
            $.ajax({
                type: "GET",
                url: getsiteurl()+'/admin/income-expenses/delete/income',
                data:{'id':id},
                success: function(response) {
                    $('#storedInc_'+id).remove();
                }
            });
        }
    });

    $(document).on("click", ".deleteExp", function(){
        var id = $(this).attr('id');

        if(confirm('Are you sure you want to delete?')){
            $.ajax({
                type: "GET",
                url: getsiteurl()+'/admin/income-expenses/delete/expense',
                data:{'id':id},
                success: function(response) {
                    $('#storedExp_'+id).remove();
                }
            });
        }
    });
});