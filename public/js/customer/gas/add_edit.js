$(document).ready(function(){
    $('#assignGasFrm').validate({
        rules: {
            gas_id: {
                required: true
            },
            rate: {
                required: true
            },
            rent: {
                required: true
            },
            start_rent: {
                required: true
            }
        },messages: {
            gas_id: {
                required: "Please select gas"
            },
            rate: {
                required: "Please enter rate"
            },
            rent: {
                required: "Please enter rent/day"
            },
            start_rent: {
                required: "Please enter days"
            }
        },errorPlacement: function(error, element) {
            if(element.attr("name") == "gas_id"){
                error.insertAfter("#gasError");
            }else{
                error.insertAfter(element);
            }
        }
    });
});