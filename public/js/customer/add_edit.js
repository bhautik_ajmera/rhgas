$(document).ready(function(){
    jQuery.validator.addMethod("valid_email", function (value, element) {
        return this.optional(element) || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
    }, 'Please enter valid email');

    $('#cusFrm').validate({
        rules: {
            companny_name: {
                required: true
            },
            email: {
                valid_email: true
            },
            branch_id: {
                required: true
            },
            deposit: {
                required: true
            },
            pin_code: {
                minlength: 6
            },
            mob_no: {
                minlength: 10
            },
            alternate_mob_no: {
                minlength: 10
            },
            gstno: {
                minlength: 15
            },
            addhar_card_no: {
                minlength: 12
            }
        },messages: {
            companny_name: {
                required: "Please enter company name"
            },
            branch_id: {
                required: "Please select branch"
            },
            deposit: {
                required: "Please enter deposit"
            },
            pin_code: {
                minlength: "Please enter 6 digit pin code"
            },
            mob_no: {
                minlength: "Please enter 10 digit mobile no"
            },
            alternate_mob_no: {
                minlength: "Please enter 10 digit mobile no"
            },
            gstno: {
                minlength: "Please enter 15 digit GST no"
            },
            addhar_card_no: {
                minlength: "Please enter 12 digit addhar card no"
            }
        },errorPlacement: function(error, element) {
            if(element.attr("name") == "branch_id") {
                error.insertAfter("#branchError");
            }else{
                error.insertAfter(element);
            }
        }
    });
});