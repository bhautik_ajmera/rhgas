$(document).ready(function(){
    $('#gasFrm').validate({
        rules: {
            name: {
                required: true
            },
            hsn_code: {
                required: true
            },
            gst: {
                required: true
            }
        },messages: {
            name: {
                required: "Please enter name"
            },
            hsn_code: {
                required: "Please enter HSN Code"
            },
            gst: {
                required: "Please select gst"
            }
        },errorPlacement: function(error, element) {
            if(element.attr("name") == "gst") {
                error.insertAfter("#gstError");
            }else{
                error.insertAfter(element);
            }
        }
    });
});