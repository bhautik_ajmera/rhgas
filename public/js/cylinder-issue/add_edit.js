$(document).ready(function(){
    var cusId = '';
    var ftype = 'create';

    if($("#cylIssId").length){
        cusId = $('#cus_id').val()
        ftype = 'edit';
        rateTotal();
    }

    $(".ddate").datepicker({
        dateFormat: 'dd-mm-yy',
        // maxDate: new Date()
    });

    $('#cylIssueFrm').validate({
        rules: {
            branch_id: {
                required: true
            },
            cus_id: {
                required: true
            },
            delivery_date: {
                required: true
            }
        },messages: {
            branch_id: {
                required: "Please select Branch/Distributor "
            },
            cus_id: {
                required: "Please select Customer"
            },
            delivery_date: {
                required: "Please select Delivery Date"
            }
        },errorPlacement: function(error, element) {
            if(element.attr("name") == "branch_id") {
                error.insertAfter("#branchError");
            }else if(element.attr("name") == "cus_id"){
                error.insertAfter("#cusError");
            }else{
                error.insertAfter(element);
            }
        },submitHandler: function (form) {
            var result = $("#test select").filter(function () {
                if($(this).attr('name') == 'cylNo[]' || $(this).attr('name') == 'gas[]'){
                    ($(this).val() == '')?$(this).addClass('is-invalid'):$(this).removeClass('is-invalid');
                    return $(this).val() == ''
                }
            }).length == 0;

            if(result == true){
                form.submit();
            }
        }
    });

    $(document).on("change", "#branch_id", function(){
        var id   = $(this).val();
        var mtype = 'issue';

        $.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/get/branch/customer',
            data:{'id':id,'mtype':mtype},
            success: function(response) {
                $('.gas'+' option').remove();
                $('.gas').append($("<option></option>").attr("value", '').text('Select Gas'));

                $('.cyl_no'+' option').remove();
                $('.cyl_no').append($("<option></option>").attr("value", '').text('Select Cylinder No'));
                
                $('.rate').val('0.00');

                $('#renderCus').empty();
                $("#renderCus").append(response.result);
                $('.select2').select2();

                $('#trans').val('');
                $('#mob_no').val('');
                $('#address').val('');
            }
        });
    });

    $(document).on("click", "#view_cus", function(){
        cusId = $('#cus_id').val();

        if(cusId != ''){
            $.ajax({
                type: "GET",
                url: getsiteurl()+'/admin/get/customer/detail',
                data:{'cusId':cusId},
                success: function(response) {
                    $('#renderCusDetail').empty();
                    $("#renderCusDetail").append(response.result);
                    $('#customer-modal').modal('show');
                }
            });
        }
    });

    $(document).on("click", "#cyl_report", function(){
        cusId = $('#cus_id').val();

        if(cusId!= ''){
            window.open(
                getsiteurl()+"/admin/report/cylinder/"+cusId,
                '_blank'
            );
        }
    });

    $(document).on("click", "#addMore", function(){
        var count = $('#rowCount').val();
        count++;
        $('#rowCount').val(count);

        $.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/cyl-issue/add_more',
            data:{'count':count},
            success: function(response) {
                // $("#listing tbody").append(response.result);
                $("#listing tbody").closest('table').find('tr:last').prev().after(response.result);

                if(cusId != ''){
                    cusGas(count)
                }
            }
        });
    });

    $(document).on("click", ".remove", function(){
        var id    = $(this).attr('id');
        $('#newIssue_'+id).remove();
        rateTotal();
    });

    $(document).on("change", "#cus_id", function(){
        cusId = $(this).val();

        $('.gas'+' option').remove();
        $('.gas').append($("<option></option>").attr("value", '').text('Select Gas'));

        $('.cyl_no'+' option').remove();
        $('.cyl_no').append($("<option></option>").attr("value", '').text('Select Cylinder No'));
        
        $('.rate').val('0.00');

        cusGas(null);
        custTrans();
    });

    function custTrans(){
       $.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/cylinder-issue/customer/trans',
            data:{'cusId':cusId},
            success: function(response) {
                $('#trans').val(response.info.trans);
                $('#mob_no').val(response.info.mob_no);
                $('#address').val(response.cusAddress);
            }
        }); 
    }

    function cusGas(count){
        $.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/cylinder-issue/customer/gas',
            data:{'cusId':cusId},
            success: function(response) {
                if(count == null){
                    Object.values(response.gases).map((gname,index) => {
                        $('.gas')
                        .append($("<option></option>")
                        .attr("value", Object.keys(response.gases)[index])
                        .text(gname));
                    });
                }else{
                    Object.values(response.gases).map((gname,index) => {
                        $('#gas_'+count)
                        .append($("<option></option>")
                        .attr("value", Object.keys(response.gases)[index])
                        .text(gname));
                    });
                }
            }
        });
    }

    $(document).on("change", ".gas", function(){
        cusId         = $('#cus_id').val();
        var gasId     = $(this).val();
        var elementId = $(this).attr('id');
        var val       = elementId.substr(elementId.indexOf('_')+1);

        $('#cyl_'+val+' option').remove();
        $('#cyl_'+val).append($("<option></option>").attr("value", '').text('Select Cylinder No'));

        $.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/cylinder-issue/get/cyl-no',
            data:{'gasId':gasId,'cusId':cusId,'ftype':ftype},
            success: function(response) {
                if(response.result.length > 0){
                    Object.values(response.result).map((value) => {
                        $('#cyl_'+val)
                        .append($("<option></option>")
                        .attr("value", value.id)
                        .text((value.branch_cyl_no).toUpperCase()));
                    });
                }

                $('#rate_'+val).val(response.rate);
                $('#rent_'+val).val(response.rent);
                $('#srent_'+val).val(response.start_rent);

                rateTotal();
            }
        });
    });

    $(document).on("change", ".cyl_no", function(){
        var ab = $(this).val();
        var cd = $(this).attr('data-id');

        $(".cyl_no").each(function(){
            if($(this).attr('data-id') != cd && $(this).val() != '' && $(this).val() == ab){
                $('#cyl_'+cd).prop("selectedIndex", 0);

                Swal.fire({
                    type: 'info',
                    title: 'Opps!Cylinder No is already selected,Please select different'
                });
                
                return false;
            }
        });
    });

    $(document).on("click", ".delete", function(){
        var id  = $(this).attr('id');
        var row = $(this).attr('data-row');

        Swal.fire({
          title: 'Are you sure you want to delete?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then((result) => {
            if(result.value) {
                $.ajax({
                    type: "GET",
                    url: getsiteurl()+'/admin/delete/issue_detail',
                    data:{'id':id},
                    success: function(response) {
                        if(response.status == 'success'){
                            $('#storedDetail_'+id).remove();
                            rateTotal();
                        }
                    }
                });
            }
        });
    });

    function rateTotal(){
        var sum = 0.00;

        $('.rate').each(function() {
            sum = sum + parseFloat($(this).val());
        });

        $('#total').val(sum.toFixed(2));
    }
});