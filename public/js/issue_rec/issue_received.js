$(document).ready(function(){
    var bid       = '';
    var cid       = '';
    var challanNo = '';

  	$('#searchFrm').validate({
	    rules: {
	    	branch: {
	    		required: true
	    	},
            customer: {
                required: true
            },
            sysChalNo: {
                required: true
            }
	    },messages: {
	    	branch: {
	    		required: "Please select branch"
	    	},
            customer: {
                required: "Please select customer"
            },
            sysChalNo: {
                required: "Please select challan no"
            }
	    },errorPlacement: function(error, element) {
            if(element.attr("name") == "branch") {
                error.insertAfter("#brError");
            }else if(element.attr("name") == "customer") {
                error.insertAfter("#cusError");
            }else if(element.attr("name") == "sysChalNo") {
                error.insertAfter("#chaError");
            }
        },submitHandler: function (form) {
            chNo = $('#sysChalNo').val();

            $.ajax({
                type: "GET",
                url: getsiteurl()+'/admin/get/issued/list',
                data:{'bid':bid,'cid':cid,'chNo':chNo},
                success: function(response) {
                    $('#renderList').empty();
                    $("#renderList").append(response.result);
                    rateTotal();
                }
            }); 
        }
	});

    $(document).on("change", "#branch", function(){
        bid       = $(this).val();
        var mtype = 'received';

        $.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/get/branch/customer',
            data:{'id':bid,'mtype':mtype},
            success: function(response) {
                $('#sysChalNo'+' option').remove();
                $('#sysChalNo').append($("<option></option>").attr("value", '').text('Select'));

                $('#renderCus').empty();
                $("#renderCus").append(response.result);
                $('.select2').select2();
            }
        });
    });

    $(document).on("change", "#customer", function(){
        cid = $(this).val();
        
        $.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/cyl-issue/received/get/challan',
            data:{'bid':bid,'cid':cid},
            success: function(response) {
                $('#renderChalNo').empty();
                $("#renderChalNo").append(response.result);
                $('.select2').select2();
            }
        });
    });

    $(document).on("change", ".status", function(event) {
        var cyIssDetailId = $(this).attr('id');
        var received      = $(this).prop("checked")?1:0;

        $.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/change/issue/status',
            data:{'cyIssDetailId':cyIssDetailId,'received':received},
            success: function(response) {
                Swal.fire({
                    type: response.status,
                    title: response.message,
                    showConfirmButton:false,
                    timer: 1000
                }); 
            }
        });
    });

    function rateTotal(){
        var sum = 0.00;

        $('.rate').each(function() {
            sum = sum + parseFloat($(this).val());
        });

        $('#total').val(sum.toFixed(2));
    }
});