$(document).ready(function(){
    $('#purFrm').validate({
        rules: {
            branch_id: {
                required: true
            },
            supplier_name: {
                required: true
            },
            date: {
                required: true
            },
            invoice_no: {
                required: true
            }
        },messages: {
            branch_id: {
                required: "Please select branch"
            },
            supplier_name: {
                required: "Please enter supplier name"
            },
            date: {
                required: "Please select date"
            },
            invoice_no: {
                required: "Please enter invoice no"
            }
        },errorPlacement: function(error, element) {
            if(element.attr("name") == "branch_id") {
                error.insertAfter("#branchError");
            }else{
                error.insertAfter(element);
            }
        },submitHandler: function (form) {
            var result = $("#test input").filter(function () {
                if($(this).attr('type') == 'text' && ($(this).attr('name') == 'product[]' || $(this).attr('name') == 'qty[]' || $(this).attr('name') == 'rate[]' || $(this).attr('name') == 'tamount[]')){
                    ($.trim($(this).val()).length == 0)?$(this).addClass('is-invalid'):$(this).removeClass('is-invalid');
                    return $.trim($(this).val()).length == 0
                }
            }).length == 0;

            // var selRes = $("#test select").filter(function () {
            //     if($(this).attr('name') == 'per[]'){
            //         ($(this).val() == '')?$(this).addClass('is-invalid'):$(this).removeClass('is-invalid');
            //         return $(this).val() == ''
            //     }
            // }).length == 0;

            if(result == true){
                form.submit();
            }
        }
    });

    var count = 0;
    $(document).on("click", "#addMore", function(){
        count++;

        $.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/purchase/add_more',
            data:{'count':count},
            success: function(response) {
                $("#listing tbody").append(response.result);
            }
        });
    });

    $(document).on("click", ".remove", function(){
        $('#newPurchase_'+$(this).attr('id')).remove();
        totalAmt();
    });

    $(document).on("click", ".delete", function(){
        var id = $(this).attr('id');

        if(confirm('Are you sure you want to delete?')){
            $.ajax({
                type: "GET",
                url: getsiteurl()+'/admin/delete/purchase',
                data:{'id':id},
                success: function(response) {
                    $('#storedDetail_'+id).remove();
                    totalAmt();
                }
            });
        }
    });

    $(document).on("change", "#file", function(e){
        var fileName = e.target.files[0].name;
        $('#selFileName').text(fileName);
    });

    //  
    $(document).on("blur keypress keyup", ".qty", function() {
        var index = $(this).attr('data-index');
        var val1  = $('#qty_'+index).val();
        var val2  = $('#rate_'+index).val();

        var cal = val1 * val2;
        $('#tamount_'+index).val(cal.toFixed(2));
        totalAmt();
    });

    //  
    $(document).on("blur keypress keyup", ".rate", function() {
        var index = $(this).attr('data-index');
        var val1  = $('#qty_'+index).val();
        var val2  = $('#rate_'+index).val();

        var cal = val1 * val2;
        $('#tamount_'+index).val(cal.toFixed(2));
        totalAmt();
    });

    function totalAmt(){
        var sum = 0;

        $('input[name="tamount[]"]').each(function() {
            if($(this).val() != '' && $(this).val() > 0){
                sum = sum + parseFloat($(this).val());
            }
        });

        if(sum > 0){
            $('#subtotal').val('');
            $('#grandtotal').val('');

            $('#subtotal').val(sum.toFixed(2));
            var cal  = (sum * 18) / 100;
            var gtot = sum - cal;

            $('#grandtotal').val(gtot.toFixed(2));
        }
    }

    $(document).on("click", "#increment", function() {
        var incval = Math.ceil(parseFloat($('#grandtotal').val()));
        $('#grandtotal').val(incval+".00");
    });

    $(document).on("click", "#decrement", function() {
        var incval = Math.floor(parseFloat($('#grandtotal').val()));
        $('#grandtotal').val(incval+".00");
    });
});