$(document).ready(function(){
    var cid = '';
    var invType = 1;

    $('.select2bs4').select2({
        theme: 'bootstrap4'
    });

    $('#invFrm').validate({
        submitHandler: function (form) {
            var result = $("#renderListing select").filter(function () {
                if($(this).attr('name') == 'tax[]'){
                    ($(this).val() == '')?$(this).addClass('is-invalid'):$(this).removeClass('is-invalid');
                    return $(this).val() == ''
                }
            }).length == 0;

            if(result == true){
                form.submit();
            }
        }
    });

    $(document).on("change", ".normal-rent", function(){
        invType = $('input[name="invtype"]:checked').val();
        customerDetail();
    });

    $(document).on("change", "#customer", function(){
    	cid = $(this).val();
        customerDetail();
    });

    function customerDetail(){
    	$.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/invoice/customer/detail',
            data:{'cid':cid,'invType':invType},
            success: function(response) {
                $('#renderAdd').empty();
                $("#renderAdd").append(response.result);

                $('#renderChallan').empty();
                $("#renderChallan").append(response.chalResult);
                $('.select2bs4').select2({ theme: 'bootstrap4' });

                $('#renderListing').empty();
                $("#renderListing").append(response.rawData);

                $('#createInvoice').prop('disabled', true);
            }
        });
    }

    $(document).on("change", "#challan", function(){
        var challanNo = $(this).val();

        $.ajax({
            type: "GET",
            url: getsiteurl()+'/admin/invoice/get/challan_detail',
            data:{'challanNo':challanNo,'cid':cid,'invType':invType},
            success: function(response) {
                $('#renderListing').empty();
                $("#renderListing").append(response.rawData);

                $('#createInvoice').prop('disabled', true);
                if(response.allowToCreate){
                    $('#createInvoice').prop('disabled', false);
                }
                calculate();
            }
        });
    });

    $(document).on("change", ".tax", function(){
        calculate();
    });

    function calculate(){
        var tax12Total = 0.00;
        var tax18Total = 0.00;

        $("tr.info").each(function() {
            if(invType == 1){
                var rowTotal = parseFloat($(this).find("td").eq(8).html());
                var rowTax   = parseFloat($('td:eq(9) select',this).val());
            }else{
                var rowTotal = parseFloat($(this).find("td").eq(10).html());
                var rowTax   = parseFloat($('td:eq(11) select',this).val());
            }
            
            if(rowTax != ''){
                var taxCal = (rowTotal * rowTax) / 100;

                if(rowTax == 12){
                    tax12Total = tax12Total + taxCal;
                }else if(rowTax == 18){
                    tax18Total = tax18Total + taxCal;
                }
            }
        });

        // 
        var divide1 = (tax12Total/2).toFixed(2);
        $('#tbl_cgst6').text(divide1);
        $('#tbl_sgst6').text(divide1);
        $('#cgst6').val(divide1);
        $('#sgst6').val(divide1);

        // 
        var divide2 = (tax18Total/2).toFixed(2);
        $('#tbl_cgst9').text(divide2);
        $('#tbl_sgst9').text(divide2);
        $('#cgst9').val(divide2);
        $('#sgst9').val(divide2);

        var total    = (invType == 1)?parseFloat($('#lastTotal').val()):parseFloat($('#lastRentTotal').val());
        var netTotal = (total + tax12Total + tax18Total).toFixed(2);

        if($("tr.info").length == 0){
            $('#tbl_netTotal').text('0.00');
            $('#netTotal').val('0.00');
        }else{
            $('#tbl_netTotal').text(netTotal);
            $('#netTotal').val(netTotal);
        }
    }
});